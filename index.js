/**
 * @format
 */
import React, { Component, useEffect, useState } from 'react';
import { AppRegistry } from 'react-native';
import App from './App';
import { Provider, useSelector, shallowEqual } from 'react-redux';
import { name as appName } from './app.json';
import { store } from './src/store/configureStore'
const ReduxProvider = () => {
    return (
        <Provider store={store}>
            <App />
        </Provider>
    )
}
AppRegistry.registerComponent(appName, () => ReduxProvider);

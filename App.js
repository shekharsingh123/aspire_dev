import React, { Component, useEffect, useState } from 'react';
import {
    BaseButton,
    GestureHandlerRootView
} from 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from './src/util/RootNavigation';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import IntroScreen from './src/component/IntroScreen';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import Login from './src/component/SignIn';
import { store } from './src/store/configureStore'
import FlashMessage from 'react-native-flash-message';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Provider, useSelector, shallowEqual } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import SignUp from './src/component/SignUp';
import SuccessRegisteration from './src/component/SuccessRegisteration';
import SelectionScreen from './src/component/SelectionScreen'
import { Platform, Text, StyleSheet, View, SafeAreaViewBase, LogBox, ImageBackground, Image } from 'react-native'
import Filter from './src/component/filter/FilterCat';
import FilterProduct from './src/component/filter/FliterProduct';
import Home from './src/component/Home/HomeScreen';
import ProductDescription from './src/component/Home/ProductDescription';
import refundPolicy from './src/component/Cart/refundPolicy'
import OrderDetail from './src/component/Profile/OrderDetail'

import Cart from './src/component/Cart/cartScreen';

import orderScreen from './src/component/Profile/orderScreen'
import CheckOut from './src/component/Cart/CheckOut';
import Profile from './src/component/Profile/profileScreen';
import SignIn from './src/component/SignIn';
import StaticPage from './src/component/Profile/staticPage';
import EditProfile from './src/component/Profile/EditProfile';
import EditAddress from './src/component/Profile/EditAddress';
import FavoriteProduct from './src/component/Profile/FavoriteProduct';
import PaymentMethod from './src/component/Profile/PaymentMethod'
import { hpx, wpx } from './src/constants/constants';
import cartScreen from './src/component/Cart/cartScreen';
let home = require('./src/assets/images/Home.png');
let category = require('./src/assets/images/pay.png');
let cart = require('./src/assets/images/Credit.png');
let payment = require('./src/assets/images/payment.png');
let profile = require('./src/assets/images/user.png');
const AuthStack = createNativeStackNavigator();
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
function NavStack() {
    return (
        <AuthStack.Navigator initialRouteName="IntroScreen">
            <AuthStack.Screen name="IntroScreen" component={IntroScreen} options={{
                title: "IntroScreen",
                headerShown: false,
            }} />
            <AuthStack.Screen name="Login" component={Login} options={{
                title: "Login",
                headerShown: false,
            }} />
            <AuthStack.Screen name="SignUp" component={SignUp} options={{
                title: "SignUp",
                headerShown: false,
            }} />
            <AuthStack.Screen name="SuccessRegisteration" component={SuccessRegisteration} options={{
                title: "SuccessRegisteration",
                headerShown: false,
            }} />
            <AuthStack.Screen name="SelectionScreen" component={SelectionScreen} options={{
                title: "SelectionScreen",
                headerShown: false,
            }} />



        </AuthStack.Navigator>
    );
}

function HomeStack() {
    return (
        <Stack.Navigator
            initialRouteName="Home"
            screenOptions={{
                headerShown: false,
                headerStyle: { backgroundColor: '#42f44b' },
                headerTintColor: '#fff',
                headerTitleStyle: { fontWeight: 'bold' }
            }}>
            <Stack.Screen
                name="Home"
                component={Home}
                options={{ title: 'Home Page' }} />
            <Stack.Screen
                name="ProductDescription"
                component={ProductDescription}
                options={{ title: 'ProductDescription Page' }} />


        </Stack.Navigator>
    );
}
function CartStack() {
    return (
        <Stack.Navigator
            initialRouteName="Cart"
            screenOptions={{
                headerShown: false,
                headerStyle: { backgroundColor: '#42f44b' },
                headerTintColor: '#fff',
                headerTitleStyle: { fontWeight: 'bold' }
            }}>
            <Stack.Screen
                name="Cart"
                component={Cart}
                options={{ title: 'Cart Page' }} />
            <Stack.Screen
                name="CheckOut"
                component={CheckOut}
                options={{ title: 'Cart Page' }} />

            <Stack.Screen
                name="refundPolicy"
                component={refundPolicy}
                options={{ title: 'Static Page' }} />


        </Stack.Navigator>
    );
}

function SettingsStack() {
    return (
        <Stack.Navigator
            initialRouteName="Profile"
            screenOptions={{
                headerShown: false,
                headerStyle: { backgroundColor: '#42f44b' },
                headerTintColor: '#fff',
                headerTitleStyle: { fontWeight: 'bold' }
            }}>
            <Stack.Screen
                name="Profile"
                component={Profile}
                options={{ title: 'Profile Page' }} />
            <Stack.Screen
                name="StaticPage"
                component={StaticPage}
                options={{ title: 'Static Page' }} />
            <Stack.Screen
                name="EditProfile"
                component={EditProfile}
                options={{ title: 'Static Page' }} />
            <Stack.Screen
                name="EditAddress"
                component={EditAddress}
                options={{ title: 'Static Page' }} />
            <Stack.Screen
                name="FavoriteProduct"
                component={FavoriteProduct}
                options={{ title: 'Static Page' }} />
            <Stack.Screen
                name="PaymentMethod"
                component={PaymentMethod}
                options={{ title: 'Static Page' }} />
            <Stack.Screen
                name="orderScreen"
                component={orderScreen}
                options={{ title: 'Static Page' }} />
            <Stack.Screen
                name="OrderDetail"
                component={OrderDetail}
                options={{ title: 'OrderDetail Page' }} />


        </Stack.Navigator>
    );
}
function FilterStack() {
    return (
        <Stack.Navigator
            initialRouteName="Filter"
            screenOptions={{
                headerShown: false,
                headerStyle: { backgroundColor: '#42f44b' },
                headerTintColor: '#fff',
                headerTitleStyle: { fontWeight: 'bold' }
            }}>
            <Stack.Screen
                name="Filter"
                component={Filter}
                options={{ title: 'Filter Page' }} />
            <Stack.Screen
                name="FilterProduct"
                component={FilterProduct}
                options={{ title: 'Filter Page' }} />


        </Stack.Navigator>
    );
}





export default function App() {
    const { renderAgainStack, selected_Cat } = useSelector(
        (state) => ({
            renderAgainStack: state.globalReducer.renderAgainStack,
            selected_Cat: state.homeReducer.selected_Cat
        }),
        shallowEqual,
    );
    const [screenType, setscreenType] = useState(0);
    // const [startScreen, setstartScreen] = useState(true);
    const getData = async () => {
        let userTokens = await AsyncStorage.getItem('Token');

        if (userTokens || selected_Cat) {
            //if user is authenticated
            setscreenType(1);
        } else {
            setscreenType(1);
        }

    }
    useEffect(() => {
        LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
        console.log("screentype", screenType)
        getData();
    }, [renderAgainStack]);

    return (

        <SafeAreaProvider style={{
            flex: 1,
            backgroundColor: 'black'
        }}>
            <Provider store={store} >

                <FlashMessage
                    position="top"

                    animated={true}
                    duration={2000}
                    statusBarHeight={Platform.OS == 'android' && hpx(10)}
                //  hideStatusBar={true}
                //icon={true}
                // onPress={true}
                // GLOBAL FLASH MESSAGE COMPONENT INSTANCE {"success" (green), "warning" (orange), "danger" (red), "info" (blue) and "default" (gray)}
                />
                <NavigationContainer ref={navigationRef}>


                    <Tab.Navigator

                        initialRouteName="Category"
                        screenOptions={{
                            tabBarActiveTintColor: '#01D167',

                            tabBarInactiveTintColor: '#DDDDDD',
                            tabBarStyle: { height: hpx(56), backgroundColor: '#FFFFFF', paddingBottom: 5, },
                        }}
                    >
                        <Tab.Screen
                            name="HomeStack"
                            listeners={{
                                tabPress: e => {
                                    // Prevent default action
                                    e.preventDefault();
                                },
                            }}
                            component={HomeStack}

                            options={{

                                headerShown: false,
                                tabBarOptions: {
                                    labelStyle: {
                                        fontSize: 9,
                                        margin: 0,
                                        padding: 0,
                                    },
                                },
                                tabBarLabel: 'Home',
                                tabBarIcon: ({ size, focused, color }) => {
                                    return (
                                        <Image source={home}
                                            style={{ height: hpx(24), width: wpx(24), tintColor: focused ? '#01D167' : '#EEEEEE' }}
                                        />
                                    );
                                },
                            }} />

                        <Tab.Screen
                            name="Category"

                            component={FilterStack}

                            options={{
                                // tabBarStyle: { display: 'none' },
                                tabBarOptions: {
                                    labelStyle: {
                                        fontSize: 9,
                                        margin: 0,
                                        padding: 0,
                                    },
                                },
                                headerShown: false,

                                tabBarLabel: 'Debit Cart',
                                tabBarIcon: ({ size, focused, color }) => {
                                    return (
                                        <Image source={category}
                                            style={{ height: hpx(24), width: wpx(24), tintColor: focused ? '#01D167' : '#EEEEEE' }}
                                        />
                                    );
                                },
                            }} />
                        <Tab.Screen
                            name="Cart"
                            listeners={{
                                tabPress: e => {
                                    // Prevent default action
                                    e.preventDefault();
                                },
                            }}
                            component={CartStack}

                            options={{
                                tabBarOptions: {
                                    labelStyle: {
                                        fontSize: 9,
                                        margin: 0,
                                        padding: 0,
                                    },
                                },
                                headerShown: false,

                                tabBarLabel: 'Payments',
                                tabBarIcon: ({ size, focused, color }) => {
                                    return (
                                        <Image source={payment}
                                            style={{ height: hpx(24), width: wpx(24), tintColor: focused ? '#01D167' : '#EEEEEE' }}
                                        />
                                    );
                                },
                            }} />
                        <Tab.Screen
                            name="Credit"
                            listeners={{
                                tabPress: e => {
                                    // Prevent default action
                                    e.preventDefault();
                                },
                            }}
                            component={SettingsStack}

                            options={{
                                tabBarOptions: {
                                    labelStyle: {
                                        fontSize: 9,
                                        margin: 0,
                                        padding: 0,
                                    },
                                },
                                headerShown: false,

                                tabBarLabel: 'Credit',
                                tabBarIcon: ({ size, focused, color }) => {
                                    return (
                                        <Image source={cart}
                                            style={{ height: hpx(24), width: wpx(24), tintColor: focused ? '#01D167' : '#EEEEEE' }}
                                        />
                                    );
                                },
                            }} />
                        <Tab.Screen
                            name="Account"
                            listeners={{
                                tabPress: e => {
                                    // Prevent default action
                                    e.preventDefault();
                                },
                            }}
                            component={SettingsStack}

                            options={{
                                tabBarOptions: {
                                    labelStyle: {
                                        fontSize: 9,
                                        margin: 0,
                                        padding: 0,
                                    },
                                },
                                headerShown: false,

                                tabBarLabel: 'Profile',
                                tabBarIcon: ({ size, focused, color }) => {
                                    return (
                                        <Image source={profile}
                                            style={{ height: hpx(24), width: wpx(24), tintColor: focused ? '#01D167' : '#EEEEEE' }}
                                        />
                                    );
                                },
                            }} />
                    </Tab.Navigator>


                </NavigationContainer>



            </Provider>
        </SafeAreaProvider>
    );
}
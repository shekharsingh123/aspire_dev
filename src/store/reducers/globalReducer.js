import { types } from '../ActionTypes';

const INITIAL_STATE = {
  loader: false,
  renderAgainStack: false,
  index: 0,
  cart_index: 0,
  business_Step_Index: 0
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case types.TOGGLE_LOADING: {
      return { ...state, loader: payload };
    }
    case types.RENDER_AGAIN_STACK_NAV: {
      //renders the stack so that we can swicth to auth or home stack as per async data
      return { ...state, renderAgainStack: !state.renderAgainStack };
    }
    case types.INCREMENT_INDEX: {
      return { ...state, index: payload };
    }
    case types.STEP_INITIAL: {
      return { ...state, index: 0 };
    }

    case types.CART_INCREMENT_INDEX: {
      return { ...state, cart_index: payload };
    }
    case types.BUSINESS_INDEX: {
      return { ...state, business_Step_Index: payload };
    }
    default:
      return state;
  }
};

import ensureNativeModuleAvailable from 'react-native-vector-icons/dist/lib/ensure-native-module-available';
import { types } from '../ActionTypes';

const INITIAL_STATE = {
  banner_Data: null,
  categories_Data: null,
  products_list: null,
  selected_Cat: null,
  product_detail: null,
  rating_success: null,
  selectedFav: [],
  wishlist_item: null,
  cart_item: ensureNativeModuleAvailable
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case types.GET_BANNERS_HOME_SUCCESS: {
      return {
        ...state,
        banner_Data: payload,
      };
    }
    case types.GET_CATEGORY_SUCCESS: {
      return {
        ...state,
        categories_Data: payload
      };
    }
    case types.GET_PRODUCTS_SUCCESS: {
      return {
        ...state,
        products_list: payload
      };
    } case types.SELECTED_CATEGORY: {
      return {
        ...state,
        selected_Cat: payload
      };
    }
    case types.PRODUCT_DETAILS_SUCCESS: {
      return {
        ...state,
        product_detail: payload
      };
    }
    case types.RATING_SUCCESS: {
      return {
        ...state,
        rating_success: payload
      };
    }
    case types.FAV_ITEMS: {
      return {
        ...state,
        selectedFav: payload
      };
    }
    case types.GET_WISHLIST_SUCCESS: {
      return {
        ...state,
        wishlist_item: payload
      };
    }
    case types.CART_ITEM_SUCCESS: {
      return {
        ...state,
        cart_item: payload
      };
    }


    default:
      return state;
  }
};

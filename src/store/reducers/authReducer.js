import { types } from '../ActionTypes';

const INITIAL_STATE = {
  userData: '',
  swiperIndex: null,
  address: null,
  contact_data: null,
  Selected_follow: [],
  user_id: null,
  device_token: '',
  signUpDATA: null
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case types.LOGIN_SAGA_SUCCESS: {
      return {
        ...state,
        userData: payload,
      };
    }
   
    default:
      return state;
  }
};

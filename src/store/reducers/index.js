import { combineReducers } from 'redux';
import { types } from '../ActionTypes';
import authReducer from './authReducer';
import globalReducer from './globalReducer';
import profileReducer from './profileReducer';
import homeReducer from './homeReducer';
const appReducer = combineReducers({
  authReducer,
  globalReducer,
  profileReducer,
  homeReducer

});

const rootReducer = (state, action) => {
  if (action.type === types.DESTROY_SESSION) state = undefined;

  return appReducer(state, action);
};
function reducer(state = initState(), action) {
  return state
}
function initState() {
  return {
    user_id: localStorage.user_id,

  }
}
export default rootReducer;

import { types } from '../ActionTypes';

const INITIAL_STATE = {
    staticPage: null,
    profile_Data: null,
    address_Data: null
};

export default (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case types.STATIC_PAGES_SUCCESS: {

            return {
                ...state,
                staticPage: payload,
            };
        }
        case types.PROFILE_DATA_SUCCESS: {

            return {
                ...state,
                profile_Data: payload,
            };
        }

        case types.GET_ADDRESS_SUCCESS: {

            return {
                ...state,
                address_Data: payload,
            };
        }


        default:
            return state;
    }

};

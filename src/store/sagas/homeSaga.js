import { delay, select, takeLatest, put, call } from 'redux-saga/effects';
import { types } from '../ActionTypes';
import { serviceUrl } from '../../constants/constants';
import { apiCall } from '../../util/utility';

import { Alert } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import * as RootNavigation from '../../util/RootNavigation';

function* addWeekLimint(obj) {

    try {
        const { payload } = obj;

        // delete payload.navigation;

        //GET ALL/MY GAMES:
        // let { status, response } = yield call(
        //     apiCall,
        //     `${serviceUrl.baseURL}addCardLimit`,
        //     payload,
        //     'POST',
        // );
        showMessage({
            message: "Limit Updated Successfully",
            type: "default",
            backgroundColor: "#5DD986", // background color
            color: "#fafafa", // text color
            titleStyle: { textAlign: 'center' },
        });
        yield put({
            type: types.TOGGLE_LOADING,
            payload: false,
        });


        RootNavigation.navigate('Filter')
        // yield put({
        //   type: types.RENDER_AGAIN_STACK_NAV,
        // });



    } catch (error) {

        yield put({
            type: types.TOGGLE_LOADING,
            payload: false,
        });
        console.log('SAGA_CATCH_ERROR', error.message);
    }
}





export default function* watchSettingSaga() {


    yield takeLatest(types.WEEK_LIMIT, addWeekLimint);



}
import { delay, select, takeLatest, put, call } from 'redux-saga/effects';
import { types } from '../ActionTypes';
import { serviceUrl } from '../../constants/constants';
import { apiCall } from '../../util/utility';

import { Alert } from 'react-native';
import { showMessage } from 'react-native-flash-message';


function* getStaticPage(obj) {

    try {
        const { payload } = obj;

        let { status, response } = yield call(
            apiCall,
            `${serviceUrl.baseURL}page/${payload}`,
            null,
            'GET',
        );
        if (status) {

            yield put({
                type: types.STATIC_PAGES_SUCCESS,
                payload: response.data,
            });
            yield put({
                type: types.TOGGLE_LOADING,
                payload: false,
            });

        } else {

            yield put({
                type: types.TOGGLE_LOADING,
                payload: false,
            });
        }
    } catch (error) {

        yield put({
            type: types.TOGGLE_LOADING,
            payload: false,
        });
        console.log('SAGA_CATCH_ERROR', error.message);
    }
}

function* getProfileData(obj) {

    try {
        const { payload } = obj;

        let { status, response } = yield call(
            apiCall,
            `${serviceUrl.baseURL}dashboard`,
            null,
            'GET',
        );
        if (status) {

            yield put({
                type: types.PROFILE_DATA_SUCCESS,
                payload: response.data,
            });
            yield put({
                type: types.TOGGLE_LOADING,
                payload: false,
            });

        } else {

            yield put({
                type: types.TOGGLE_LOADING,
                payload: false,
            });
        }
    } catch (error) {

        yield put({
            type: types.TOGGLE_LOADING,
            payload: false,
        });
        console.log('SAGA_CATCH_ERROR', error.message);
    }
}

function* getAddress(obj) {

    try {
        const { payload } = obj;

        let { status, response } = yield call(
            apiCall,
            `${serviceUrl.baseURL}addresses`,
            null,
            'GET',
        );
        if (status) {

            yield put({
                type: types.GET_ADDRESS_SUCCESS,
                payload: response.data,
            });
            yield put({
                type: types.TOGGLE_LOADING,
                payload: false,
            });

        } else {

            yield put({
                type: types.TOGGLE_LOADING,
                payload: false,
            });
        }
    } catch (error) {

        yield put({
            type: types.TOGGLE_LOADING,
            payload: false,
        });
        console.log('SAGA_CATCH_ERROR', error.message);
    }
}
function* deleteWishlist(obj) {
    try {
        yield put({
            type: types.TOGGLE_LOADING,
            payload: true,
        });
        const { payload } = obj;
        //GET ALL/MY GAMES:
        let { status, response } = yield call(
            apiCall,
            `${serviceUrl.baseURL}wishlist/${payload}/remove`,
            null,
            'DELETE',
        );
        if (status) {

            yield put({
                type: types.CART_ITEM_SUCCESS,
                payload: response,
            });


            yield put({
                type: types.TOGGLE_LOADING,
                payload: false,
            });
        } else {
            yield put({
                type: types.TOGGLE_LOADING,
                payload: false,
            });
        }
    } catch (error) {
        yield put({
            type: types.TOGGLE_LOADING,
            payload: false,
        });
        console.log('SAGA_CATCH_ERROR', error.message);
    }
}
function* deleteCartItemlist(obj) {
    try {
        yield put({
            type: types.TOGGLE_LOADING,
            payload: true,
        });
        const { payload } = obj;
        //GET ALL/MY GAMES:
        let { status, response } = yield call(
            apiCall,
            `${serviceUrl.baseURL}cart/removeItem?cart=${payload.cart}&item=${payload.item}`,
            null,
            'DELETE',
        );
        if (status) {
            ;
            yield put({
                type: types.GET_WISHLIST,

            });

            yield put({
                type: types.TOGGLE_LOADING,
                payload: false,
            });
        } else {
            yield put({
                type: types.TOGGLE_LOADING,
                payload: false,
            });
        }
    } catch (error) {
        yield put({
            type: types.TOGGLE_LOADING,
            payload: false,
        });
        console.log('SAGA_CATCH_ERROR', error.message);
    }
}
function* updateProfile(obj) {
    try {
        yield put({
            type: types.TOGGLE_LOADING,
            payload: true,
        });
        const { payload } = obj;
        //GET ALL/MY GAMES:
        let { status, response } = yield call(
            apiCall,
            `${serviceUrl.baseURL}account/update`,
            payload,
            'PUT',
        );
        if (status) {
            yield put({
                type: types.GET_USER_PROFILE,
                payload: response,
            });
            yield put({
                type: types.TOGGLE_LOADING,
                payload: false,
            });
            NavigationService.navigate('ProfileScreen');
        } else {
            yield put({
                type: types.TOGGLE_LOADING,
                payload: false,
            });
        }
    } catch (error) {
        yield put({
            type: types.TOGGLE_LOADING,
            payload: false,
        });
        console.log('SAGA_CATCH_ERROR', error.message);
    }
}


export default function* watchSettingSaga() {

    yield takeLatest(types.STATIC_PAGES, getStaticPage);
    yield takeLatest(types.REMOVE_WISHLIST_ITEM, deleteWishlist);
    yield takeLatest(types.REMOVE_CART_ITEM, deleteCartItemlist);
    yield takeLatest(types.PROFILE_DATA, getProfileData);
    yield takeLatest(types.USER_PROFILE_UPDATE, updateProfile);
    yield takeLatest(types.GET_ADDRESS, getAddress);
    


}
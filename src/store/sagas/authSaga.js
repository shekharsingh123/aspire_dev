import { delay, select, takeLatest, put, call } from 'redux-saga/effects';
import { types } from '../ActionTypes';
import { serviceUrl } from '../../constants/constants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { apiCall } from '../../util/utility';

import { showMessage, hideMessage } from 'react-native-flash-message';

import * as RootNavigation from '../../util/RootNavigation';
import {
  Colors,
  hp,
  wp,
  hpx,
  mixpanel,
  wpx,
  nf,
  Fonts,
} from '../../constants/constants';


function* login(obj) {
  try {
    const { payload } = obj;

    // delete payload.navigation;

    //GET ALL/MY GAMES:
    let { status, response } = yield call(
      apiCall,
      `${serviceUrl.baseURL}auth/login`,
      payload,
      'POST',
    );

    if (status) {
      console.log('user_id', response, response);
      RootNavigation.navigate('SelectionScreen')
      AsyncStorage.setItem('Token', response?.data?.api_token);
      AsyncStorage.setItem('user_id', response?.data.id.toString());
      yield put({
        type: types.TOGGLE_LOADING,
        payload: false,
      });
      showMessage({
        message: "Login successfully",
        type: "default",
        backgroundColor: "#5DD986", // background color
        color: "#fafafa", // text color
        titleStyle: { textAlign: 'center' },
      });

      // yield put({
      //   type: types.RENDER_AGAIN_STACK_NAV,
      // });

      yield put({
        type: types.LOGIN_SAGA_SUCCESS,
        payload: response,
      });
    } else {
      showMessage({
        message: response.message,
        type: 'danger',
        titleStyle: { textAlign: 'center' },
      });
      yield put({
        type: types.TOGGLE_LOADING,
        payload: false,
      });
    }
  } catch (error) {
    yield put({
      type: types.TOGGLE_LOADING,
      payload: false,
    });
    console.log('SAGA_CATCH_ERROR', error.message);
  }
}

function* signUp(obj) {
  try {
    const { payload } = obj;
    //GET ALL/MY GAMES:
    let { status, response } = yield call(
      apiCall,
      `${serviceUrl.baseURL}auth/register`,
      payload,
      'POST',
    );
    if (status) {
      AsyncStorage.setItem('Token', response?.data?.api_token);
      AsyncStorage.setItem('user_id', response?.data.id.toString());
      yield put({
        type: types.TOGGLE_LOADING,
        payload: false,
      });
      RootNavigation.navigate('SuccessRegisteration')

      yield put({
        type: types.SIGN_UP_SAVED_DATA,
        payload: response,
      });
    } else {
      yield put({
        type: types.TOGGLE_LOADING,
        payload: false,
      });
      showMessage({
        message: response.message,
        type: 'danger',
        titleStyle: { textAlign: 'center' },
      });
    }
  } catch (error) {
    yield put({
      type: types.TOGGLE_LOADING,
      payload: false,
    });
    showMessage({
      message: response.message,
      type: 'danger',
      titleStyle: { textAlign: 'center' },
    });
    console.log('SAGA_CATCH_ERROR', error.message);
  }
}



function* logoutSaga(obj) {
  yield call(AsyncStorage.clear);
  yield put({
    type: types.DESTROY_SESSION,
  });
  // try {
  //   const { payload } = obj;
  //   let { status, response } = yield call(
  //     apiCall,
  //     `${serviceUrl.baseURL}logout`,
  //     payload,
  //     'POST',
  //   );
  //   if (status) {
  //     yield call(AsyncStorage.clear);
  //     yield put({
  //       type: types.DESTROY_SESSION,
  //     });
  //     yield put({
  //       type: types.RENDER_AGAIN_STACK_NAV,
  //     });
  //   }
  //   yield put({
  //     type: types.CHANGE_SWIPER_INDEX,
  //     payload: 1,
  //   });
  //   console.log('logoutSaga', status, response);
  // } catch (error) {
  //   console.log('SAGA_CATCH_ERROR', error.message);
  // }
}

export default function* watchAuthSaga() {
  yield takeLatest(types.LOGIN_SAGA, login);
  yield takeLatest(types.SINGUP_SAGA, signUp);
  yield takeLatest(types.LOGOUT_SAGA, logoutSaga);

}

import React from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

import { Colors, Icons, hp, wp, hpx, wpx, nf, Fonts } from './constants';

const { width } = Dimensions.get('screen');

const ImagePickerModal = ({ ImageModal, OpenCamera, OpenGallery, ImageItem, ImageIndex }) => {
  return (
    <View style={Styles.cameraModalContainer}>
      <TouchableOpacity activeOpacity={1.0}
        onPress={() => ImageModal()}
        style={Styles.cameraModalContainerBackground}
      />
      <View style={Styles.cameraModalContainerMainScreen}>
        <TouchableOpacity activeOpacity={1.0}
          style={Styles.cameraTextContainer}
          onPress={() => OpenCamera(ImageItem, ImageIndex)}>
          <Text style={Styles.cameraText}>
            Take a Photo
          </Text>
        </TouchableOpacity>
        <View style={Styles.contentSeprateLine} />
        <TouchableOpacity activeOpacity={1.0}
          style={Styles.cameraTextContainer}
          onPress={() => OpenGallery(ImageItem, ImageIndex)}>
          <Text style={Styles.cameraText}>Gallery</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const Styles = StyleSheet.create({
  cameraModalContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  cameraModalContainerBackground: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  cameraModalContainerMainScreen: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: Colors.white,
    width: wpx(375),
    height: hpx(180),
  },
  cameraTextContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  cameraText: {
    fontSize: nf(18),
    fontFamily: Fonts.regular,
  },
  contentSeprateLine: {
    alignSelf: 'center',
    width: wpx(340),
    height: hpx(1),
    backgroundColor: Colors.grey,
    marginVertical: hpx(24),
    opacity: 0.5,
  },
});
export default ImagePickerModal;

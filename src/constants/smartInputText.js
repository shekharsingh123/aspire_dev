import React from "react";
import { View, TextInput, Dimensions } from "react-native";
import {
    Colors,
    Icons,
    hp,
    wp,
    hpx,
    wpx,
    nf,
    Fonts,
  } from '../constants/constants';
const { width } = Dimensions.get("window");

const SmartInputText = ({
  refValue,
  valid,
  dataValue,
  handleChange,
  onSubmit,
  placeholderText,
  errorPlaceholderText,
  keyboard_type,
  keyboard_return_type,
}) => {
  return (
    <View
      style={
        valid
          ? {
              width: width / 1.12,
              height: hpx(50),
              borderRadius: wpx(10),
              alignItems: "center",
              backgroundColor: "rgb(241,241,241)",
              alignSelf: "center",
              marginVertical: hpx(5),
            }
          : {
              width: width / 1.12,
              height: hpx(50),
              borderRadius: wpx(10),
              borderWidth: wpx(1),
              borderColor: "rgb(222, 37, 37)",
              alignItems: "center",
              backgroundColor: "rgb(241,241,241)",
              alignSelf: "center",
              marginVertical: hpx(5),
            }
      }
    >
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          marginVertical: hpx(5),
          marginHorizontal: wpx(5),
        }}
      >
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            width: wpx(10),
            backgroundColor: "#000",
          }}
        />
        <TextInput
          ref={refValue}
          style={{
            flex: 1,
            width: width / 2.8,
            backgroundColor: "transparent",
            height: hpx(46),
            fontSize: nf(16),
          }}
          value={dataValue}
          autoCorrect={false}
          onChangeText={(val) => handleChange(val)}
          placeholder={valid ? placeholderText : errorPlaceholderText}
          placeholderTextColor={valid ? "rgba(0,0,0,0.6)" : "rgb(222, 37, 37)"}
          returnKeyType={keyboard_return_type}
          keyboardType={keyboard_type}
          onSubmitEditing={() => onSubmit(dataValue)}
          autoCapitalize="none"
        />
      </View>
    </View>
  );
};

export default SmartInputText;

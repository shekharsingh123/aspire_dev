import React from 'react';
import { View, ActivityIndicator,Text } from 'react-native';
import {
  Colors,
  hp,
  wp,
} from './constants';

const Spinner = () => {
     return(
      <View style={styles.screenContainer2}>
      <View style={styles.centerContainer}>
        <ActivityIndicator size='large'  color="#D80B8C" />
      </View>
    </View>
  );
};

export default Spinner;

const styles = {
  screenContainer2: {
    flex: 1,
    backgroundColor:Colors.white,
    // opacity: 0.4,
  },
  centerContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
};
//USAGE:     import { Colors, Icons, hp, wp, hpx, wpx, nf, Fonts } from '../../constants/constants'
import { Dimensions, PixelRatio, Platform } from 'react-native';



const baseURL = 'https://springs15.com/test_springs15/public/api/'
const webUrl = 'http://springs15.com/test_springs15/public/'

export const serviceUrl = {
  baseURL: baseURL,
  webUrl: webUrl

}


//COLORS CONSTANTS
export const Colors = {
  darkBg: '#1E1E1E',
  white: '#FFFFFF',
  textRed: '#FD3C4F',
  secondaryDark: '#292929',
  redDots: '#FF0467',
  redText: '#FD3D4E',
  secondaryPink: '#FE627F',
  orange: '#FC7437',
  green: '#47F15A',
  blue: '#0DE8E8',
  purple: '#5A3D84',
  blue: '#3280C2',
  grey: '#999999',
  cardGreyBg: '#292929',
  darkBlack: '#141414',
  greenText: '#83CE44',
  greyText: '#858585',
  shadow: '#3131312B',
  shadow1: '#BCBCBC26',
  black: '#000000',
  pink: '#D80B8C',
  secondBlack: '#1A1A1A',

};

//FONT STYLE CONSTANTS
export const Fonts = {
  avenir_next_bold: 'AvenirNext-Bold',
  avenir_next_medium: 'AvenirNext-Medium',
  avenir_next_DemiBold: 'AvenirNext-DemiBold',
  avenir_next_Regular: 'AvenirNext-Regular'
};

//DYNAMIC UI FUNCTIONS:
const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get('window');

//FONT SCALING
//Usage: nf(16)
const scale = SCREEN_HEIGHT / 812;
const normalizeFont = (size) => {
  const newSize = size * scale;
  return Math.round(PixelRatio.roundToNearestPixel(newSize));

};

//DYNAMIC DIMENSION CONSTANTS
//Usage: wp(5), hp(20)
const widthPercentageToDP = (widthPercent) => {
  // Convert string input to decimal number
  const elemWidth = parseFloat(widthPercent);
  return PixelRatio.roundToNearestPixel((SCREEN_WIDTH * elemWidth) / 100);
};
const heightPercentageToDP = (heightPercent) => {
  // Convert string input to decimal number
  const elemHeight = parseFloat(heightPercent);
  return PixelRatio.roundToNearestPixel((SCREEN_HEIGHT * elemHeight) / 100);
};

//Usage: wpx(141), hpx(220)
const widthFromPixel = (widthPx, w = 375) => {
  const newSize = widthPx * (SCREEN_WIDTH / w);
  return newSize;
};
const heightFromPixel = (heightPx, h = 812) => {
  const newSize = heightPx * (SCREEN_HEIGHT / h);
  return newSize;
};

export {
  normalizeFont as nf,
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  widthFromPixel as wpx,
  heightFromPixel as hpx,
};

import { Platform } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { serviceUrl } from '../constants/constants';
//import NavigationService from '../routing/NavigationService';
import { store } from '../store/configureStore'
import { types } from '../store/ActionTypes'

exports.apiCall = async function (url, body, method, token = null) {

    if (!token) {
        token = await AsyncStorage.getItem("Token")
    }
    console.log('exports.Call:', url, token, method);
    return fetch(url, {
        method: method,
        headers: !token ? //anonymous apis
            {
                Accept: 'application/json',
                'Content-Type': 'application/json',

            }
            :
            {      // logged in user
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
        body: body ? JSON.stringify(body) : null,
    })
        .then((response) => {
            // console.log('response util : ', response);
            return new Promise(function (resolve, reject) {
                response.json().then(responseParsed => {
                    // console.log('response util : ', responseParsed);
                    if (response.status == 200 || response.status == 201 || response.status == 204) {               // success
                        resolve({ status: true, response: responseParsed })
                    } else if (response.status == 401 || response.status == 403) {             // access token unauthorised
                        // BY CALLBACK
                        exports.apiCallForRefreshToken(function () {
                            resolve(exports.apiCall(url, body, method))
                        })
                        // BY PROMISE
                        resolve({ status: false, response: responseParsed })
                    } else if (response.status == 599) {
                        if (token) {
                            // AsyncStorage.multiRemove(['userTokens']);
                            AsyncStorage.clear();
                            store.dispatch({
                                type: types.RENDER_AGAIN_STACK_NAV
                            })
                            // NavigationService.reset(0, 'IntroScreen')
                        }
                    }
                    else {
                        resolve({ status: false, response: responseParsed })                                         // failed
                        // resolve(response.json())
                    }
                })

            })
        })
        .catch((err) => {
            console.log('Something went wrong, please check your network.')
        })
}

// to refresh token by calling api BY PROMISE
exports.apiCallForRefreshToken = async function () {

    let refreshToken = await AsyncStorage.getItem("refreshToken")
    let deviceToken = await AsyncStorage.getItem("deviceToken")
    await AsyncStorage.removeItem('accessToken')

    return new Promise(function (resolve, reject) {
        resolve(exports.apiCall(`${serviceUrl.user}access-token/new`,
            {
                "refreshToken": refreshToken,
                "deviceToken": deviceToken
            },
            "POST",
            null
        )
            .then(function (response) {
                console.log("apiCallForRefreshTokenRES", response)
                if (response.status) {
                    AsyncStorage.setItem('userTokens', JSON.stringify(response.data));
                    AsyncStorage.setItem('accessToken', response.response.data.accessToken);
                    // AsyncStorage.setItem('refreshToken', response.data.refreshToken);
                } else {
                    // AsyncStorage.multiRemove(['userTokens'])
                    AsyncStorage.clear()
                        .catch(err => {
                            console.log("can not be removed due to", err)
                        });
                    store.dispatch({
                        type: types.RENDER_AGAIN_STACK_NAV
                    })
                    //NavigationService.reset(0, 'IntroScreen')
                    return true
                }
            })
            .catch(function (error) {
                console.log("Error at utility in function3: apiCallForRefreshToken" + error)
                return { error: true, errorMessage: error }
            })
        )
    })
}


// Multipart Api call
export const multipartApi = async function (url, body, method) {
    let token;
    token = await AsyncStorage.getItem("userTokens")
    // console.log('utility data : ', url)
    // console.log('utility data : ', body)
    // console.log('utility data : ', token)
    // if(Platform.OS=="ios"){
    return fetch(url, {
        method: method,
        headers: {
            'accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            // 'access_token': `${token}`,
            'client_secret': "2B4D6251655468576D5A7134743777217A25432646294A404E635266556A586E3272357538782F413F4428472D4B6150645367566B5970337336763979244226"
        },
        body: body,
    })
        .then((response) => {
            return new Promise(function (resolve, reject) {
                if (response.status == 200 || response.status == 201 || response.status == 204) {               // success
                    resolve(response.json())
                } else if (response.status == 401) {             // access token unauthorised
                    resolve(response.json())
                } else {                                                                // failed
                    resolve(response.json())
                }
            })
        }).catch((err) => {
            console.log("error : " + err)
        })

}



// export const GlobalToast = (message, duration = 5000, style, textStyle) =>
//     toast.show(message, {
//         duration,
//         style: { padding: 0, ...style },
//         textStyle: { fontSize: 20, ...textStyle },
//     });


// import React, { useEffect } from 'react'
// import { useDispatch } from 'react-redux'
// import messaging from '@react-native-firebase/messaging'
// import { types } from '../store/ActionTypes'
// import PushNotification from "react-native-push-notification";
// import AsyncStorage from '@react-native-async-storage/async-storage'
// //import { handlePushNotification } from '../common/Function'
// import NavigationService from '../routing/NavigationService';
// import { handlePushNotification } from './handlePushNotification';
// import PushNotificationIOS from '@react-native-community/push-notification-ios';
// export default function NotificationHandler() {

//     const dispatch = useDispatch()

//     const requestUserPermission = async () => {
//         const authStatus = await messaging().requestPermission({
//             alert: true,
//             announcement: false,
//             badge: true,
//             carPlay: true,
//             provisional: false,
//             sound: true,
//         });
//         const enabled =
//             authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
//             authStatus === messaging.AuthorizationStatus.PROVISIONAL;

//         if (enabled) {
//             getFcmToken()
//             console.log('Firebase Authorization status:', authStatus);
//         }
//     }

//     const getFcmToken = async () => {
//         const fcmToken = await messaging().getToken();
//         if (fcmToken) {
//             console.log("Your Firebase Token is:", fcmToken);
//             await AsyncStorage.setItem('fcmToken', fcmToken + '')
//         } else {
//             console.log("Failed", "No token received");
//         }
//     }


//     useEffect(() => {

//         //Foreground Handler,when the app is open or in view
//         const unsubscribe = messaging().onMessage((remoteMessage) => {
//             console.log("handle in foreground", remoteMessage)
//             const { notification, messageId } = remoteMessage

//             if (Platform.OS == 'ios') {
//                 PushNotificationIOS.addNotificationRequest({
//                     id: messageId,
//                     body: notification.body,
//                     title: notification.title,
//                     sound: 'default',

//                 });


//             } else {
//                 PushNotification.localNotification({
//                     channelId: "your-channel-id",
//                     id: messageId,
//                     body: notification.body,
//                     title: 'android notif title',
//                     soundName: 'default',
//                     vibrate: true,
//                     playSound: true
//                 })
//             }

//             // NavigationService.navigate('Inbox')
//             //handlePushNotification(dispatch, remoteMessage?.data?.title, remoteMessage?.data?.subtitle, remoteMessage?.data?.id)
//             AsyncStorage.setItem("push_notification_data", JSON.stringify(remoteMessage.data));

//         })

//         return unsubscribe
//     }, [])

//     useEffect(() => {
//         //Background Handler,when the app is in background
//         const unsubscribe = messaging().onNotificationOpenedApp(remoteMessage => {
//             console.log("Remote Message (onNotificationOpenedApp) ", remoteMessage)

//             NavigationService.navigate('Inbox')
//             // handlePushNotification(dispatch, remoteMessage?.data?.title, remoteMessage?.data?.subtitle, remoteMessage?.data?.id)
//         });
//         return unsubscribe
//     }, [])

//     useEffect(() => {
//         //Quit App Handler,when the app is quit
//         const unsubscribe = messaging().getInitialNotification()
//             .then(remoteMessage => {
//                 console.log("Remote Message (getInitialNotification)", remoteMessage)
//                 if (remoteMessage) {
//                     var itr = 0

//                     var navigate = setInterval(() => {

//                         itr += 1

//                         NavigationService.navigate('Inbox')
//                         if (itr > 5)

//                             clearInterval(navigate)

//                     }, 1000)

//                     //  handlePushNotification(dispatch, remoteMessage?.data?.title, remoteMessage?.data?.subtitle, remoteMessage?.data?.id)
//                 }
//             });

//         return unsubscribe
//     }, [])



//     useEffect(() => {
//         requestUserPermission();
//     }, [])

//     return null
// }

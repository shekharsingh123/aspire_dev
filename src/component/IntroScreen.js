//import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';



import { Platform, Text, StyleSheet, View, TouchableOpacity, SafeAreaView, ImageBackground, Image } from 'react-native'

import { hpx, wpx, Colors, nf, Fonts } from '../constants/constants';
let bg = require('../assets/images/splash.png');
let logo = require('../assets/images/logo.png');



export default function IntroScreen(props) {
    return (
        <ImageBackground source={bg}
            style={styles.MainContainer} >

            <View style={styles.card}>
                <Image
                    source={logo}
                    style={styles.logoStyle}></Image>
            </View>
            <View
                style={{
                    flexDirection: 'row',
                    height: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    position: 'absolute', //Here is the trick
                    bottom: 15,


                }}>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('SelectionScreen')}
                    style={{
                        ...styles.button,
                        backgroundColor: '#FFFFFF',
                        borderWidth: 1,
                        borderColor: '#707070',
                    }}>
                    <Text style={{ ...styles.buttonText, color: '#2E7847', }}>
                        Sign in as Guest
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('Login')}
                    style={{
                        ...styles.button,
                        backgroundColor: '#FFFFFF',
                        borderWidth: 1,
                        borderColor: '#707070',
                    }}>
                    <Text style={{ ...styles.buttonText, color: '#2E7847', }}>Login | Sign Up</Text>
                </TouchableOpacity>
            </View>

        </ImageBackground>

    );
}


const styles = StyleSheet.create({
    MainContainer: {
        flex: 1, justifyContent: 'center'
    },
    logoStyle: {
        width: wpx(400),
        height: hpx(600)
    },
    card: {
        backgroundColor: Colors.white,
        width: wpx(400),
        height: hpx(150),
        elevation: 2,
        shadowColor: Colors.shadow,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        justifyContent: 'center',
        marginTop: hpx(20),
        marginBottom: hpx(20),
        borderRadius: wpx(3)
    },
    button: {
        height: hpx(42),
        width: wpx(154),
        margin: 20,
        backgroundColor: '#D80B8C',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 11,
        //marginTop: hpx(26)
    },
    buttonText: {
        fontSize: nf(16),
        fontFamily: Fonts.tofinoSemi,

    },

});


import { ScrollView, TouchableOpacity, Text, Dimensions, View, Animated, StyleSheet, Image } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import SafeAreaView from 'react-native-safe-area-view';
const DATA = [{ id: 1, title: 'The Hunger Games' }, { id: 2, title: 'Harry Potter and the Order of the Phoenix' }, { id: 3, title: 'To Kill a Mockingbird' }, { id: 4, title: 'Pride and Prejudice' }, { id: 5, title: 'Twilight' },
{ id: 6, title: 'The Book Thief' }, { id: 7, title: 'The Chronicles of Narnia' }, { id: 8, title: 'Animal Farm' }, { id: 9, title: 'Gone with the Wind' }, { id: 10, title: 'The Shadow of the Wind' }, { id: 11, title: 'The Fault in Our Stars' }, { id: 12, title: "The Hitchhiker's Guide to the Galaxy" }, { id: 13, title: 'The Giving Tree' }, { id: 14, title: 'Wuthering Heights' }, { id: 15, title: 'The Da Vinci Code' }];
import AnimatedHeader from './DailyLimitHeader';
import { hpx, wpx, nf, hp, Fonts, Colors } from '../../constants/constants';
import ToggleSwitch from 'toggle-switch-react-native'
let logo = require('../.././assets/images/logo.png');
let timer = require('../.././assets/images/timer.png');
let backIcon = require('../.././assets/images/backArrow.png');
import ScrollBottomSheet from 'react-native-scroll-bottom-sheet';
import React, { useState, useEffect, useRef } from 'react';
const windowHeight = Dimensions.get('window').height;
import { types } from '../../store/ActionTypes';
import ProgressLoader from 'rn-progress-loader';
import { showMessage } from 'react-native-flash-message';

import { useDispatch, shallowEqual, useSelector } from 'react-redux';
export default function FilterProduct({ navigation }) {
    const { loaderVisible, } = useSelector(
        (state) => ({
            loaderVisible: state.globalReducer.loader,



        }),
        shallowEqual,
    );
    const [loader, setloader] = useState(loaderVisible);
    const offset = useRef(new Animated.Value(0)).current;
    const [negotiation, setNegotiation] = useState(0);
    const [freezeCard, setfreezeCard] = useState(0);
    const [cardNumber, showCardNomber] = useState(false);
    const [Amount, setAmount] = useState();
    const dispatch = useDispatch();
    useEffect(() => {
        console.log("loader", loaderVisible)
        setloader(loaderVisible)
    }, [loaderVisible]);
    const SetWeekLimit = () => {
        if (Amount == undefined) {
            showMessage({
                message: "Choose Price",
                type: "danger",
                // background color
                color: "#fafafa", // text color
                titleStyle: { textAlign: 'center' },
            });
        } else {
            dispatch({
                type: types.TOGGLE_LOADING,
                payload: true,
            });
            dispatch({
                type: types.WEEK_LIMIT,
                payload: Amount,
            });
        }

    }
    return (

        <SafeAreaView style={{ flex: 1, width: '100%', flexDirection: 'column', backgroundColor: '#0C365A', }}>
            <ProgressLoader
                visible={loader}
                barHeight={100}
                isModal={true} isHUD={false}
                hudColor={"#5DD986"}
                color={"#FFFFFF"} />
            <View style={styles.commonHeaderView}>
                <TouchableOpacity
                    activeOpacity={1.0}
                    style={styles.backIconTouch}
                    onPress={() => navigation.goBack()}>
                    <Image
                        source={backIcon}
                        style={styles.backArrow}
                        resizeMode="contain"
                    />
                </TouchableOpacity>
                <Image
                    source={logo}
                    style={styles.logo}
                    resizeMode="contain"
                />
            </View>

            <View style={{ flex: 1, marginTop: hpx(20), width: '100%', alignItems: 'flex-start', backgroundColor: '#0C365A', height: '30%', }}>
                <Text allowFontScaling={false} style={styles.heading}>
                    Spending limit
                </Text>
                <View style={{ flex: 1, marginTop: hpx(32), width: '100%', alignItems: 'flex-start', backgroundColor: '#fff', height: '60%', borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
                    <View style={{ flex: 1, width: '100%', marginTop: hpx(32), }}>
                        <View style={styles.body}>
                            <Image source={timer}
                                style={styles.eyeImage}>

                            </Image>
                            <Text allowFontScaling={false} style={{ ...styles.subHeading, marginStart: wpx(10) }}>
                                Set a weekly debit card spending limit
                            </Text>
                        </View>
                        <View style={{ ...styles.body, marginTop: hpx(20), height: hpx(50) }}>
                            <View style={{ flex: 1, flexDirection: 'row', height: hpx(30), }}>
                                <View style={styles.designerNameView}>
                                    <Text allowFontScaling={false} style={{ ...styles.subheading2, }}>
                                        5$
                                    </Text>
                                </View>

                                <Text allowFontScaling={false} style={{ ...styles.heading, color: 'black', marginStart: wpx(10) }}>
                                    {Amount == 20000 ? '20,000' : Amount == 10000 ? '10,000' : Amount == 5000 ? '5,000' : ''}
                                </Text>
                            </View>

                        </View>
                        <View style={{ width: '80%', alignItems: 'center', alignSelf: 'center', height: 2, backgroundColor: '#0000001F', top: -14 }}></View>
                        <View style={styles.body}>

                            <Text allowFontScaling={false} style={{ ...styles.subheading2, color: '#22222266', }}>
                                Here weekly means the last 7 days - not the calendar week
                            </Text>
                        </View>
                        <View style={{ ...styles.body, marginTop: hpx(20), height: hpx(50) }}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', height: hpx(30), }}>

                                <TouchableOpacity
                                    activeOpacity={1}

                                    onPress={() => setAmount(5000)}>
                                    <View style={styles.NameText}>
                                        <Text allowFontScaling={false} style={{ ...styles.subheading2, color: '#01D167', }}>
                                            S$ 5,000
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    activeOpacity={1}

                                    onPress={() => setAmount(10000)}>
                                    <View style={styles.NameText}>
                                        <Text allowFontScaling={false} style={{ ...styles.subheading2, color: '#01D167', }}>
                                            S$ 10,000
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    activeOpacity={1}

                                    onPress={() => setAmount(20000)}>
                                    <View style={styles.NameText}>
                                        <Text allowFontScaling={false} style={{ ...styles.subheading2, color: '#01D167', }}>
                                            S$ 20,000
                                        </Text>
                                    </View>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </View>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={{
                            ...styles.button,
                            backgroundColor: Amount ? '#01D167' : '#EEEEEE',
                        }}
                        onPress={() => SetWeekLimit()}>
                        <Text allowFontScaling={false} style={{ ...styles.buttonText, color: '#FFFFFF' }}>
                            Save
                        </Text>
                    </TouchableOpacity>
                </View>

            </View>



        </SafeAreaView >
    );

} const styles = StyleSheet.create({

    searchBox: {
        height: hpx(49),
        //width: wpx(340),
        backgroundColor: '#5DD986',
        borderRadius: hpx(7),
        borderColor: '#E4E6E772',
        marginTop: hpx(5),
        flexDirection: 'row',

        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: wpx(10),
    },
    heading: {
        marginStart: wpx(20),
        fontSize: nf(24),
        color: Colors.white,
        fontFamily: Fonts.avenir_next_bold,
        textAlign: 'center',
        bottom: hpx(10)
    },
    headingReview: {

        fontSize: nf(15),

        fontFamily: Fonts.begum,
        textAlign: 'center',
    },
    logo: {
        tintColor: 'green',
        width: wpx(25.59), height: hpx(25),

    },
    backArrow: {
        width: wpx(20),
        height: hpx(20),
        tintColor: 'white'
    },
    address: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
        alignContent: 'center',
        bottom: hpx(25)
    },
    subheading2: {
        fontSize: nf(12),
        color: Colors.white,
        fontFamily: Fonts.avenir_next_medium,
        textAlign: 'center',

    },
    commonHeaderView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: hpx(53),
        width: wpx(375),
        backgroundColor: '#0C365A',
        paddingHorizontal: wpx(21),
    },
    location_style: {
        flex: 1,
        justifyContent: 'center',
        height: hpx(126),
        margin: 20, bottom: 1
    },

    designerImage: {
        width: wpx(105),
        height: hpx(106),
        //    backgroundColor: 'black'


    },
    designerNameView: {

        bottom: hpx(5),
        justifyContent: 'center',
        width: wpx(40),
        height: hpx(22),
        borderRadius: 5,
        alignItems: 'center',
        alignContent: 'center',


        borderColor: Colors.grey,
        elevation: 1,
        shadowColor: '#00000029',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        backgroundColor: '#01D167',
    },
    NameText: {
        width: wpx(104),
        height: hpx(40),
        bottom: hpx(5),
        justifyContent: 'center',

        borderRadius: 5,
        alignItems: 'center',
        alignContent: 'center',


        borderColor: Colors.grey,
        elevation: 1,
        shadowColor: '#E5F6DF',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 7,
        shadowRadius: 3,
        backgroundColor: '#e2f7eb',

    },

    cart_button: {
        height: hpx(43),
        width: wpx(115),

        alignContent: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
        //marginTop: hpx(26)
    },
    body: {


        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignContent: 'flex-start',
        alignItems: 'center',

        width: '100%',

        paddingHorizontal: hpx(20),

    },
    buttonText: {
        fontSize: nf(16),
        fontFamily: Fonts.avenir_next_DemiBold,


    },
    button: {
        width: wpx(300),
        height: hpx(56),

        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: wpx(30),
        marginVertical: hpx(30),
    },
    line: {
        width: wpx(159),
        marginTop: hpx(20),

        justifyContent: 'center',
        alignItems: 'center',

        //marginTop: hpx(26)
    },
    review: {

        alignContent: 'center',
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
        top: -6,
        width: wpx(346),
        height: hpx(220),
        backgroundColor: '#01D167',
        borderColor: '#D3EEDC',
        shadowColor: '#707070',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        borderRadius: 10,

    },
    profileImage: {
        width: hpx(21),
        height: wpx(21),

        borderRadius: 20,
    },
    profileImage1: {
        width: hpx(32),
        height: wpx(32),

        borderRadius: 20,
    },
    visaImage: {
        width: hpx(59),
        height: wpx(20),

    },
    eyeImage: {
        width: hpx(16),
        height: wpx(16),
        tintColor: 'black'

    },
    card1: {

        height: '50%',
        width: '100%',

        flexDirection: 'column',
        paddingHorizontal: hpx(20),

        alignItems: 'flex-start',

    },
    card2: {

        alignContent: 'center',
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'flex-end',
        right: 15,

        width: wpx(151),
        height: hpx(36),
        backgroundColor: '#ffffff',
        borderColor: '#EEEEEE',
        shadowColor: '#EEEEEE',
        shadowOffset: { width: 0, height: 1 },
        borderWidth: 2,


        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,

    },
    card3: {
        top: hpx(-30),

        right: wpx(-30),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: hpx(25),
    },
    cardText1: { fontFamily: Fonts.medium, fontSize: nf(16), color: Colors.white, },
    cardText2: {
        fontFamily: Fonts.avenir_next_medium,
        fontSize: nf(22),
        color: Colors.white,
        marginLeft: wpx(10),

        fontWeight: "bold"
    },
    cardText3: {
        fontFamily: Fonts.avenir_next_DemiBold,
        fontSize: nf(12),
        marginLeft: wpx(10),
        color: Colors.white,

    },
    cardText4: {
        fontFamily: Fonts.book_font,
        fontSize: nf(12),
        color: Colors.white,
    },
    cardText5: {
        fontFamily: Fonts.book_font,
        fontSize: nf(12),
        color: Colors.white,
        marginTop: hpx(12),
    },
    SectionStyle: {
        flex: 1,
        marginStart: wpx(25),
        marginEnd: wpx(30),
        marginTop: wpx(20),
        flexDirection: 'row',
        justifyContent: 'flex-start',




    },
});
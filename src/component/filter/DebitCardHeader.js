import React, { Component, useEffect, useCallback, useState } from 'react';
import { Platform, Text, StyleSheet, Image, Dimensions, ScrollView, FlatList, TextInput, Animated, TouchableOpacity, View, SafeAreaViewBase, SafeAreaView, ImageBackground, } from 'react-native'
import { hpx, wpx, nf, hp, Fonts, Colors } from '../../constants/constants';

let logo = require('../.././assets/images/logo.png');
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';

import ProgressLoader from 'rn-progress-loader';
import { types } from '../../store/ActionTypes';
import * as RootNavigation from '../../util/RootNavigation'
import { useDispatch, shallowEqual, useSelector } from 'react-redux';

import { useSafeAreaInsets } from 'react-native-safe-area-context';

const HEADER_HEIGHT = 200;

const AnimatedHeader = ({ animatedValue, navigation }) => {
    const insets = useSafeAreaInsets();

    const headerHeight = animatedValue.interpolate({
        inputRange: [0, HEADER_HEIGHT + insets.top],
        outputRange: [HEADER_HEIGHT + insets.top, insets.top + 44],
        extrapolate: 'clamp'
    });

    return (
        <Animated.View
            style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                zIndex: 10,
                height: headerHeight,
                backgroundColor: '#0C365A'
            }}
        >
            <View style={{ flexDirection: 'row', alignContent: 'center', marginTop: hpx(10) }}>
                <View style={styles.commonHeaderView}>

                    <Text allowFontScaling={false} style={styles.heading}>
                        Debit Card
                    </Text>
                </View>

                <View style={styles.location_style}>
                    <View style={styles.address}>
                        <Image source={logo} style={styles.logo}>
                        </Image>
                    </View>

                </View>

            </View>
            <View style={{ flex: 1, width: '100%' }}>
                <View style={styles.body}>
                    <Text allowFontScaling={false} style={styles.subHeading}>
                        Available balance
                    </Text>
                </View>
                <View style={{ ...styles.body, marginTop: hpx(20), height: hpx(50) }}>
                    <View style={{ flex: 1, flexDirection: 'row', height: hpx(30), }}>
                        <View style={styles.designerNameView}>
                            <Text allowFontScaling={false} style={{ ...styles.subheading2, }}>
                                S$
                            </Text>
                        </View>

                        <Text allowFontScaling={false} style={{ ...styles.heading, marginStart: wpx(10) }}>
                            3,000
                        </Text>
                    </View>
                </View>

            </View>
        </Animated.View>
    );
};
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#0C365A',
        justifyContent: 'flex-start',
        alignContent: 'flex-start',
        alignItems: 'flex-start'
        // alignSelf:'flex-start'
    },


    heading: {
        fontSize: nf(24),
        color: Colors.white,
        fontFamily: Fonts.avenir_next_bold,
        textAlign: 'center',
        bottom: hpx(10)
    },
    subHeading: {
        fontSize: nf(14),
        color: Colors.white,
        fontFamily: Fonts.avenir_next_medium,
        textAlign: 'center',

    },
    subheading2: {
        fontSize: nf(12),
        color: Colors.white,
        fontFamily: Fonts.avenir_next_medium,
        textAlign: 'center',

    },
    body: {

        justifyContent: 'flex-start',
        alignContent: 'flex-start',
        alignItems: 'flex-start',
        width: '100%',

        paddingHorizontal: hpx(20),

    },
    logo: {
        width: wpx(25.59), height: hpx(25),

    },
    backArrow: {
        width: wpx(16),
        height: hpx(16),
    },
    address: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
        alignContent: 'center',
        bottom: hpx(25)
    },
    selectInfo: {

        marginLeft: wpx(10),
        fontSize: nf(14),
        fontFamily: Fonts.tofino_pro_personal_book,
        padding: 2,
    },
    commonHeaderView: {

        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingHorizontal: wpx(21),
        marginTop: hpx(15)
    },
    location_style: {
        flex: 1,
        justifyContent: 'center',
        height: hpx(100),
        paddingHorizontal: wpx(21),
        bottom: 1
    },

    designerImage: {
        width: wpx(105),
        height: hpx(106),
        //    backgroundColor: 'black'


    },
    designerNameView: {
        bottom: Platform.OS == 'ios' ? hpx(5) : hpx(0),
        justifyContent: 'center',
        width: wpx(40),
        height: hpx(22),
        borderRadius: 5,
        alignItems: 'center',
        alignContent: 'center',


        borderColor: Colors.grey,
        elevation: 1,
        shadowColor: '#00000029',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        backgroundColor: '#01D167',
    },
    designerNameText: {

        top: 2,
        fontSize: nf(12),
        fontFamily: Fonts.tofino_pro_personal_book,
        textAlign: 'center',
        textAlignVertical: 'center',
        color: Colors.black,

    },
});
export default AnimatedHeader;
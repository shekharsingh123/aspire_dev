
import { ScrollView, TouchableOpacity, Text, Dimensions, View, Animated, StyleSheet, Image } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import SafeAreaView from 'react-native-safe-area-view';
const DATA = [{ id: 1, title: 'The Hunger Games' }, { id: 2, title: 'Harry Potter and the Order of the Phoenix' }, { id: 3, title: 'To Kill a Mockingbird' }, { id: 4, title: 'Pride and Prejudice' }, { id: 5, title: 'Twilight' },
{ id: 6, title: 'The Book Thief' }, { id: 7, title: 'The Chronicles of Narnia' }, { id: 8, title: 'Animal Farm' }, { id: 9, title: 'Gone with the Wind' }, { id: 10, title: 'The Shadow of the Wind' }, { id: 11, title: 'The Fault in Our Stars' }, { id: 12, title: "The Hitchhiker's Guide to the Galaxy" }, { id: 13, title: 'The Giving Tree' }, { id: 14, title: 'Wuthering Heights' }, { id: 15, title: 'The Da Vinci Code' }];
import AnimatedHeader from './DebitCardHeader';
import { hpx, wpx, nf, hp, Fonts, Colors } from '../../constants/constants';
import ToggleSwitch from 'toggle-switch-react-native'
let logo = require('../.././assets/images/Home.png');
let visa = require('../.././assets/images/visa.png');
let weekly = require('../.././assets/images/weekly.png');
let topup = require('../.././assets/images/topup.png');
let newcard = require('../.././assets/images/newcard.png');
let deactive = require('../.././assets/images/deactive.png');
let freeze = require('../.././assets/images/freeze.png');
let eye = require('../.././assets/images/remove_eye.png');
let disableEye = require('../.././assets/images/disableEye.png');
import ScrollBottomSheet from 'react-native-scroll-bottom-sheet';
import React, { useState, useEffect, useRef } from 'react';
import { navigationRef } from '../../util/RootNavigation';
const windowHeight = Dimensions.get('window').height;
import { showMessage } from 'react-native-flash-message';
export default function FilterCat({ navigation }) {
    const offset = useRef(new Animated.Value(0)).current;
    const [negotiation, setNegotiation] = useState(false);
    const [freezeCard, setfreezeCard] = useState(false);
    const [cardNumber, showCardNomber] = useState(false);
    return (
        <SafeAreaView style={{ flex: 1, flexDirection: 'column', }}>
            <View>
                <AnimatedHeader animatedValue={offset} />
            </View>
            <ScrollBottomSheet
                componentType="ScrollView"

                snapPoints={[60, '20%', windowHeight - 620]}
                initialSnapIndex={2}
                renderHandle={() => (
                    <View style={styles.header}>
                        <View style={styles.panelHandle} />
                    </View>
                )}

            >
                <TouchableOpacity
                    activeOpacity={1.0}
                    onPress={() => showCardNomber(!cardNumber)}>
                    <View style={styles.card2}>

                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: '100%' }}>
                            {
                                cardNumber ?
                                    <Image
                                        source={disableEye}
                                        style={styles.eyeImage}
                                        resizeMode="cover"
                                    />
                                    : <Image
                                        source={eye}
                                        style={styles.eyeImage}
                                        resizeMode="cover"
                                    />
                            }

                            {
                                cardNumber ?
                                    <Text style={{ ...styles.cardText3, color: '#01D167' }}>Hide card number</Text>
                                    : <Text style={{ ...styles.cardText3, color: '#01D167' }}>Show card number</Text>
                            }


                        </View>



                    </View>
                </TouchableOpacity>
                <View style={styles.review}>

                    <View style={{ flex: 1, height: 30, flexDirection: 'row', padding: hpx(20), justifyContent: 'flex-end', width: '100%' }}>
                        <Image
                            source={logo}
                            style={styles.profileImage}
                            resizeMode="cover"
                        />

                        <Text style={styles.cardText1}>aspire</Text>

                    </View>
                    <View style={styles.card1}>
                        <Text style={styles.cardText2}>Mark Henry</Text>
                        {
                            cardNumber ?
                                <Text style={{ ...styles.cardText1, marginTop: hpx(24), marginLeft: wpx(10), }}>5647    5647    5647    5647 </Text> :
                                <Text style={{ ...styles.cardText1, marginTop: hpx(24), marginLeft: wpx(10), }}>****   ****    ****    5647 </Text>
                        }
                        {
                            cardNumber ?
                                <Text style={{ ...styles.cardText3, marginTop: hpx(15), }}>Thru: 12/20        CVV: 456 </Text>
                                :
                                <Text style={{ ...styles.cardText3, marginTop: hpx(15), }}>Thru: 12/20        CVV: *** </Text>

                        }


                    </View>

                    <View style={{ flex: 1, height: 30, flexDirection: 'row', padding: hpx(20), justifyContent: 'flex-end', width: '100%' }}>
                        <Image
                            source={visa}
                            style={styles.visaImage}
                            resizeMode="cover"
                        />



                    </View>


                </View>
                <View style={{ paddingHorizontal: hpx(20), paddingBottom: hpx(20) }}>


                    <View style={{ marginTop: hpx(34), flex: 1, alignItems: 'center', flexDirection: 'row', height: hpx(41), width: '100%', justifyContent: 'space-between', alignContent: 'center', alignSelf: 'center' }}>
                        <Image
                            source={topup}
                            style={styles.profileImage1}
                            resizeMode="cover"
                        />

                        <View style={{ flex: 1, marginStart: wpx(14), alignItems: 'flex-start', flexDirection: 'column', height: hpx(41), width: '100%', justifyContent: 'space-between', alignContent: 'flex-start', alignSelf: 'flex-start' }}>
                            <Text style={{ fontSize: nf(14), color: '#222222', fontFamily: Fonts.avenir_next_medium }}>Top-up account </Text>
                            <Text style={{ fontSize: nf(13), color: '#22222266', fontFamily: Fonts.avenir_next_Regular }}>Deposit money to your account to use with card </Text>

                        </View>
                    </View>


                    <View style={{ marginTop: hpx(34), flex: 1, alignItems: 'center', flexDirection: 'row', height: hpx(41), width: '100%', justifyContent: 'space-between', alignContent: 'center', alignSelf: 'center' }}>
                        <Image
                            source={weekly}
                            style={styles.profileImage1}
                            resizeMode="cover"
                        />

                        <View style={{ flex: 1, marginStart: wpx(14), alignItems: 'flex-start', flexDirection: 'column', height: hpx(41), width: '100%', justifyContent: 'space-between', alignContent: 'flex-start', alignSelf: 'flex-start' }}>
                            <Text style={{ fontSize: nf(14), color: '#222222', fontFamily: Fonts.avenir_next_medium }}>Weekly spending limit </Text>
                            <Text style={{ fontSize: nf(13), color: '#22222266', fontFamily: Fonts.avenir_next_Regular }}>You haven’t set any spending limit on card </Text>

                        </View>
                        <ToggleSwitch
                            isOn={negotiation}
                            onColor={'#01D167'}
                            offColor={Colors.grey}
                            size="small"
                            onToggle={() => {
                                negotiation ? setNegotiation(false) : setNegotiation(true), navigation.navigate('FilterProduct')


                            }

                            }
                        />
                    </View>
                    <View style={{ marginTop: hpx(34), flex: 1, alignItems: 'center', flexDirection: 'row', height: hpx(41), width: '100%', justifyContent: 'space-between', alignContent: 'center', alignSelf: 'center' }}>
                        <Image
                            source={freeze}
                            style={styles.profileImage1}
                            resizeMode="cover"
                        />

                        <View style={{ flex: 1, marginStart: wpx(14), alignItems: 'flex-start', flexDirection: 'column', height: hpx(41), width: '100%', justifyContent: 'space-between', alignContent: 'flex-start', alignSelf: 'flex-start' }}>
                            <Text style={{ fontSize: nf(14), color: '#222222', fontFamily: Fonts.avenir_next_medium }}>Freeze card</Text>
                            <Text style={{ fontSize: nf(13), color: '#22222266', fontFamily: Fonts.avenir_next_Regular }}>Your debit card is currently active</Text>

                        </View>
                        <ToggleSwitch
                            isOn={freezeCard}
                            onColor={'#01D167'}
                            offColor={Colors.grey}
                            size="small"
                            onToggle={() =>
                                freezeCard ? setfreezeCard(false) : setfreezeCard(true)
                            }
                        />
                    </View>
                    <View style={{ marginTop: hpx(34), flex: 1, alignItems: 'center', flexDirection: 'row', height: hpx(41), width: '100%', justifyContent: 'space-between', alignContent: 'center', alignSelf: 'center' }}>
                        <Image
                            source={newcard}
                            style={styles.profileImage1}
                            resizeMode="cover"
                        />

                        <View style={{ flex: 1, marginStart: wpx(14), alignItems: 'flex-start', flexDirection: 'column', height: hpx(41), width: '100%', justifyContent: 'space-between', alignContent: 'flex-start', alignSelf: 'flex-start' }}>
                            <Text style={{ fontSize: nf(14), color: '#222222', fontFamily: Fonts.avenir_next_medium }}>Get a new card</Text>
                            <Text style={{ fontSize: nf(13), color: '#22222266', fontFamily: Fonts.avenir_next_Regular }}>This deactivates your current debit card</Text>

                        </View>

                    </View>
                    <View style={{ marginTop: hpx(34), flex: 1, alignItems: 'center', flexDirection: 'row', height: hpx(41), width: '100%', justifyContent: 'space-between', alignContent: 'center', alignSelf: 'center' }}>
                        <Image
                            source={deactive}
                            style={styles.profileImage1}
                            resizeMode="cover"
                        />

                        <View style={{ flex: 1, marginStart: wpx(14), alignItems: 'flex-start', flexDirection: 'column', height: hpx(41), width: '100%', justifyContent: 'space-between', alignContent: 'flex-start', alignSelf: 'flex-start' }}>
                            <Text style={{ fontSize: nf(14), color: '#222222', fontFamily: Fonts.avenir_next_medium }}>Deactivated cards</Text>
                            <Text style={{ fontSize: nf(13), color: '#22222266', fontFamily: Fonts.avenir_next_Regular }}>Your previously deactivated cards</Text>

                        </View>

                    </View>
                </View>
            </ScrollBottomSheet>
        </SafeAreaView>
    );

} const styles = StyleSheet.create({

    searchBox: {
        height: hpx(49),
        //width: wpx(340),
        backgroundColor: '#5DD986',
        borderRadius: hpx(7),
        borderColor: '#E4E6E772',
        marginTop: hpx(5),
        flexDirection: 'row',

        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: wpx(10),
    },
    heading: {
        fontSize: nf(15),
        marginHorizontal: wpx(6),
        fontFamily: Fonts.begum,
        textAlign: 'center',
    },
    headingReview: {
        fontSize: nf(15),

        fontFamily: Fonts.begum,
        textAlign: 'center',
    },
    backArrow: {
        width: wpx(16),
        height: hpx(16),
    },
    address: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
        alignContent: 'center',
        bottom: hpx(25)
    },
    selectInfo: {

        marginLeft: wpx(10),
        fontSize: nf(14),
        fontFamily: Fonts.tofino_pro_personal_book,
        padding: 2,
    },
    // header: {
    //     alignItems: 'center',
    //     backgroundColor: 'white',
    //     paddingVertical: 20,
    //     borderTopLeftRadius: 20,
    //     borderTopRightRadius: 20
    // },
    // panelHandle: {
    //     width: 40,
    //     height: 2,
    //     backgroundColor: 'rgba(0,0,0,0.3)',
    //     borderRadius: 4
    // },
    commonHeaderView: {

        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: hpx(53),
        top: 20,
        width: wpx(175),
        paddingHorizontal: wpx(21),
    },
    location_style: {
        flex: 1,
        justifyContent: 'center',
        height: hpx(126),
        margin: 20, bottom: 1
    },

    designerImage: {
        width: wpx(105),
        height: hpx(106),
        //    backgroundColor: 'black'


    },
    designerNameView: {

        justifyContent: 'center',
        width: wpx(105),
        height: hpx(131),
        borderRadius: 5,
        alignItems: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        marginHorizontal: 8,
        borderColor: Colors.grey,
        marginTop: hpx(20),

        elevation: 1,
        shadowColor: '#00000029',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        backgroundColor: 'white',
    },
    designerNameText: {

        top: 2,
        fontSize: nf(12),
        fontFamily: Fonts.tofino_pro_personal_book,
        textAlign: 'center',
        textAlignVertical: 'center',
        color: Colors.black,

    },
    button: {
        height: hpx(35),
        width: wpx(68),

        alignContent: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 6,
        //marginTop: hpx(26)
    },
    cart_button: {
        height: hpx(43),
        width: wpx(115),

        alignContent: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
        //marginTop: hpx(26)
    },
    inputStyle: {
        borderBottomColor: '#707070', // Add this to specify bottom border color
        width: wpx(320),
        height: hpx(140),
        borderBottomWidth: 0.5,
        borderBottomColor: '#5DD986',
        borderWidth: 0.5,
        paddingStart: hpx(10),
        borderColor: '#5DD986',
        fontSize: nf(22),
        color: '#5DD986',



        fontFamily: Fonts.tofinoSemi,

    },
    buttonText: {
        fontSize: nf(20),
        fontFamily: Fonts.tofinoSemi,

    },
    button1: {
        height: hpx(42),
        width: wpx(139),

        shadowColor: '#707070',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        //marginTop: hpx(26)
    },
    line: {
        width: wpx(159),
        marginTop: hpx(20),

        justifyContent: 'center',
        alignItems: 'center',

        //marginTop: hpx(26)
    },
    review: {

        alignContent: 'center',
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
        top: -6,
        width: wpx(346),
        height: hpx(220),
        backgroundColor: '#01D167',
        borderColor: '#D3EEDC',
        shadowColor: '#707070',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        borderRadius: 10,

    },
    profileImage: {
        width: hpx(21),
        height: wpx(21),

        borderRadius: 20,
    },
    profileImage1: {
        width: hpx(32),
        height: wpx(32),

        borderRadius: 20,
    },
    visaImage: {
        width: hpx(59),
        height: wpx(20),

    },
    eyeImage: {
        width: hpx(16),
        height: wpx(16),
        tintColor: '#01D167'

    },
    card1: {

        height: '50%',
        width: '100%',

        flexDirection: 'column',
        paddingHorizontal: hpx(20),

        alignItems: 'flex-start',

    },
    card2: {

        alignContent: 'center',
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'flex-end',
        right: 15,

        width: wpx(151),
        height: hpx(36),
        backgroundColor: '#ffffff',
        borderColor: '#EEEEEE',
        shadowColor: '#EEEEEE',
        shadowOffset: { width: 0, height: 1 },
        borderWidth: 2,


        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,

    },
    card3: {
        top: hpx(-30),

        right: wpx(-30),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: hpx(25),
    },
    cardText1: { fontFamily: Fonts.medium, fontSize: nf(16), color: Colors.white, },
    cardText2: {
        fontFamily: Fonts.avenir_next_medium,
        fontSize: nf(22),
        color: Colors.white,
        marginLeft: wpx(10),

        fontWeight: "bold"
    },
    cardText3: {
        fontFamily: Fonts.avenir_next_DemiBold,
        fontSize: nf(12),
        marginLeft: wpx(10),
        color: Colors.white,

    },
    cardText4: {
        fontFamily: Fonts.book_font,
        fontSize: nf(12),
        color: Colors.white,
    },
    cardText5: {
        fontFamily: Fonts.book_font,
        fontSize: nf(12),
        color: Colors.white,
        marginTop: hpx(12),
    },
    SectionStyle: {
        flex: 1,
        marginStart: wpx(25),
        marginEnd: wpx(30),
        marginTop: wpx(20),
        flexDirection: 'row',
        justifyContent: 'flex-start',




    },
});
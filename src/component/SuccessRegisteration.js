//import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';



import { Platform, Text, StyleSheet, View, TouchableOpacity, TextInput, ImageBackground, Image, ScrollView } from 'react-native'

import { hpx, wpx, Colors, nf, Fonts } from '../constants/constants';
let bg = require('../assets/images/splash.png');
let logo = require('../assets/images/logo.png');
let eyeOn = require('../assets/images/eyeOn.png')
let eyeOff = require('../assets/images/eyeOff.png')


export default function SuccessRegisteration(props) {
    const [isVisible, setIsVisible] = useState(true);
    const [password, setPassword] = useState('');
    return (
        <ScrollView style={styles.MainContainer}>

      
        <View 
            style={styles.MainContainer} >

            <View style={styles.card}>
                <Image
                    source={logo}
                    style={styles.logoStyle}></Image>
                
               
            </View>
            <View style={{flex:1,margin:wpx(18)}}>
                <Text style={styles.welcomeStyle}>
                  Congratulations!

                </Text>

                    <Text style={{ ...styles.startedStyle, marginTop:wpx(30),marginEnd:wpx(60) }}>
                        You can now continue shopping on Springs 15 Marketplace! Have fun!

                    </Text>
                </View>
               
                <View style={{ flex: 1, justifyContent: 'flex-start', margin: wpx(18),flexDirection:'row'}}>
                
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('Login')}
                    style={{
                        ...styles.button,
                        bottom:15,
                        backgroundColor: '#2DA461',
                       
                        borderColor: '#707070',
                    }}>
                    <Text style={{ ...styles.buttonText, color: 'white', }}>Shop Now</Text>
                </TouchableOpacity>
                </View>
                {/* <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row',marginRight:wpx(30) }}>

                    <TouchableOpacity
                        onPress={() => props.navigation.navigate('Login')}
                       >
                        <Text style={{ fontFamily: Fonts.pangram_regular, color: '#707070', marginTop: hpx(10), fontSize: nf(15),right:10 }}>or Sign Up</Text>
                    </TouchableOpacity>
                </View> */}
        </View>
        </ScrollView>
    );
}


const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor:'#F8FBF9'
    },
    SectionStyle: {
        flex: 1,
        marginStart: wpx(25),
        marginEnd: wpx(30),
        marginTop:wpx(20),
        flexDirection: 'row',
        justifyContent: 'flex-start',
       
  
       
      
    },
    inputStyle: {
     borderBottomColor: '#707070', // Add this to specify bottom border color
        width: wpx(320),
        borderBottomWidth: 1,
        fontSize: nf(22),
       
        fontFamily: Fonts.tofinoSemi,
       
    },
    logoStyle: {
        width: wpx(400),
        left: wpx(-50),
     height: hpx(500),
        tintColor: Colors.white
    },
    card: {
        backgroundColor: '#5DD986',
        width: wpx(390),
        height: hpx(111),
        elevation: 2,
        shadowColor: Colors.shadow,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        justifyContent: 'center',
         borderRadius: wpx(3)
    },
    button: {
       
        flexDirection: "row",
        justifyContent: "flex-start",
        height: hpx(43),
        width: wpx(145),
        marginTop: wpx(40),
        marginRight:wpx(20),
        backgroundColor: '#D80B8C',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3,
        shadowColor: "#000",
        shadowOffset: {
            width: 1,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        //marginTop: hpx(26)
    },
    welcomeStyle: {
        fontSize: nf(32),
        color:'#2DA461',
        fontFamily: Fonts.tofinoSemi,

    },
    startedStyle: {
        fontSize: nf(22),
        color: '#707070',
        fontFamily: Fonts.book_font,

    },
    eye: {
        marginTop:hpx(15),
        position: 'absolute',
        right: wpx(-10),
    
        

    },
    eye_icon: {
        width: wpx(37),
        tintColor: '#5DD986',
        height: hpx(28),
    },
    buttonText: {
        fontSize: nf(20),
        fontFamily: Fonts.tofinoSemi,

    },
});

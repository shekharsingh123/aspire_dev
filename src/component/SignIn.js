//import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';


import { showMessage } from 'react-native-flash-message';
import { Platform, Text, StyleSheet, View, TouchableOpacity, TextInput, ImageBackground, Image, ScrollView } from 'react-native'
import { useDispatch, shallowEqual, useSelector } from 'react-redux';
import { hpx, wpx, Colors, nf, Fonts } from '../constants/constants';
let bg = require('../assets/images/splash.png');
let logo = require('../assets/images/logo.png');
let eyeOn = require('../assets/images/eyeOn.png')
let eyeOff = require('../assets/images/eyeOff.png')
import ProgressLoader from 'rn-progress-loader';
import { types } from './../store/ActionTypes';
export default function SignIn(props) {
    const { loaderVisible } = useSelector(
        (state) => ({
            loaderVisible: state.globalReducer.loader,

        }),
        shallowEqual,
    );

    const dispatch = useDispatch();
    const [isVisible, setIsVisible] = useState(true);
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');

    const [loader, setloader] = useState(loaderVisible);
    useEffect(() => {
        console.log("loader", loaderVisible)
        setloader(loaderVisible)
    }, [loaderVisible]);

    const onSignIn = () => {

        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (reg.test(email) == true) {
            //   if (/^(?=.*?[A-Za-z])(?=.*?[0-9]).{8,}$/.test(password)) {
            if (password.length > 5) {
                let body = {
                    email: email,
                    password: password,

                };

                dispatch({
                    type: types.TOGGLE_LOADING,
                    payload: true,
                });
                dispatch({
                    type: types.LOGIN_SAGA,
                    payload: body,
                });

            } else {
                showMessage({
                    message:
                        'Password must contain atleast 6 characters.',
                    type: 'danger',

                    titleStyle: { textAlign: 'center' },
                });
            }
        } else {

            showMessage({
                message: 'Please enter the valid Email ID',
                type: 'danger',
                titleStyle: { textAlign: 'center' },
            });
        }
    };

    return (
        <ScrollView style={styles.MainContainer}>

            <View
                style={{ backgroundColor: "#06566e", justifyContent: 'center', alignItems: 'center', flex: 1 }}>

                <ProgressLoader
                    visible={loader}
                    barHeight={100}
                    isModal={true} isHUD={false}
                    hudColor={"#5DD986"}
                    color={"#FFFFFF"} />
            </View>
            <View
                style={styles.MainContainer} >

                <View style={styles.card}>
                    <Image
                        source={logo}
                        style={styles.logoStyle}></Image>


                </View>
                <View style={{ flex: 1, margin: wpx(18) }}>
                    <Text style={styles.welcomeStyle}>
                        Welcome to

                    </Text>

                    <Text style={styles.welcomeStyle}>
                        Springs15 MarketPlace!

                    </Text>
                    <Text style={styles.startedStyle}>
                        Let's get started

                    </Text>
                </View>
                <View style={{ ...styles.SectionStyle, marginTop: hpx(90) }}>
                    <TextInput
                        style={styles.inputStyle}
                        // onChangeText={(UserEmail) => setUserEmail(UserEmail)}
                        underlineColorAndroid="#f000"
                        placeholder="Email"

                        keyboardType="email-address"
                        placeholderTextColor='#5DD986'
                        value={email}
                        onChangeText={(text) => setEmail(text)}
                        returnKeyType="next"

                    />
                </View>
                <View style={{ ...styles.SectionStyle, }}>
                    <TextInput
                        style={styles.inputStyle}
                        // onChangeText={(UserPassword) =>
                        //     setUserPassword(UserPassword)
                        // }
                        secureTextEntry={isVisible}
                        underlineColorAndroid="#f000"
                        placeholder="Password"
                        placeholderTextColor='#5DD986'

                        returnKeyType="next"
                        value={password}

                        onChangeText={text => setPassword(text)}
                        blurOnSubmit={false}
                    />

                    <TouchableOpacity onPress={() => setIsVisible(!isVisible)} style={{ ...styles.eye, }}>
                        <Image
                            style={{ ...styles.eye_icon, }}
                            source={isVisible ? eyeOff : eyeOn} />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row' }}>

                    <TouchableOpacity
                        activeOpacity={1.0}
                        onPress={onSignIn}
                        style={{
                            ...styles.button,
                            backgroundColor: '#5DD986',

                            borderColor: '#707070',
                        }}>
                        <Text style={{ ...styles.buttonText, color: 'white', }}>Log In</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', marginRight: wpx(30) }}>

                    <TouchableOpacity
                        onPress={() => props.navigation.navigate('SignUp')}
                    >
                        <Text style={{ fontFamily: Fonts.pangram_regular, color: '#707070', marginTop: hpx(10), fontSize: nf(15), right: 10 }}>or Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    );
}


const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: '#F8FBF9'
    },
    SectionStyle: {
        flex: 1,
        marginStart: wpx(25),
        marginEnd: wpx(30),
        marginTop: wpx(20),
        flexDirection: 'row',
        justifyContent: 'flex-start',




    },
    inputStyle: {
        borderBottomColor: '#707070', // Add this to specify bottom border color
        width: wpx(320),
        borderBottomWidth: 1,
        fontSize: nf(22),
        color: '#000000',



        fontFamily: Fonts.tofinoSemi,

    },
    logoStyle: {
        width: wpx(400),
        left: wpx(-50),
        height: hpx(500),
        tintColor: Colors.white
    },
    card: {
        backgroundColor: '#5DD986',
        width: wpx(390),
        height: hpx(111),
        elevation: 2,
        shadowColor: Colors.shadow,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        justifyContent: 'center',


        borderRadius: wpx(3)
    },
    button: {

        flexDirection: "row",
        justifyContent: "flex-end",
        height: hpx(39),
        width: wpx(100),
        marginTop: wpx(40),
        marginRight: wpx(20),
        backgroundColor: '#D80B8C',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3,
        shadowColor: "#000",
        shadowOffset: {
            width: 1,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        //marginTop: hpx(26)
    },
    welcomeStyle: {
        fontSize: nf(32),
        color: '#2DA461',
        fontFamily: Fonts.tofinoSemi,

    },
    startedStyle: {
        fontSize: nf(22),
        color: '#707070',
        fontFamily: Fonts.book_font,

    },
    eye: {
        position: 'absolute',
        right: wpx(-10),
    },
    eye_icon: {
        top: Platform.OS == 'android' ? 12 : 0,
        width: wpx(37),
        tintColor: '#5DD986',
        height: hpx(28),
    },
    buttonText: {
        fontSize: nf(20),
        fontFamily: Fonts.tofinoSemi,
    },
});

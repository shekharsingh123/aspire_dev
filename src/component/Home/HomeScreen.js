import React, { Component, useEffect, useCallback, useState } from 'react';
import { Platform, Text, StyleSheet, Image, Dimensions, ScrollView, FlatList, TextInput, TouchableOpacity, View, SafeAreaViewBase, SafeAreaView, ImageBackground, } from 'react-native'
import { hpx, wpx, nf, hp, Fonts, Colors } from '../../constants/constants';
let search = require('../.././assets/images/search.png');
let location = require('../.././assets/images/pin.png');
let logo = require('../.././assets/images/logo.png');
let drow_down = require('../.././assets/images/down-arrow.png');
let meals_cooked = require('../.././assets/images/cookedmeals.jpg');
let demoImg = require('../.././assets/images/download.jpeg');
import Carousel from 'react-native-banner-carousel';
let meals_kit = require('../.././assets/images/mealkits.jpg');
import ProgressLoader from 'rn-progress-loader';
import { types } from '../../store/ActionTypes';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RootNavigation from '../../util/RootNavigation'

import { useFocusEffect } from '@react-navigation/native';
import { useDispatch, shallowEqual, useSelector } from 'react-redux';
const images = [
    demoImg,
    meals_cooked,
    meals_kit
];
const HomeScreen = ({ navigation }) => {

    const { loaderVisible, selected_Cat, banner_Data, products_list, categories_Data } = useSelector(
        (state) => ({
            loaderVisible: state.globalReducer.loader,
            products_list: state.homeReducer.products_list,
            banner_Data: state.homeReducer.banner_Data,
            categories_Data: state.homeReducer.categories_Data,
            selected_Cat: state.homeReducer.selected_Cat

        }),
        shallowEqual,
    );
    const dispatch = useDispatch();
    const [loader, setloader] = useState(loaderVisible);
    useEffect(() => {
        console.log("loader", loaderVisible)
        setloader(loaderVisible)
    }, [loaderVisible]);
    const BannerWidth = Dimensions.get('window').width;
    const BannerHeight = 260;



    useFocusEffect(
        React.useCallback(() => {
            getCatdata();
            getBannerData();
        }
            , []),
    );


    const getCatdata = async () => {
        try {
            const value = await AsyncStorage.getItem('SELECTED_CATEGORY');
            if (value !== null) {

                dispatch({
                    type: types.SELECTED_CATEGORY,
                    payload: value,
                });
                const arrayData = JSON.parse(value);

                dispatch({
                    type: types.GET_PRODUCTS,
                    payload: arrayData?.id
                });
            }
        } catch (error) {

            // Error retrieving data
        }

    }
    const callDescription = (item) => {


        dispatch({
            type: types.TOGGLE_LOADING,
            payload: true,
        });
        dispatch({
            type: types.PRODUCT_DETAILS,
            payload: item.id
        });
        dispatch({
            type: types.GET_WISHLIST,

        });

    }

    const getBannerData = (from) => {


        dispatch({
            type: types.TOGGLE_LOADING,
            payload: true,
        });
        dispatch({
            type: types.GET_BANNERS_HOME,

        });


        // dispatch({
        //     type: types.GET_PRODUCTS,
        //     payload: selected_Cat?.id
        // });
        // dispatch({
        //     type: types.GET_CATEGORY,
        // });
    };
    const keyExtractor = useCallback((item, index) => index.toString(), []);
    const renderPage = (banners, index) => {
        return (
            <View key={index}>
                <Image style={{ width: wpx(543), height: hpx(200) }} source={{ uri: banners?.image?.path }} />
            </View>
        );
    }
    return (
        <ScrollView horizontal={false} style={{ width: '100%', height: '100%' }}>
            <ProgressLoader
                visible={loader}
                barHeight={100}
                isModal={true} isHUD={false}
                hudColor={"#5DD986"}
                color={"#FFFFFF"} />

            <SafeAreaView style={{ flex: 1, }}>


                <View style={{ height: hpx(60), flexDirection: 'row', alignContent: 'center', }}>
                    <View style={styles.card}>
                        <Image
                            source={logo}
                            style={styles.logoStyle}></Image>


                    </View>

                    <View style={styles.location_style}>
                        <View style={styles.address}>
                            <Image source={location} style={{
                                width: wpx(13.5), height: hpx(16.5), bottom: hpx(5),
                                marginRight: wpx(5)
                            }}></Image>
                            <Text style={{ fontFamily: Fonts.pangram_regular, color: '#707070', fontSize: nf(18), }}>Deliver to</Text>
                        </View>
                        <View style={styles.address}>

                            <Text style={{ fontFamily: Fonts.pangram_regular, color: '#2DA461', fontSize: nf(24), }}>HOME </Text>
                            <Image source={drow_down} style={{ tintColor: '#2DA461', width: wpx(13.7), height: hpx(7.83), bottom: hpx(10) }}></Image>
                        </View>
                    </View>
                </View>

                <View style={{ justifyContent: 'flex-start', height: '88%', paddingHorizontal: 5, marginTop: hpx(20) }}>
                    <TouchableOpacity style={styles.searchBox}>

                        <TextInput
                            style={styles.selectInfo}
                            onChangeText={(text) => getRecentSearch(text, tab)}
                            placeholderTextColor={'#FFFFFF'}
                            //   value={searchtext}
                            placeholder="Search for items"
                            keyboardType="default"
                            underlineColorAndroid="transparent"
                        />
                        <Image
                            source={search}
                            resizeMode="contain"
                            style={{ height: hpx(20.2), width: wpx(20.2), tintColor: Colors.white }}
                        />
                        {/* <TouchableOpacity activeOpacity={1.0} onPress={() => removeText()}>
                        <Image
                            source={remove}
                            resizeMode="contain"
                            style={{
                                width: wpx(13.43),
                                height: hpx(13.42),
                                tintColor: '#999999',
                            }}
                        />
                    </TouchableOpacity> */}
                    </TouchableOpacity>
                    <ScrollView horizontal={true} style={{ width: '100%', marginBottom: hpx(20) }}>

                        {products_list?.length > 0 && (
                            <FlatList

                                data={products_list.slice(0, 8)}
                                numColumns={3}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) => (
                                    <TouchableOpacity
                                        activeOpacity={1.0}
                                        onPress={() => callDescription(item)}
                                    >
                                        <View style={styles.designerNameView}>
                                            {
                                                item.cover_image ?
                                                    <Image
                                                        style={styles.designerImage}
                                                        source={{ uri: item.cover_image }}
                                                        resizeMode="cover"
                                                    /> : <Image
                                                        style={styles.designerImage}
                                                        source={meals_cooked}
                                                        resizeMode="cover"
                                                    />
                                            }

                                            <Text
                                                allowFontScaling={false}
                                                style={styles.designerNameText}>
                                                {item.title}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                )}
                                keyExtractor={keyExtractor}
                            />)
                        }
                    </ScrollView>


                </View>

            </SafeAreaView>
            <Carousel
                autoplay={true}
                activePageIndicatorStyle={{ backgroundColor: Colors.green, }}


                pageIndicatorOffset={18}
                showsPageIndicator={true}
                pageIndicatorStyle={{ height: 8, width: 8, borderRadius: 4 }}

                autoplayTimeout={3000}
                pageIndicatorContainerStyle={{ position: 'absolute', bottom: hpx(-20), }}>

                {banner_Data?.map((image, index) => renderPage(image, index))}

            </Carousel>
        </ScrollView>
    );
}

const styles = StyleSheet.create({

    searchBox: {
        height: hpx(49),
        //width: wpx(340),
        backgroundColor: '#5DD986',
        borderRadius: hpx(7),
        borderColor: '#E4E6E772',
        marginTop: hpx(5),
        flexDirection: 'row',

        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: wpx(10),
    },
    address: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
        alignContent: 'center',
        bottom: hpx(25)
    },
    selectInfo: {

        marginLeft: wpx(10),
        fontSize: nf(14),
        fontFamily: Fonts.tofino_pro_personal_book,
        padding: 2,
    },
    card: {
        height: hpx(111),
        justifyContent: 'center',
        borderRadius: wpx(3)
    },
    location_style: {
        flex: 1,
        justifyContent: 'center',
        height: hpx(126),
        margin: 20, bottom: 10
    },
    logoStyle: {
        width: wpx(200),
        top: hpx(-10),
        height: hpx(350),
        tintColor: '#2DA461'
    },
    designerImage: {
        width: wpx(105),
        height: hpx(106),
        //    backgroundColor: 'black'


    },
    designerNameView: {

        justifyContent: 'center',
        width: wpx(105),
        height: hpx(131),
        borderRadius: 5,
        alignItems: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        marginHorizontal: 8,
        borderColor: Colors.grey,
        marginTop: hpx(20),

        elevation: 1,
        shadowColor: '#00000029',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        backgroundColor: 'white',
    },
    designerNameText: {

        top: 2,
        fontSize: nf(12),
        fontFamily: Fonts.tofino_pro_personal_book,
        textAlign: 'center',
        textAlignVertical: 'center',
        color: Colors.black,

    },
});
export default HomeScreen;
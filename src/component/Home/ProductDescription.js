import React, { Component, useEffect, useCallback, useState, useImperativeHandle } from 'react';
import { Platform, Text, StyleSheet, Image, Dimensions, ScrollView, FlatList, TextInput, TouchableOpacity, View, SafeAreaViewBase, SafeAreaView, ImageBackground, } from 'react-native'
import { hpx, wpx, nf, hp, Fonts, Colors } from '../../constants/constants';
let search = require('../.././assets/images/search.png');
let location = require('../.././assets/images/pin.png');
import { useFocusEffect } from '@react-navigation/native';
let logo = require('../.././assets/images/logo.png');
let drow_down = require('../.././assets/images/down-arrow.png');
let meals_cooked = require('../.././assets/images/cookedmeals.jpg');
let heart = require('../.././assets/images/heart.png');
let select_heart = require('../.././assets/images/selected_heart.png');
let demoImg = require('../.././assets/images/download.jpeg');
import * as RootNavigation from '../../util/RootNavigation'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Rating } from 'react-native-ratings';
import Carousel from 'react-native-banner-carousel';
let plusImg = require('../.././assets/images/plus.png');
let minusImg = require('../.././assets/images/minus.png');
let user = require('../.././assets/images/user.png');
let backImg = require('../.././assets/images/backArrow.png');
let infoImg = require('../.././assets/images/info.png');
let addImg = require('../.././assets/images/add.png');
import { back } from 'react-native/Libraries/Animated/Easing';
let meals_kit = require('../.././assets/images/chicken_lat.jpeg');
import ProgressLoader from 'rn-progress-loader';
import { types } from '../../store/ActionTypes';
import { useDispatch, shallowEqual, useSelector } from 'react-redux';
import AwesomeAlert from 'react-native-awesome-alerts';
import { color } from 'react-native-reanimated';
import HTML from 'react-native-render-html';
import 'intl';
import 'intl/locale-data/jsonp/en';
const formatter = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 10 });
const images = [
    demoImg,
    meals_cooked,
    meals_kit
];
let userTokens;
const ProductDescription = ({ navigation }) => {
    var favItem = [];
    const { loaderVisible, selectedFav, wishlist_item, banner_Data, products_list, categories_Data, product_detail, rating_success } = useSelector(
        (state) => ({
            loaderVisible: state.globalReducer.loader,
            products_list: state.homeReducer.products_list,
            banner_Data: state.homeReducer.banner_Data,
            categories_Data: state.homeReducer.categories_Data,
            product_detail: state.homeReducer.product_detail,
            rating_success: state.homeReducer.rating_success,
            selectedFav: state.homeReducer.selectedFav,
            wishlist_item: state.homeReducer.wishlist_item,


        }),
        shallowEqual,
    );
    const dispatch = useDispatch();
    const [showAlert, setAlert] = useState(rating_success);
    const [loader, setloader] = useState(loaderVisible);
    const [showAlertLogin, setAlertLogin] = useState(false);
    const [button, setbutton] = useState('description');
    const [inc_dec, setIncDec] = useState(0);
    const [rate, setRate] = useState('');
    const [review_clicked, setReviewClick] = useState(false);
    const [review, setReview] = useState('');
    const [Selected_fav, setFav] = useState(selectedFav);
    useEffect(() => {
        console.log("loader", loaderVisible)
        setloader(loaderVisible)
    }, [loaderVisible]);


    useEffect(() => {
        getData();
    }, []);

    useFocusEffect(
        React.useCallback(() => {
            if (wishlist_item?.length > 0) {
                wishlist_item?.map((item, index) => {
                    if (item.listing_id == product_detail?.id) {


                        favItem.push(product_detail);
                        setFav(favItem);
                    }
                });
                dispatch({
                    type: types.FAV_ITEMS,
                    payload: favItem,
                });
            }
        }, [wishlist_item, product_detail]),
    );
    useEffect(() => {

        setAlert(rating_success)
    }, [rating_success]);

    const getData = async () => {
        userTokens = await AsyncStorage.getItem('Token');
    }
    const addTocart = () => {

        if (userTokens && inc_dec > 0) {
            let body = {
                item_id: product_detail?.id,
                quantity: inc_dec

            }

            dispatch({
                type: types.TOGGLE_LOADING,
                payload: true,
            });
            dispatch({
                type: types.ADD_TO_CART,
                payload: body
            });
        } else if (userTokens && inc_dec == 0) {

        }
        else {
            setAlertLogin(true);
        }

    }
    const CheckToken = () => {

        if (userTokens) {
            let body = {
                inventory_id: product_detail?.id,
            }
            dispatch({
                type: types.ADD_WISHLIST,
                payload: body,
            });


            let total_item;
            var selectedIds = [...Selected_fav];

            if (
                selectedIds?.filter((value) => value.id == product_detail?.id).length >
                0
            ) {

                var removeIndex = selectedIds.findIndex(
                    (x) => x.id === product_detail?.id,
                );
                selectedIds?.splice(removeIndex, 1);
                var index;
                setFav(selectedIds);
                if (Selected_fav?.length > 1) {
                    total_item = selectedIds;
                } else {
                    total_item = [];
                }
                dispatch({
                    type: types.FAV_ITEMS,
                    payload: total_item,
                });
            } else {

                selectedIds.push(product_detail);
                setFav(selectedIds);
                dispatch({
                    type: types.FAV_ITEMS,
                    payload: selectedIds,
                });
            }


        } else {
            setAlertLogin(true);
        }
    }
    const CheckLogin = () => {
        if (userTokens) {
            setReviewClick(true);
        } else {
            setAlertLogin(true);
        }
    }
    const callNav = (from) => {
        if (from === 'minus' && inc_dec > 0) {
            setIncDec(inc_dec - 1)

        } else if (from === 'plus') {
            setIncDec(inc_dec + 1)
        } else {
            setIncDec(0)
        }



    };

    const callReviewApi = () => {
        let body = {
            rating: rate,
            comment: review,
            inventory_id: product_detail.product.id,
            order_id: product_detail.id,
        }
        dispatch({
            type: types.TOGGLE_LOADING,
            payload: true,
        });
        dispatch({
            type: types.ADD_RATING,
            payload: body
        });

    }


    const ratingCompleted = (rating) => {
        setRate(rating);
        console.log("Rating is: " + rating)
    }


    const keyExtractor = useCallback((item, index) => index.toString(), []);
    const renderPage = (image, index) => {
        return (
            <View key={index}>
                <Image style={{ width: wpx(543), height: hpx(146) }} source={image} />
            </View>
        );
    }
    return (
        <SafeAreaView horizontal={false} style={{ flex: 1, backgroundColor: '#FFFFFF' }}>


            <ProgressLoader
                visible={loader}
                barHeight={100}
                isModal={true} isHUD={false}
                hudColor={"#5DD986"}
                color={"#FFFFFF"} />
            <ScrollView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <ImageBackground style={{ width: wpx(390), height: hpx(325), }}
                    source={{ uri: product_detail?.product?.image }} >



                    <View style={{ height: '10%', flexDirection: 'row', alignContent: 'center', bottom: 20 }}>
                        <View style={styles.commonHeaderView}>
                            <TouchableOpacity
                                activeOpacity={1.0}
                                onPress={() => navigation.goBack()}>
                                <Image
                                    source={backImg}
                                    style={styles.backArrow}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                            <Text allowFontScaling={false} style={styles.heading}>
                                Back
                            </Text>
                        </View>

                        <View style={styles.location_style}>
                            <View style={styles.address}>
                                <Image source={location} style={{
                                    width: wpx(13.5), height: hpx(16.5), bottom: hpx(5),
                                    marginRight: wpx(5)
                                }}>


                                </Image>
                                <Text style={{ fontFamily: Fonts.pangram_regular, color: '#707070', fontSize: nf(18), }}>Deliver to</Text>
                            </View>
                            <View style={styles.address}>

                                <Text style={{ fontFamily: Fonts.pangram_regular, color: '#2DA461', fontSize: nf(24), }}>HOME </Text>
                                <Image source={drow_down} style={{ tintColor: '#2DA461', width: wpx(13.7), height: hpx(7.83), bottom: hpx(10) }}></Image>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
                <View style={{
                    height: hpx(446),
                    width: wpx(375),
                    justifyContent: 'flex-start',
                    alignContent: 'flex-start',
                    alignItems: 'flex-start',
                    borderTopLeftRadius: 15,
                    borderTopRightRadius: 15,

                    top: - 10,
                    borderTopColor: 'black',
                    borderRadius: 10,

                    borderColor: '#707070',
                    paddingHorizontal: wpx(10),

                    shadowColor: '#707070',
                    // shadowOffset: { width: 0, height: 2 },
                    // shadowOpacity: 0.9,

                    backgroundColor: '#FFFFFF',
                }}>

                    <View style={{ marginTop: hpx(20), width: '100%', height: hpx(40), flexDirection: 'row', justifyContent: 'space-between', }}>
                        <Text allowFontScaling={false} style={{ ...styles.heading, color: '#5DD986' }}>
                            {product_detail?.title}
                        </Text>

                        <TouchableOpacity
                            onPress={() => CheckToken()}>

                            {Selected_fav?.some(
                                (name) => name.id == product_detail?.id,
                            ) ? (
                                <Image style={{ width: wpx(29), height: hpx(27) }}
                                    source={select_heart}
                                />
                            ) : (
                                <Image style={{ width: wpx(29), height: hpx(27) }}
                                    source={heart}
                                />
                            )}
                        </TouchableOpacity>
                    </View>
                    <Text allowFontScaling={false} style={{ ...styles.heading, color: '#5DD986', fontSize: nf(20), }}>
                        {product_detail?.condition_note}
                    </Text>

                    <View style={{ marginTop: hpx(20), width: '100%', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', }}>
                        <Text allowFontScaling={false} style={{ ...styles.heading, color: '#5DD986' }}>
                            {product_detail?.currency_symbol} {formatter.format(product_detail?.raw_price)}
                        </Text>

                        <View style={{

                            ...styles.button,
                            backgroundColor: '#D3EEDC',
                            borderWidth: 1,
                            paddingHorizontal: hpx(5),
                            borderColor: '#D3EEDC',
                            shadowColor: '#707070',
                            shadowOffset: { width: 0, height: 1 },
                            shadowOpacity: 0.8,
                            shadowRadius: 1,
                        }}>


                            <TouchableOpacity
                                TouchableOpacity={5}
                                onPress={() => callNav('minus')}>

                                <Image style={{ width: wpx(9), height: hpx(3), }}
                                    source={minusImg} />
                            </TouchableOpacity>
                            <TouchableOpacity
                            >

                                <Text style={{ ...styles.buttonText, color: '#010101', fontSize: nf(16), }}>{inc_dec}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                TouchableOpacity={5}
                                onPress={() => callNav('plus')}>

                                <Image style={{ width: wpx(9), height: hpx(9) }}
                                    source={plusImg} />
                            </TouchableOpacity>
                        </View>

                    </View>
                    <View style={{ marginTop: hpx(20), width: '100%', height: hpx(40), flexDirection: 'row', justifyContent: 'space-around', marginTop: hpx(40) }}>
                        <TouchableOpacity
                            onPress={() => setbutton('description')}
                            style={{
                                ...styles.button1,
                                backgroundColor: button === 'description' ? '#87EAA8' : '#AEBCB3',
                                borderWidth: 1,
                                borderColor: button === 'description' ? '#87EAA8' : '#AEBCB3',
                            }}>
                            <Text style={{ ...styles.buttonText, color: '#FFFFFF', fontSize: nf(16), }}>
                                Description
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => setbutton('Review')}
                            style={{
                                ...styles.button1,
                                backgroundColor: button !== 'description' ? '#87EAA8' : '#AEBCB3',
                                borderWidth: 1,
                                borderColor: button !== 'description' ? '#87EAA8' : '#AEBCB3',
                            }}>
                            <Text style={{ ...styles.buttonText, color: '#FFFFFF', fontSize: nf(16), }}>Review</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: hpx(20), width: '100%', height: hpx(40), flexDirection: 'row', justifyContent: 'center', marginTop: hpx(10) }}>
                        <View
                            style={{
                                ...styles.line,
                                height: hpx(1),
                                backgroundColor: button === 'description' ? '#87EAA8' : '#AEBCB3',
                                borderWidth: 0.1,
                                borderColor: button === 'description' ? '#87EAA8' : '#AEBCB3',
                            }}>

                        </View>
                        <View
                            style={{
                                ...styles.line,
                                height: hpx(1),
                                backgroundColor: button !== 'description' ? '#87EAA8' : '#AEBCB3',

                                borderColor: button !== 'description' ? '#87EAA8' : '#AEBCB3',
                            }}></View>
                    </View>
                    {
                        button === 'description' ?
                            <Text allowFontScaling={false} style={{ ...styles.heading, color: '#000', marginTop: hpx(10), marginStart: wpx(10) }}>
                                {product_detail?.description}
                            </Text> :
                            review_clicked ?
                                <View style={{ height: hpx(210), flexDirection: 'column', justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center', }}>

                                    <View style={{ height: hpx(15), justifyContent: 'flex-end', alignContent: 'flex-end', alignItems: 'flex-end', alignSelf: 'flex-end', paddingEnd: hpx(30) }}>
                                        <Rating
                                            fractions={2}
                                            ratingColor={'#5DD986'}
                                            style={{ backgroundColor: '#5DD986' }}
                                            imageSize={13}
                                            startingValue={5}
                                            readonly={false}
                                            onFinishRating={ratingCompleted}
                                        />
                                    </View>

                                    {/* <Text style={styles.cardText4}>{Moment(24 / 03 / 2022).format('DD/MM/YYYY')}</Text> */}

                                    <View style={{ ...styles.SectionStyle, }}>
                                        <TextInput
                                            style={styles.inputStyle}
                                            // onChangeText={(UserEmail) => setUserEmail(UserEmail)}
                                            underlineColorAndroid="transparent"
                                            placeholder="write here..."
                                            underlineColorAndroid="#f000"
                                            keyboardType="email-address"
                                            placeholderTextColor='#5DD986'
                                            value={review}
                                            onChangeText={(text) => setReview(text)}
                                            returnKeyType="next"

                                        />
                                    </View>




                                </View>
                                :



                                < View style={{ height: hpx(210), flexDirection: 'column', justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center', }}>
                                    {
                                        product_detail?.feedbacks?.length > 0 ?
                                            product_detail.feedbacks?.map((item) => {
                                                return (

                                                    <View style={styles.review}>
                                                        <View style={styles.card1}>
                                                            <View
                                                                style={{
                                                                    ...styles.card1,
                                                                    justifyContent: 'space-between',
                                                                }}>
                                                                <Image
                                                                    source={user}
                                                                    style={styles.profileImage}
                                                                    resizeMode="cover"
                                                                />
                                                                <View style={{ marginLeft: wpx(10) }}>
                                                                    <View style={styles.card2}>
                                                                        <Text style={styles.cardText1}>shekhar
                                                                        </Text>
                                                                        <Text style={styles.cardText2}>Follow</Text>
                                                                    </View>
                                                                    <Text style={styles.cardText3}>Silyara ,tehri UK </Text>
                                                                </View>
                                                            </View>
                                                            <View style={styles.card3}>
                                                                <Rating
                                                                    fractions={2}
                                                                    ratingColor={'#5DD986'}
                                                                    style={{ backgroundColor: '#5DD986' }}
                                                                    imageSize={13}
                                                                    startingValue={item.rating}
                                                                    readonly={true}


                                                                />
                                                                {/* <Text style={styles.cardText4}>{Moment(24 / 03 / 2022).format('DD/MM/YYYY')}</Text> */}
                                                            </View>
                                                        </View>

                                                        <Text style={styles.cardText5}>
                                                            {item.comment}
                                                        </Text>


                                                    </View>)
                                            }) : null
                                    }




                                </View>


                    }


                </View>
                <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end', marginEnd: hpx(30) }}>
                    {
                        review_clicked && button !== 'description' ?
                            <TouchableOpacity onPress={() => callReviewApi()}>
                                <Text allowFontScaling={false} style={{ ...styles.headingReview, color: '#5DD986', fontSize: nf(16), marginTop: hpx(20), marginBottom: hpx(20), }}>
                                    Submit
                                </Text>
                            </TouchableOpacity> :
                            !review_clicked && button !== 'description' ?

                                <TouchableOpacity
                                    onPress={() => CheckLogin()}
                                >
                                    <Text allowFontScaling={false} style={{ ...styles.headingReview, color: '#5DD986', fontSize: nf(16), marginTop: hpx(20), marginBottom: hpx(20), }}>
                                        Write Review
                                    </Text>
                                </TouchableOpacity>
                                : null}

                </View>
                <View style={{
                    height: hpx(111),
                    width: wpx(375),
                    bottom: 0,
                    borderTopColor: 'black',


                    borderColor: '#707070',

                    elevation: 1,
                    shadowColor: '#707070',
                    // shadowOffset: { width: 0, height: 2 },
                    // shadowOpacity: 0.9,

                    backgroundColor: '#5DD986',
                }}>
                    <View style={{ flex: 1, flexDirection: 'row', alignSelf: 'center', alignItems: 'center', justifyContent: 'space-between', }}>
                        <Text allowFontScaling={false} style={{ ...styles.heading, color: '#ffffff', fontSize: nf(20), marginEnd: wpx(20) }}>
                            {inc_dec} items |  {product_detail?.currency_symbol} {formatter.format(product_detail?.raw_price * inc_dec)}
                        </Text>
                        <TouchableOpacity
                            onPress={() => addTocart()}
                        >
                            <View style={{

                                ...styles.cart_button,
                                backgroundColor: inc_dec > 0 && userTokens ? '#FFFFFF' : '#AEBCB3',
                                borderWidth: 1,
                                paddingHorizontal: hpx(5),
                                borderColor: inc_dec > 0 && userTokens ? '#D3EEDC' : '#AEBCB3',
                                shadowColor: '#707070',
                                shadowOffset: { width: 0, height: 1 },
                                shadowOpacity: 0.8,
                                shadowRadius: 1,
                            }}>





                                <Text style={{ ...styles.buttonText, color: inc_dec > 0 && userTokens ? '#5DD986' : '#FFFFFF', fontSize: nf(16), }}>Cart</Text>



                                <Image
                                    source={backImg}
                                    style={{ ...styles.backArrow, left: wpx(10), tintColor: inc_dec > 0 && userTokens ? '#5DD986' : '#FFFFFF', transform: [{ rotate: '180deg' }] }}
                                    resizeMode="contain"
                                />

                            </View>
                        </TouchableOpacity>

                    </View>
                </View>
            </ScrollView >

        </SafeAreaView >
    );
}

const styles = StyleSheet.create({

    searchBox: {
        height: hpx(49),
        //width: wpx(340),
        backgroundColor: '#5DD986',
        borderRadius: hpx(7),
        borderColor: '#E4E6E772',
        marginTop: hpx(5),
        flexDirection: 'row',

        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: wpx(10),
    },
    heading: {
        fontSize: nf(15),
        marginHorizontal: wpx(6),
        fontFamily: Fonts.begum,
        textAlign: 'center',
    },
    headingReview: {
        fontSize: nf(15),

        fontFamily: Fonts.begum,
        textAlign: 'center',
    },
    backArrow: {
        width: wpx(16),
        height: hpx(16),
    },
    address: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
        alignContent: 'center',
        bottom: hpx(25)
    },
    selectInfo: {

        marginLeft: wpx(10),
        fontSize: nf(14),
        fontFamily: Fonts.tofino_pro_personal_book,
        padding: 2,
    },
    commonHeaderView: {

        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: hpx(53),
        top: 20,
        width: wpx(175),
        paddingHorizontal: wpx(21),
    },
    location_style: {
        flex: 1,
        justifyContent: 'center',
        height: hpx(126),
        margin: 20, bottom: 1
    },

    designerImage: {
        width: wpx(105),
        height: hpx(106),
        //    backgroundColor: 'black'


    },
    designerNameView: {

        justifyContent: 'center',
        width: wpx(105),
        height: hpx(131),
        borderRadius: 5,
        alignItems: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        marginHorizontal: 8,
        borderColor: Colors.grey,
        marginTop: hpx(20),

        elevation: 1,
        shadowColor: '#00000029',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        backgroundColor: 'white',
    },
    designerNameText: {

        top: 2,
        fontSize: nf(12),
        fontFamily: Fonts.tofino_pro_personal_book,
        textAlign: 'center',
        textAlignVertical: 'center',
        color: Colors.black,

    },
    button: {
        height: hpx(35),
        width: wpx(68),

        alignContent: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 6,
        //marginTop: hpx(26)
    },
    cart_button: {
        height: hpx(43),
        width: wpx(115),

        alignContent: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
        //marginTop: hpx(26)
    },
    inputStyle: {
        borderBottomColor: '#707070', // Add this to specify bottom border color
        width: wpx(320),
        height: hpx(140),
        borderBottomWidth: 0.5,
        borderBottomColor: '#5DD986',
        borderWidth: 0.5,
        paddingStart: hpx(10),
        borderColor: '#5DD986',
        fontSize: nf(22),
        color: '#5DD986',



        fontFamily: Fonts.tofinoSemi,

    },
    buttonText: {
        fontSize: nf(20),
        fontFamily: Fonts.tofinoSemi,

    },
    button1: {
        height: hpx(42),
        width: wpx(139),

        shadowColor: '#707070',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        //marginTop: hpx(26)
    },
    line: {
        width: wpx(159),
        marginTop: hpx(20),

        justifyContent: 'center',
        alignItems: 'center',

        //marginTop: hpx(26)
    },
    review: {
        alignContent: 'center',
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
        width: wpx(314),
        height: hpx(13),
        backgroundColor: '#5DD986',
        borderColor: '#D3EEDC',
        shadowColor: '#707070',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        borderRadius: 20,
        marginBottom: 20
    },
    profileImage: {
        width: 40,
        height: 40,
        backgroundColor: '#ffffff',
        borderRadius: 20,
    },
    lengaImage: {
        width: 58,
        height: 70,
        top: hpx(15),
    },
    card1: {
        left: -15,
        top: -15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',

    },
    card2: {

        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    card3: {
        top: hpx(-30),

        right: wpx(-30),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: hpx(25),
    },
    cardText1: { fontFamily: Fonts.medium, fontSize: nf(16), color: Colors.white, },
    cardText2: {
        fontFamily: Fonts.book_font,
        fontSize: nf(14),
        color: Colors.white,
        marginLeft: wpx(10),
    },
    cardText3: {
        fontFamily: Fonts.book_font,
        fontSize: nf(12),
        color: Colors.white,

    },
    cardText4: {
        fontFamily: Fonts.book_font,
        fontSize: nf(12),
        color: Colors.white,
    },
    cardText5: {
        fontFamily: Fonts.book_font,
        fontSize: nf(12),
        color: Colors.white,
        marginTop: hpx(12),
    },
    SectionStyle: {
        flex: 1,
        marginStart: wpx(25),
        marginEnd: wpx(30),
        marginTop: wpx(20),
        flexDirection: 'row',
        justifyContent: 'flex-start',




    },
});
export default ProductDescription;
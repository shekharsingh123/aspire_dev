import React, { Component, useEffect, useCallback, useState } from 'react';
import { Platform, Text, StyleSheet, Image, Dimensions, ScrollView, FlatList, TextInput, TouchableOpacity, View, SafeAreaViewBase, SafeAreaView, ImageBackground, } from 'react-native'
import { hpx, wpx, nf, hp, Fonts, Colors, wp } from '../../constants/constants';
let search = require('../.././assets/images/search.png');
let location = require('../.././assets/images/pin.png');
import { useFocusEffect } from '@react-navigation/native';
let logo = require('../.././assets/images/logo.png');
let drow_down = require('../.././assets/images/down-arrow.png');
let meals_cooked = require('../.././assets/images/cookedmeals.jpg');
let demoImg = require('../.././assets/images/download.jpeg');
import Carousel from 'react-native-banner-carousel';
let backImg = require('../.././assets/images/backArrow.png');
let infoImg = require('../.././assets/images/info.png');
let shopping_cart = require('../.././assets/images/shopping_cart.png');
let addImg = require('../.././assets/images/add.png');
import { back } from 'react-native/Libraries/Animated/Easing';
let main_logo = require('../.././assets/images/spring.png');
let meals_kit = require('../.././assets/images/mealkits.jpg');
import * as RootNavigation from '../../util/RootNavigation'
let plusImg = require('../.././assets/images/plus.png');
let minusImg = require('../.././assets/images/minus.png');
import Swipeout from 'react-native-swipeout';
let remove_fav = require('../.././assets/images/remove_fav.png');
let cartImg = require('../.././assets/images/cartImg.png');
import ProgressLoader from 'rn-progress-loader';
import { types } from '../../store/ActionTypes';
import AsyncStorage from '@react-native-async-storage/async-storage';
const formatter = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 10 });
import 'intl';
import 'intl/locale-data/jsonp/en';
import { useDispatch, shallowEqual, useSelector } from 'react-redux';

const orderScreen = ({ navigation, }) => {
    const { loaderVisible, selected_Cat, cart_item, banner_Data, products_list, categories_Data } = useSelector(
        (state) => ({
            loaderVisible: state.globalReducer.loader,
            cart_item: state.homeReducer.cart_item

        }),
        shallowEqual,
    );
    const dispatch = useDispatch();
    const [cart_item_array, setcart_item_array] = useState(cart_item);
    const [inc_dec, setIncDec] = useState(1);
    const [activeRow, setactiveRow] = useState();


    const [loader, setloader] = useState(loaderVisible);
    useEffect(() => {
        console.log("loader", loaderVisible)
        setloader(loaderVisible)
    }, [loaderVisible]);

    const getCartdata = () => {
        dispatch({
            type: types.TOGGLE_LOADING,
            payload: true,
        });
        dispatch({
            type: types.CART_ITEM,
            // payload: item.id
        });
    }
    const callNav = (from, item, index) => {


        if (from === 'minus' && cart_item_array[index].items[0].quantity > 0) {
            // Object.assign(item, { quantity: item.quantity - 1});
            //  setIncDec(inc_dec - 1)
            cart_item_array[index].items[0].quantity = parseInt(item.items[0].quantity) - 1;

        } else if (from === 'plus') {
            cart_item_array[index].items[0].quantity = parseInt(item.items[0].quantity) + 1;
            // setIncDec(inc_dec + 1)
        } else {
            //  setIncDec(0)
            cart_item_array[index].items[0].quantity = 0;
        }
        console.log("cart", cart_item_array)
        setcart_item_array([...cart_item_array])
    };


    useFocusEffect(
        React.useCallback(() => {
            getCartdata()
        }
            , []),
    );
    useFocusEffect(
        React.useCallback(() => {
            setcart_item_array(cart_item)
        }
            , [cart_item]),
    );








    const renderCartList = ({ item, index }) => {


        return (



            <View style={styles.review}>

                <View
                    style={{
                        flexDirection: 'row',
                        flex: 1,

                        width: wpx(329),
                        top: 5,
                        justifyContent: 'space-between',

                    }}>
                    <Text
                        allowFontScaling={false}
                        style={{
                            ...styles.lehangaText3,

                            textAlign: 'center',
                        }}>
                        12/01/2022
                    </Text>


                    <TouchableOpacity
                        onPress={() => navigation.navigate('OrderDetail')}
                    >

                        <Text
                            allowFontScaling={false}
                            style={{
                                color: '#2DA461',



                                textDecorationLine: 'underline',

                                textAlign: 'center',
                            }}>
                            View Order
                        </Text>
                    </TouchableOpacity>

                </View>
                <View style={{ width: '100%', }}>

                    <Text
                        allowFontScaling={false}
                        style={{ ...styles.lehangaText1, }}>
                        Delivery Address : Dubai,UAE
                    </Text>
                    {/* <Text
                            allowFontScaling={false}
                            style={{ ...styles.lehangaText2, width: wpx(160) }}>
                            chicken
                        </Text> */}

                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            marginTop: hpx(5)
                        }}>


                        <Text
                            allowFontScaling={false}
                            style={{
                                ...styles.lehangaText2,



                                textAlign: 'center',
                            }}>
                            Order Total : {item?.items[0].total}
                        </Text>


                    </View>
                </View>

                <View style={{ flex: 1, flexDirection: 'row', bottom: hpx(5), justifyContent: 'space-between', width: '100%' }}>

                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            top: 5,
                            alignSelf: 'center',

                        }}>


                        <Text
                            allowFontScaling={false}
                            style={{
                                ...styles.lehangaText2,



                                textAlign: 'center',
                            }}>
                            Payment Method : CASH
                        </Text>


                    </View>



                    <View style={{

                        ...styles.button1,
                        backgroundColor: '#FFFFFF',
                        borderWidth: 1,
                        paddingHorizontal: hpx(5),
                        borderColor: '#D3EEDC',
                        shadowColor: '#707070',
                        shadowOffset: { width: 0, height: 1 },
                        // shadowOpacity: 0.8,
                        // shadowRadius: 1,
                    }}>



                        <TouchableOpacity
                            onPress={() => navigation.navigate('Cart')}
                        >

                            <Text style={{ ...styles.buttonText, color: '#5DD986', fontSize: nf(15), }}>Order Again</Text>
                        </TouchableOpacity>
                    </View>

                </View>

            </View >

        );
    };
    const onSwipeOpen = (rowId, direction) => {
        if (typeof direction !== 'undefined') {

            setactiveRow(rowId)
            console.log("Active Row", rowId);
        }
    }
    const BannerWidth = Dimensions.get('window').width;
    const BannerHeight = 260;
    const keyExtractor = useCallback((item, index) => index.toString(), []);

    return (
        <SafeAreaView style={{ width: '100%', height: '100%', backgroundColor: 'white' }}>
            <ProgressLoader
                visible={loader}
                barHeight={100}
                isModal={true} isHUD={false}
                hudColor={"#5DD986"}
                color={"#FFFFFF"} />
            <SafeAreaView style={{ flex: 1, }}>


                <View style={{ height: '8%', flexDirection: 'row', alignContent: 'center', }}>
                    <View style={styles.commonHeaderView}>
                        <TouchableOpacity
                            activeOpacity={1.0}
                            onPress={() => navigation.goBack()}>
                            <Image
                                source={backImg}
                                style={styles.backArrow}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                        <Text allowFontScaling={false} style={styles.heading}>
                            Purchase History
                        </Text>
                        <Image source={main_logo}
                            style={

                                { width: wpx(150), height: hpx(150), right: wpx(-100), marginBottom: hpx(50), marginTop: hpx(20) }
                            }
                        >

                        </Image>
                    </View>

                </View>



                {
                    cart_item_array?.length > 0 ?
                        <View
                            style={styles.MainContainer} >
                            <View style={{ marginTop: hpx(15), marginBottom: hpx(15), marginHorizontal: wpx(20), height: hpx(37), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', alignContent: 'center' }}>
                                <Text style={{
                                    fontSize: nf(20),
                                    color: '#000',
                                    fontFamily: Fonts.tofino_pro_personal_Regular,
                                }}>Purchase History</Text>
                                {/* <Image source={shopping_cart} style={{ width: wpx(25), height: hpx(24) }}>

                                </Image> */}
                            </View>






                            <FlatList
                                //  extraData={cart_item_array}
                                data={cart_item_array}
                                showsHorizontalScrollIndicator={false}
                                renderItem={renderCartList}
                                keyExtractor={keyExtractor}
                            />





                        </View>
                        : <View
                            style={styles.MainContainer} >
                            <View style={{ flex: 1, margin: wpx(18) }}>
                                <Text style={styles.welcomeStyle}>
                                    No History found

                                </Text>

                                {/* <Text style={{ ...styles.startedStyle, marginTop: wpx(30), marginEnd: wpx(60) }}>
                            You can now continue shopping on Springs 15 Marketplace! Have fun!

                        </Text> */}
                            </View>

                            <View >
                                <Image
                                    source={cartImg}
                                    style={styles.logoStyle}></Image>


                            </View>
                            <View style={{ flex: 1, justifyContent: 'flex-end', margin: wpx(18), flexDirection: 'row' }}>

                                <TouchableOpacity
                                    onPress={() => RootNavigation.navigate('HomeStack')}
                                    style={{
                                        ...styles.button,
                                        bottom: 15,
                                        backgroundColor: '#5DD986',

                                        borderColor: '#707070',
                                    }}>
                                    <Text style={{ ...styles.buttonText, color: 'white', }}>Shop Now</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                }


                {/* <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row',marginRight:wpx(30) }}>

                    <TouchableOpacity
                        onPress={() => props.navigation.navigate('Login')}
                       >
                        <Text style={{ fontFamily: Fonts.pangram_regular, color: '#707070', marginTop: hpx(10), fontSize: nf(15),right:10 }}>or Sign Up</Text>
                    </TouchableOpacity>
                </View> */}

            </SafeAreaView>


        </SafeAreaView >
    );
}

const styles = StyleSheet.create({

    searchBox: {
        height: hpx(49),
        //width: wpx(340),
        backgroundColor: '#5DD986',
        borderRadius: hpx(7),
        borderColor: '#E4E6E772',
        marginTop: hpx(5),
        flexDirection: 'row',

        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: wpx(10),
    },
    heading: {
        fontSize: nf(14),
        marginStart: 10,
        fontFamily: Fonts.begum,
        textAlign: 'center',
    },
    backArrow: {

        width: wpx(16),
        height: hpx(16),
    },
    address: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
        alignContent: 'center',
        bottom: hpx(25)
    },
    selectInfo: {

        marginLeft: wpx(10),
        fontSize: nf(14),
        fontFamily: Fonts.tofino_pro_personal_book,
        padding: 2,
    },
    lehangaText2: {
        fontSize: nf(12),
        fontFamily: Fonts.pangram_regular,
        color: '#000',

    },
    commonHeaderView: {
        flex: 1,
        marginTop: hpx(10),

        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: hpx(53),

        width: wpx(175),
        paddingHorizontal: wpx(21),
    },
    location_style: {
        flex: 1,
        justifyContent: 'center',
        height: hpx(126),
        bottom: 1,
        paddingHorizontal: wpx(21),
    },
    lehangaText1: { fontSize: nf(16), fontFamily: Fonts.tofino_pro_personal_Regular, },
    cardView: {
        height: hpx(109),
        width: wpx(349),
        flex: 1,
        paddingHorizontal: wpx(5),
        backgroundColor: '#A3E8BA',

        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',

    },
    lenga: {
        width: wpx(80),
        height: hpx(80),
    },
    designerImage: {
        width: wpx(105),
        height: hpx(106),
        //    backgroundColor: 'black'


    },
    designerNameView: {

        justifyContent: 'center',
        width: wpx(105),
        height: hpx(131),
        borderRadius: 5,
        alignItems: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        marginHorizontal: 8,
        borderColor: Colors.grey,
        marginTop: hpx(20),

        elevation: 1,
        shadowColor: '#00000029',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        backgroundColor: 'white',
    },
    designerNameText: {

        top: 2,
        fontSize: nf(12),
        fontFamily: Fonts.tofino_pro_personal_book,
        textAlign: 'center',
        textAlignVertical: 'center',
        color: Colors.black,

    },
    MainContainer: {
        flex: 1,



    },
    SectionStyle: {
        flex: 1,
        marginStart: wpx(25),
        marginEnd: wpx(30),
        marginTop: wpx(20),
        flexDirection: 'row',
        justifyContent: 'flex-start',




    },
    inputStyle: {
        borderBottomColor: '#707070', // Add this to specify bottom border color
        width: wpx(320),
        borderBottomWidth: 1,
        fontSize: nf(22),

        fontFamily: Fonts.tofinoSemi,

    },
    logoStyle: {
        width: wpx(116),
        marginStart: wpx(20),
        height: hpx(89),
        bottom: hpx(20),
        resizeMode: 'contain'

    },
    button1: {
        height: hpx(32),
        width: wpx(117),



        alignContent: 'flex-end',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

        borderRadius: 6,
        alignSelf: 'flex-end'

        //marginTop: hpx(26)
    },
    cart_button: {
        height: hpx(43),
        width: wpx(135),

        alignContent: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
        //marginTop: hpx(26)
    },
    button: {

        flexDirection: "row",
        justifyContent: "flex-end",
        height: hpx(43),
        width: wpx(145),
        marginTop: wpx(40),
        marginRight: wpx(20),
        backgroundColor: '#D80B8C',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3,
        shadowColor: "#000",
        shadowOffset: {
            width: 1,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        //marginTop: hpx(26)
    },
    welcomeStyle: {
        fontSize: nf(32),
        color: '#2DA461',
        fontFamily: Fonts.tofinoSemi,

    },
    startedStyle: {
        fontSize: nf(22),
        color: '#707070',
        fontFamily: Fonts.book_font,

    },
    eye: {
        marginTop: hpx(15),
        position: 'absolute',
        right: wpx(-10),



    },
    eye_icon: {
        width: wpx(37),
        tintColor: '#5DD986',
        height: hpx(28),
    },
    buttonText: {
        fontSize: nf(20),
        fontFamily: Fonts.tofinoSemi,

    },
    review: {
        height: hpx(131),
        width: wpx(349),
        alignContent: 'center',
        alignItems: 'center',


        justifyContent: 'center',
        alignSelf: 'center',
        paddingHorizontal: hpx(10),

        backgroundColor: '#A3E8BA',
        borderColor: '#A3E8BA',
        shadowColor: '#A3E8BA',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        borderRadius: 10,

    },
    cardView2: {

        backgroundColor: '#A3E8BA',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    seprateCardLine: {
        width: wp(90),
        height: hpx(0.9),
        backgroundColor: '#00000029',
        marginTop: hpx(20),
        opacity: 0.5,
    },
});
export default orderScreen;
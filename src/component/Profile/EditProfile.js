import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TextInput as NativeTextInput,
  StyleSheet,
  Image,
  ScrollView,
  SafeAreaView,
  Modal,
  TouchableOpacity,
} from 'react-native';
import { Colors, hpx, wpx, nf, Fonts } from '../../constants/constants';
import { TextInput } from 'react-native-paper';
let backIcon = require('../.././assets/images/backArrow.png');
let profilePhoto = require('../.././assets/images/noPhoto.png');
let cameraIcon = require('../.././assets/images/camera.png');
import ImagePicker from 'react-native-image-crop-picker';
import ImagePickerModal from '../../constants/ImagePickerModal';
import { useDispatch, shallowEqual, useSelector } from 'react-redux';
import { types } from '../../store/ActionTypes';
import { showMessage } from 'react-native-flash-message';

import Moment from 'moment';
// import { mixpanel } from '../../constants/constants';
let circleBlack = require('../.././assets/images/circleBlack.png');
import uncheckCircle from '../.././assets/images/uncheckCircle.png';
import ImageResizer from 'react-native-image-resizer';

import ProgressLoader from 'rn-progress-loader';
export default function EditProfile({ navigation }) {
  // mixpanel.track('Edit Profile private visited');
  const [bio, setBio] = useState('');
  const [fname, setFname] = useState('');
  const [Sname, setSname] = useState('');
  const [gender, setGender] = useState('');
  const [email_address, setEmail_address] = useState('');
  const [phone_number, setPhone_number] = useState('');
  const [currency_id, setCurrency_id] = useState('');
  const [currencyCode, setCurrencyCode] = useState('');
  const [newNumber, setNewNumber] = useState('');
  const [newCallingCode, setnewCallingCode] = useState('');
  const [state, setState] = useState({
    profileImage: '',
    imageModal: false,
    profilePicSend: '',
    modalIndex: '',
    currencyModal: false,
    profileThumb: '',
    contactModal: false,
    otpModal: false,
    countryCurrency: 'SAR',
    countryCode: 'SA',
    callingCode: '+91',
  });
  const theme = {
    roundness: 0,
    colors: {
      placeholder: '#999999',
      text: '#1D2226',
      primary: '#999999',
      borderColor: '#D1D1D1',
      background: '#D3EEDC',
    },
  };

  const { userData, loaderVisible, profile_Data } = useSelector(
    (state) => ({
      profile_Data: state.profileReducer.profile_Data,
      userData: state.settingReducer?.getUserProfile,
      loaderVisible: state.globalReducer.loader,
    }),
    shallowEqual,
  );
  const [loader, setloader] = useState(loaderVisible);
  useEffect(() => {
    console.log("loader", loaderVisible)
    setloader(loaderVisible)
  }, [loaderVisible]);
  useEffect(() => {
    defaultvalues();
  }, [profile_Data]);

  const defaultvalues = () => {
    setFname(profile_Data.name);
    setSname(profile_Data?.nice_name);
    setBio(
      Moment(profile_Data?.dob).format('DD/MM/YYYY'));
    // setGender(userData?.profile_data[0]?.user_gender);
    // setEmail_address(userData?.profile_data[0]?.user_email);
    // setPhone_number(userData?.profile_data[0]?.user_phone);
    // setCurrency_id(userData?.profile_data[0]?.currency_id);
    setState((prev) => ({
      ...prev,
      // profilePicSend: userData?.profile_data[0]?.avatar,
      profileImage: profile_Data?.avatar,
      profileThumb: profile_Data?.profile_pic_thumbnail,
      // callingCode: userData?.profile_data[0]?.country_code,
    }));
    // selectCurrency(userData?.profile_data[0]?.currency_id);
  };

  const contactModalOpen = () => {
    setState((prev) => ({
      ...prev,
      contactModal: !prev.contactModal,
    }));
  };

  const otpModalOpen = (e, f) => {
    setState((prev) => ({
      ...prev,
      contactModal: false,
      otpModal: !prev.otpModal,
    }));
    setNewNumber(e);
    setnewCallingCode(f)
  };

  const contactUpdate = (e, f) => {
    otpModalOpen(e, f);
  };

  // const modalOpen = () => {
  //   setState((prev) => ({
  //     ...prev,
  //     currencyModal: !prev.currencyModal,
  //   }));
  // };

  // const finalselectedCurrency_id = (e, f) => {
  //   if (currency_id && e == '') {
  //     setCurrency_id(currency_id);
  //   } else if (currency_id && e) {
  //     setCurrency_id(e);
  //     setCurrencyCode(f);
  //   }
  // };

  const ImageModal = () => {
    setState((prev) => ({
      ...prev,
      imageModal: !prev.imageModal,
    }));
  };

  const OpenCamera = () => {
    ImagePicker.openCamera({
      width: 400,
      height: 400,
      cropperCircleOverlay: true,
      cropping: true,
      mediaType: 'photo',
      compressImageQuality: 1,
    }).then((image) => {
      console.log('#. OpenCamera() : ', image.path);
      upload(image.path);
      setState((prev) => ({
        ...prev,
        profileImage: image.path,
        // imageModal: false,
      }));
    });
  };

  const OpenGallery = () => {
    ImagePicker.openPicker({
      width: 400,
      height: 400,
      cropperCircleOverlay: true,
      cropping: true,
      compressImageQuality: 1,
      mediaType: 'photo',
    }).then((image) => {
      console.log('#. OpenGallery() : ', image.path);
      upload(image.path);
      setState((prev) => ({
        ...prev,
        profileImage: image.path,
        // imageModal: false,
      }));
    });
  };

  const imageResize = (e) => {
    // console.log('imageResize==', e, item);
    ImageResizer.createResizedImage(e, 300, 400, 'JPEG', 70)
      .then((response) => {
        // this.props.profileImageUploadAction(response);
        // this.setState({
        //   filePath: response.uri
        // })
        upload1(response.uri);
      })
      .catch((err) => {
        //     console.log('resizer_error', err);
      });
  };

  const upload1 = (e) => {
    console.log('upload1==', e);
    // let name
    const file = {
      uri: e,
      name: 'thumbnail' + '_' + moment() + '.jpeg',
      type: 'image/jpeg',
    };
    const options = {
      keyPrefix: 'user-profile/user-profile-thumbnails/',
      bucket: 'saritoria-media-dev',
      region: 'us-east-1',
      accessKey: 'AKIASYUFVUQIQ5GJY6CM',
      secretKey: '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',
      successActionStatus: 201,
      acl: 'private',
    };
    // return RNS3.put(file, options)
    //   .then((response) => {
    //     if (response.status !== 201) {
    //       throw new Error('Failed to upload image to S3');
    //     } else {
    //       // console.log(
    //       //   'Successfully uploaded image to s1. s3 bucket url: ',
    //       //   response,
    //       //   response.body.postResponse.key,
    //       // );
    //       //let name = 'image' + '_' + moment()+'.jpeg';
    //       setState((prev) => ({
    //         ...prev,
    //         profileThumb: file.name,
    //         imageModal: false,
    //       }));
    //     }
    //   })
    //   .catch((err) => {
    //     //  console.log('Errorrr123', err.message);
    //   });
  };

  const upload = (e) => {
    const file = {
      uri: e,
      name: 'image' + '_' + moment() + '.jpeg',
      type: 'image/jpeg',
    };
    const options = {
      keyPrefix: 'user-profile/user-profile-images/',
      bucket: 'saritoria-media-dev',
      region: 'us-east-1',
      accessKey: 'AKIASYUFVUQIQ5GJY6CM',
      secretKey: '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',
      successActionStatus: 201,
      acl: 'private',
    };
    // return RNS3.put(file, options)
    //   .then((response) => {
    //     if (response.status !== 201) {
    //       throw new Error('Failed to upload image to S3');
    //     } else {
    //       console.log('file Name images', file.name);
    //       imageResize(e);
    //       setState((prev) => ({
    //         ...prev,
    //         profilePicSend: file.name,
    //       }));
    //     }
    //   })
    //   .catch((err) => {
    //     console.log('Errorrr', err.message);
    //   });
  };

  const dispatch = useDispatch();
  const updateProfile = () => {
    if (bio === '') {
      showMessage({
        message: 'Bio is mandatory.',
        type: 'danger',

        titleStyle: { textAlign: 'center' },
      });
    } else if (email_address === '') {
      showMessage({
        message: 'Email Address is mandatory.',
        type: 'danger',

        titleStyle: { textAlign: 'center' },
      });
    } else {
      let body = {
        name: fname.trim(),
        nick_name: Sname.trim(),
        dob: bio,
        //  profile_pic: state.profilePicSend,
        // user_gender: gender,
        // user_phone: phone_number,
        email: email_address,
        mobile: phone_number,
      };
      dispatch({
        type: types.TOGGLE_LOADING,
        payload: true,
      });

      dispatch({
        type: types.USER_PROFILE_UPDATE,
        payload: body,
      });
    }
  };

  const genderChange = (e) => {
    if (e == 1) {
      setGender('Male');
    } else if (e == 0) {
      setGender('Female');
    }
  };


  return (
    <SafeAreaView style={styles.maincontainer}>
      <ProgressLoader
        visible={loader}
        barHeight={100}
        isModal={true} isHUD={false}
        hudColor={"#5DD986"}
        color={"#FFFFFF"} />
      <Modal
        transparent
        visible={
          state.imageModal
            ? state.imageModal
            : state.currencyModal
              ? state.currencyModal
              : state.contactModal
                ? state.contactModal
                : state.otpModal
                  ? state.otpModal
                  : null
        }
        animationType="slide"
        onRequestClose={() =>
          setState((prev) => ({
            ...prev,
            imageModal: false,
            currencyModal: false,
            contactModal: false,
            otpModal: false,
          }))
        }>
        {state.imageModal ? (
          <ImagePickerModal
            ImageModal={ImageModal}
            OpenCamera={OpenCamera}
            OpenGallery={OpenGallery}
          />
        ) : state.currencyModal ? (
          <CurrencyPickerModal
            CurrencyModal={modalOpen}
            currencyId={currency_id}
            finalselectedCurrency_id={finalselectedCurrency_id}
          />
        ) : state.contactModal ? (
          <ContactPickerModal
            contactModalOpen={contactModalOpen}
            // currencyId={currency_id}
            contactUpdate={contactUpdate}
          />
        ) : state.otpModal ? (
          <OTPPickerModal
            otpModalOpen={otpModalOpen}
            getNumber={newNumber}
            getCallingCode={newCallingCode ? newCallingCode : callingCode}
          // contactUpdate={contactUpdate}
          />
        ) : null}
      </Modal>
      <View style={styles.commonHeaderView}>
        <TouchableOpacity
          activeOpacity={1.0}
          onPress={() => navigation.goBack()}>
          <Image
            source={backIcon}
            style={styles.backArrow}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <Text allowFontScaling={false} style={styles.heading}>
          My Profile
        </Text>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ paddingHorizontal: wpx(12) }}>
          <TouchableOpacity
            activeOpacity={1.0}
            style={styles.profileCircle}
            onPress={() => ImageModal()}>
            <Image
              source={
                state.profileImage ? { uri: state.profileImage } : profilePhoto
              }
              style={
                state.profileImage
                  ? { ...styles.profileCircle, ...{ marginTop: hpx(0) } }
                  : { width: wpx(120), height: hpx(120), marginTop: hpx(10) }
              }
              resizeMode="cover"
            />
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={1.0}
          //  onPress={() => ImageModal()}
          >
            <Image
              source={cameraIcon}
              style={{
                position: 'relative',
                left: wpx(200),
                marginTop: hpx(-22),
                width: 32,
                height: 32,
                borderRadius: 16,
              }}
              resizeMode="cover"
            />
          </TouchableOpacity>
          <View style={{
            elevation: 1,

            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.9, alignItems: 'center', marginTop: 20,

            shadowColor: 'black',
            shadowOpacity: 0.26,
            shadowOffset: { width: 0, height: 2 },
            shadowRadius: 10,
            elevation: 3,
            backgroundColor: '#D3EEDC',
          }}>
            <TextInput
              allowFontScaling={false}
              label="First Name"
              theme={theme}
              style={{ ...styles.inputStyle, marginTop: hpx(19) }}
              value={fname}
              onChangeText={(text) => setFname(text)}
              mode="outlined"
            />
            <TextInput
              allowFontScaling={false}
              label="Second Name"
              theme={theme}
              style={{ ...styles.inputStyle }}
              value={Sname}
              onChangeText={(text) => setSname(text)}
              mode="outlined"
            />

            <TextInput
              allowFontScaling={false}
              label="Email Address"
              theme={theme}
              style={{ ...styles.inputStyle }}
              value={email_address}
              onChangeText={(text) => setEmail_address(text)}
              mode="outlined"

            />

            <TextInput
              allowFontScaling={false}
              label="Mobile number"
              theme={theme}
              style={{ ...styles.inputStyle }}
              value={phone_number}
              onChangeText={(text) => setPhone_number(text)}
              mode="outlined"

            />
            <TextInput
              allowFontScaling={false}
              label="Birthday(DD/MM/YYYY)"
              theme={theme}
              style={{ ...styles.inputStyle }}
              value={bio}
              onChangeText={(text) => setBio(text)}
              mode="outlined"

            />
          </View>
        </View>
        <View style={styles.subButtonView}>

          <TouchableOpacity
            activeOpacity={1.0}
            onPress={() => updateProfile()}
            style={{
              ...styles.backButtonView,
              ...{ backgroundColor: '#A3E8BA', borderColor: 'transparent' },
            }}>
            <Text allowFontScaling={false} style={styles.backButtonText}>
              Save
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  maincontainer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  commonHeaderView: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: hpx(53),
    width: wpx(375),
    backgroundColor: 'white',
    paddingHorizontal: wpx(21),
  },
  backArrow: {
    width: wpx(16),
    height: hpx(16),
  },
  heading: {
    color: Colors.black,
    fontSize: nf(15),
    fontFamily: Fonts.tofino_pro_personal_book,
    marginLeft: wpx(15),
  },
  subButtonView: {
    paddingVertical: hpx(40),
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-between',

    marginHorizontal: wpx(15),
  },
  backButtonView: {
    width: wpx(107),
    height: hpx(40),
    borderWidth: wpx(1),
    borderColor: Colors.grey,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    shadowColor: '#707070',
    shadowOpacity: 0.26,
    shadowRadius: 10,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.26,

    elevation: 3,
  },
  backButtonText: {
    fontSize: nf(16),
    fontFamily: Fonts.tofino_pro_personal_book,
    color: Colors.black,
  },
  profileCircle: {
    width: 160,
    height: 160,
    borderRadius: 80,
    borderWidth: 0.6,
    borderColor: '#99999936',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: hpx(20),
    alignSelf: 'center',
    backgroundColor: '#E4E6E7',
  },
  inputStyle: {
    fontSize: nf(14),
    fontFamily: Fonts.tofino_pro_personal_book,
    width: wpx(314),
    height: hpx(45),
    marginBottom: hpx(26),
  },
  phoneNumText: {
    fontSize: nf(15),
    fontFamily: Fonts.tofino_pro_personal_book,
    color: Colors.pink,
    textAlign: 'right',
    marginBottom: hpx(15),
  },
  phoneView2: {
    width: wpx(2),
    height: hpx(20),
    backgroundColor: Colors.grey,
    marginLeft: wpx(10),
  },
  phoneView1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

import React, { Component, useEffect, useCallback, useState } from 'react';
import { Platform, Text, StyleSheet, Image, Linking, Dimensions, ScrollView, FlatList, TextInput, TouchableOpacity, View, SafeAreaViewBase, SafeAreaView, ImageBackground, } from 'react-native'
import { hpx, wp, wpx, nf, hp, Fonts, Colors } from '../../constants/constants';
let search = require('../.././assets/images/search.png');

import { useDispatch, shallowEqual, useSelector } from 'react-redux';
let location = require('../.././assets/images/pin.png');
let logo = require('../.././assets/images/logo.png');
let drow_down = require('../.././assets/images/down-arrow.png');
let meals_cooked = require('../.././assets/images/cookedmeals.jpg');
let demoImg = require('../.././assets/images/download.jpeg');
import Carousel from 'react-native-banner-carousel';
import { types } from '../../store/ActionTypes';
let meals_kit = require('../.././assets/images/mealkits.jpg');
let forwardArrow = require('../.././assets/images/forwardArrow.png');
let profilemain = require('../.././assets/images/profileMain.png');
let heartImg = require('../.././assets/images/heart.png');
let locationImag = require('../.././assets/images/location_profile.png');
let paymentImag = require('../.././assets/images/credit_card.png');
let purchaseImag = require('../.././assets/images/purchase.png');
let callImag = require('../.././assets/images/call.png');
let mailImg = require('../.././assets/images/mail.png');
let websiteImg = require('../.././assets/images/website.png');
let safetyImg = require('../.././assets/images/safety.png');
let covidImg = require('../.././assets/images/covid.png');
let hygineImg = require('../.././assets/images/hygine.png');
let greeDotImg = require('../.././assets/images/greenDot.png');
import AsyncStorage from '@react-native-async-storage/async-storage';
import AwesomeAlert from 'react-native-awesome-alerts';
import ProgressLoader from 'rn-progress-loader';
const images = [
    demoImg,
    meals_cooked,
    meals_kit
];
var userTokens;
const profileScreen = ({ navigation }) => {
    const [showAlertLogin, setAlertLogin] = useState(false);
    const [message_name, setmessage] = useState(false);
    const { loaderVisible, profile_Data, address_Data } = useSelector(
        (state) => ({
            profile_Data: state.profileReducer.profile_Data,
            address_Data: state.profileReducer.address_Data,
            loaderVisible: state.globalReducer.loader,

        }),
        shallowEqual,
    );
    useEffect(() => {
        getprofileData();

        getData();
    }, []);

    const getprofileData = () => {
        dispatch({
            type: types.TOGGLE_LOADING,
            payload: true,
        });

        dispatch({
            type: types.PROFILE_DATA,

        });
        dispatch({
            type: types.GET_ADDRESS,

        });
    }
    const getData = async () => {
        userTokens = await AsyncStorage.getItem('Token');

    }
    const CheckLogin = (routeName, messageName) => {
        if (userTokens) {
            navigation.navigate(routeName)
        } else {
            // navigation.navigate(routeName)
            setmessage(messageName);
            setAlertLogin(true);
        }
    }
    const keyExtractor = useCallback((item, index) => index.toString(), []);
    const renderPage = (image, index) => {
        return (
            <View key={index}>
                <Image style={{ width: wpx(543), height: hpx(146) }} source={image} />
            </View>
        );
    }

    const [loader, setloader] = useState(loaderVisible);
    useEffect(() => {
        console.log("loader", loaderVisible)
        setloader(loaderVisible)
    }, [loaderVisible]);
    const dispatch = useDispatch();

    const logOut = () => {
        dispatch({
            type: types.TOGGLE_LOADING,
            payload: true,
        });

        dispatch({
            type: types.LOGOUT_SAGA,

        });
    }
    const getStaticPage = (from) => {
        if (from !== 'Website') {

            dispatch({
                type: types.TOGGLE_LOADING,
                payload: true,
            });
            dispatch({
                type: types.STATIC_PAGES,
                payload: from
            });
            if (from === 'privacy-policy') {
                navigation.navigate('StaticPage', { from: 'Privacy & Policy' })
            } else if (from === 'terms-of-use-customer') {
                navigation.navigate('StaticPage', { from: 'Terms Of Use' })
            } else if (from === 'terms-and-conditions') {
                navigation.navigate('StaticPage', { from: 'Terms & Conditions' })
            }
            else if (from === 'food-hygiene') {
                navigation.navigate('StaticPage', { from: 'Food - Hygiene' })
            }
            else if (from === 'covid-19-measures') {
                navigation.navigate('StaticPage', { from: 'Covid - 19 - Measures' })
            }
            else if (from === 'safe-delivery') {
                navigation.navigate('StaticPage', { from: 'Safe - Delivery' })
            }

        } else {
            navigation.navigate('StaticPage', { from: 'Website' })
        }
    };
    return (


        <SafeAreaView style={{ flex: 1, }}>
            <AwesomeAlert
                show={showAlertLogin}
                showProgress={false}
                title={"You have to log in to access " + message_name}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}

                cancelText="Cancel"
                cancelButtonTextStyle={{ color: '#000', }}
                confirmButtonTextStyle={{ color: '#000' }}
                confirmText="Login"
                cancelButtonColor='#5DD986'
                confirmButtonColor="#5DD986"
                onCancelPressed={() => {
                    setAlertLogin(false)

                }}
                onConfirmPressed={() => {
                    setAlertLogin(false)

                    AsyncStorage.clear();
                    dispatch({
                        type: types.DESTROY_SESSION,
                    });
                    // RootNavigation.navigate('Login')

                }}
            />
            <ProgressLoader
                visible={loader}
                barHeight={100}
                isModal={true} isHUD={false}
                hudColor={"#5DD986"}
                color={"#FFFFFF"} />


            <View style={{ height: '15%', flexDirection: 'row', alignContent: 'center', backgroundColor: '#5DD986' }}>
                <View style={styles.card}>



                </View>

                <View style={styles.location_style}>
                    <View style={styles.address}>

                        <Text style={{ fontFamily: Fonts.tofino_pro_personal_Regular, color: '#FFFFFF', fontSize: nf(18), }}>Hello</Text>
                    </View>
                    <View style={styles.address}>

                        <Text style={{ fontFamily: Fonts.tofino_pro_personal_Regular, color: '#FFFFFF', fontSize: nf(24), }}>{profile_Data?.name} </Text>

                    </View>
                </View>
            </View>
            <ScrollView style={{ width: '100%', height: hpx(230), }}>
                <View style={{ justifyContent: 'flex-start', height: '85%', paddingHorizontal: 5, marginTop: hpx(40), paddingHorizontal: 15 }}>

                    <Text style={{ fontFamily: Fonts.tofino_pro_personal_Regular, fontSize: nf(18), top: hpx(10), }}>My Account</Text>


                    <View style={styles.designerNameView}>

                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={styles.contentView}
                            onPress={() => CheckLogin('EditProfile', 'Profile')}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', align: 'center' }}>
                                <Image
                                    source={profilemain}
                                    style={{ width: wpx(28.68), height: hpx(28.68), marginEnd: wpx(30) }}
                                    resizeMode="contain"
                                />
                                <Text allowFontScaling={false} style={styles.contentText}>
                                    My Profile
                                </Text>
                            </View>

                            <Image
                                source={forwardArrow}
                                style={{ ...styles.contentImage, end: 0 }}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                        <View style={styles.seprateViewLine} />

                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={styles.contentView}
                            onPress={() => CheckLogin('FavoriteProduct', 'Favorite')}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', align: 'center' }}>
                                <Image
                                    source={heartImg}
                                    style={{ width: wpx(28.68), height: hpx(28.68), marginEnd: wpx(30) }}
                                    resizeMode="contain"
                                />
                                <Text allowFontScaling={false} style={styles.contentText}>
                                    Favorites
                                </Text>
                            </View>
                            <Image
                                source={forwardArrow}
                                style={{ ...styles.contentImage, end: 0 }}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                        <View style={styles.seprateViewLine} />

                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={styles.contentView}
                            onPress={() => CheckLogin('EditAddress', 'Address')}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', align: 'center' }}>
                                <Image
                                    source={locationImag}
                                    style={{ width: wpx(28.68), height: hpx(28.68), marginEnd: wpx(30) }}
                                    resizeMode="contain"
                                />
                                <Text allowFontScaling={false} style={styles.contentText}>
                                    Addresses
                                </Text>
                            </View>
                            <Image
                                source={forwardArrow}
                                style={{ ...styles.contentImage, end: 0 }}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                        <View style={styles.seprateViewLine} />

                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={styles.contentView}
                            onPress={() => CheckLogin('PaymentMethod', 'PaymentMethod')}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', align: 'center' }}>
                                <Image
                                    source={paymentImag}
                                    style={{ width: wpx(28.68), height: hpx(28.68), marginEnd: wpx(30) }}
                                    resizeMode="contain"
                                />
                                <Text allowFontScaling={false} style={styles.contentText}>
                                    Payment Details
                                </Text>
                            </View>
                            <Image
                                source={forwardArrow}
                                style={{ ...styles.contentImage, end: 0 }}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                        <View style={styles.seprateViewLine} />

                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={styles.contentView}
                            onPress={() => CheckLogin('orderScreen', 'Order')}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', align: 'center' }}>
                                <Image
                                    source={purchaseImag}
                                    style={{ width: wpx(28.68), height: hpx(28.68), marginEnd: wpx(30) }}
                                    resizeMode="contain"
                                />
                                <Text allowFontScaling={false} style={styles.contentText}>
                                    Purchase History
                                </Text>
                            </View>
                            <Image
                                source={forwardArrow}
                                style={{ ...styles.contentImage, end: 0 }}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                        <View style={styles.seprateViewLine} />
                    </View>
                    <Text style={{ fontFamily: Fonts.tofino_pro_personal_Regular, fontSize: nf(18), top: hpx(20), paddingTop: 20, marginBottom: hpx(8) }}>Support</Text>

                    <View style={{ ...styles.designerNameView, height: hpx(169) }}>

                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={styles.contentView}
                        // onPress={() => Linking.openURL('tel:8777111223')}
                        >
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', align: 'center' }}>
                                <Image
                                    source={callImag}
                                    style={{ width: wpx(28.68), height: hpx(28.68), marginEnd: wpx(30) }}
                                    resizeMode="contain"
                                />

                                <Text allowFontScaling={false} style={styles.contentText}>
                                    Call us
                                </Text>
                            </View>

                            <Text allowFontScaling={false} style={styles.contentText}>
                                +971 4 xxx xxxx
                            </Text>
                        </TouchableOpacity>
                        <View style={styles.seprateViewLine} />

                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={styles.contentView}
                        // onPress={() => Linking.openURL('mailto:info@springs15.com')}
                        >
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', align: 'center' }}>
                                <Image
                                    source={mailImg}
                                    style={{ width: wpx(28.68), height: hpx(28.68), marginEnd: wpx(30) }}
                                    resizeMode="contain"
                                />
                                <Text allowFontScaling={false} style={styles.contentText}>
                                    Mail us
                                </Text>
                            </View>
                            <Text allowFontScaling={false} style={styles.contentText}>
                                info@springs15.com
                            </Text>
                        </TouchableOpacity>
                        <View style={styles.seprateViewLine} />

                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={{ ...styles.contentView }}
                        // onPress={() => getStaticPage('Website')}
                        >
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', align: 'center' }}>
                                <Image
                                    source={websiteImg}
                                    style={{ width: wpx(28.68), height: hpx(28.68), marginEnd: wpx(30) }}
                                    resizeMode="contain"
                                />
                                <Text allowFontScaling={false} style={styles.contentText}>
                                    Website
                                </Text>
                            </View>
                            <Text allowFontScaling={false} style={styles.contentText}>
                                www.springs15.com
                            </Text>
                        </TouchableOpacity>
                        <View style={styles.seprateViewLine} />



                    </View>
                    <Text style={{ fontFamily: Fonts.tofino_pro_personal_Regular, fontSize: nf(18), top: hpx(20), paddingTop: 20, marginBottom: hpx(8) }}>Food Safety</Text>

                    <View style={{ ...styles.designerNameView, height: hpx(169) }}>

                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={{ ...styles.contentView }}
                            onPress={() => getStaticPage('food-hygiene')}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', align: 'center' }}>
                                <Image
                                    source={hygineImg}
                                    style={{ width: wpx(28.68), height: hpx(28.68), marginEnd: wpx(30) }}
                                    resizeMode="contain"
                                />
                                <Text allowFontScaling={false} style={styles.contentText}>
                                    Food Hygiene
                                </Text>
                            </View>

                            <Image
                                source={forwardArrow}
                                style={{ ...styles.contentImage, end: 0 }}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                        <View style={styles.seprateViewLine} />

                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={{ ...styles.contentView }}
                            onPress={() => getStaticPage('covid-19-measures')}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', align: 'center' }}>
                                <Image
                                    source={covidImg}
                                    style={{ width: wpx(28.68), height: hpx(28.68), marginEnd: wpx(30) }}
                                    resizeMode="contain"
                                />
                                <Text allowFontScaling={false} style={styles.contentText}>
                                    Covid-19 Measures
                                </Text>
                            </View>
                            <Image
                                source={forwardArrow}
                                style={{ ...styles.contentImage, end: 0 }}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                        <View style={styles.seprateViewLine} />

                        <TouchableOpacity
                            activeOpacity={1.0}
                            style={{ ...styles.contentView }}
                            onPress={() => getStaticPage('safe-delivery')}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', align: 'center' }}>
                                <Image
                                    source={safetyImg}
                                    style={{ width: wpx(28.68), height: hpx(28.68), marginEnd: wpx(30) }}
                                    resizeMode="contain"
                                />
                                <Text allowFontScaling={false} style={styles.contentText}>
                                    Safe Delivery
                                </Text>
                            </View>
                            <Image
                                source={forwardArrow}
                                style={{ ...styles.contentImage, end: 0 }}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                        <View style={styles.seprateViewLine} />



                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', height: hpx(120), marginTop: 40, alignItems: 'center', alignContent: 'center', justifyContent: 'space-between' }}>
                        <TouchableOpacity
                            activeOpacity={1.0}

                            onPress={() => logOut()}>
                            <Text allowFontScaling={false} style={{ ...styles.contentText, color: '#257640', }}>
                                SIGN OUT
                            </Text>
                        </TouchableOpacity>
                        <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignSelf: 'center', alignContent: 'center' }}>
                                <TouchableOpacity
                                    activeOpacity={1.0}
                                    style={{ ...styles.contentView }}
                                    onPress={() => getStaticPage('terms-of-use-customer')}>
                                    <Text allowFontScaling={false} style={{ ...styles.contentText, fontSize: nf(11), bottom: hpx(4) }}>
                                        Terms of Sale
                                    </Text>
                                    <Image
                                        source={greeDotImg}
                                        style={{ width: wpx(25.44), height: hpx(25.44), }}
                                        resizeMode="contain"
                                    />
                                </TouchableOpacity>


                            </View>

                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignSelf: 'center', alignContent: 'center' }}>
                                <TouchableOpacity
                                    activeOpacity={1.0}
                                    style={{ ...styles.contentView }}
                                    onPress={() =>
                                        getStaticPage('terms-and-conditions')}
                                >
                                    <Text allowFontScaling={false} style={{ ...styles.contentText, fontSize: nf(11), bottom: hpx(4) }}>
                                        Terms &amp; Conditions
                                    </Text>
                                    <Image
                                        source={greeDotImg}
                                        style={{ width: wpx(25.44), height: hpx(25.44), }}
                                        resizeMode="contain"
                                    />

                                </TouchableOpacity>

                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignSelf: 'center', alignContent: 'center' }}>
                                <TouchableOpacity
                                    activeOpacity={1.0}
                                    style={{ ...styles.contentView }}
                                    onPress={() =>
                                        getStaticPage('privacy-policy')}
                                >
                                    <Text allowFontScaling={false} style={{ ...styles.contentText, fontSize: nf(11), bottom: hpx(4) }}>
                                        Privacy Policy
                                    </Text>
                                </TouchableOpacity>
                                {/* <Image
                                    source={greeDotImg}
                                    style={{ width: wpx(25.44), height: hpx(25.44), }}
                                    resizeMode="contain"
                                /> */}

                            </View>


                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView >


    );
}

const styles = StyleSheet.create({

    searchBox: {
        height: hpx(49),
        //width: wpx(340),
        backgroundColor: '#5DD986',
        borderRadius: hpx(7),
        borderColor: '#E4E6E772',
        marginTop: hpx(5),
        flexDirection: 'row',

        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: wpx(10),
    },
    address: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
        alignContent: 'center',
        bottom: hpx(40)

    },

    card: {
        height: hpx(111),
        justifyContent: 'center',
        borderRadius: wpx(3)
    },
    location_style: {
        flex: 1,
        justifyContent: 'center',
        height: hpx(126),
        margin: 20, bottom: 10
    },

    designerImage: {
        width: wpx(105),
        height: hpx(106),
        //    backgroundColor: 'black'


    },
    designerNameView: {

        // justifyContent: 'space-between',
        width: wpx(347),
        height: hpx(289),
        backgroundColor: '#D3EEDC',

        // alignItems: 'flex-start',
        // alignContent: 'flex-start',
        // alignSelf: 'flex-start',

        borderColor: Colors.grey,
        marginTop: hpx(20),
        borderRadius: 10,
        elevation: 1,
        shadowColor: '#00000029',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,

    },
    designerNameText: {

        top: 2,
        fontSize: nf(12),
        fontFamily: Fonts.tofino_pro_personal_book,
        textAlign: 'center',
        textAlignVertical: 'center',
        color: Colors.black,

    },
    contentView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: wpx(5),
        marginTop: hpx(15),
    },
    contentText: { fontSize: nf(16), fontFamily: Fonts.tofino_pro_personal_book, color: '#000000', },
    contentImage: {
        width: wpx(24.87),
        height: hpx(24.87),
    },
    seprateViewLine: {

        height: hpx(0.5),
        backgroundColor: Colors.grey,
        opacity: 0.5,
        marginHorizontal: wpx(10),
        marginTop: hpx(10),
    },
});
export default profileScreen;
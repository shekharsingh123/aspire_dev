import React, { Component, useEffect, useCallback, useState } from 'react';
import { Platform, Text, StyleSheet, Image, Dimensions, ScrollView, FlatList, TextInput, TouchableOpacity, View, SafeAreaViewBase, SafeAreaView, ImageBackground, } from 'react-native'
import { hpx, wpx, nf, hp, Fonts, Colors } from '../../constants/constants';
let search = require('../.././assets/images/search.png');
let location = require('../.././assets/images/pin.png');
let logo = require('../.././assets/images/logo.png');
let drow_down = require('../.././assets/images/down-arrow.png');
let meals_cooked = require('../.././assets/images/cookedmeals.jpg');
let demoImg = require('../.././assets/images/download.jpeg');
import Carousel from 'react-native-banner-carousel';
let backImg = require('../.././assets/images/backArrow.png');
let backIcon = require('../.././assets/images/backArrow.png');
let infoImg = require('../.././assets/images/info.png');
let addImg = require('../.././assets/images/add.png');
let plusImg = require('../.././assets/images/plus.png');
import { back } from 'react-native/Libraries/Animated/Easing';
let meals_kit = require('../.././assets/images/mealkits.jpg');
let main_logo = require('../.././assets/images/spring.png');
let select_heart = require('../.././assets/images/selected_heart.png');
let remove_fav = require('../.././assets/images/remove_fav.png');
import ProgressLoader from 'rn-progress-loader';
import { types } from '../../store/ActionTypes';
let minusImg = require('../.././assets/images/minus.png');
import { useDispatch, shallowEqual, useSelector } from 'react-redux';
const images = [
    demoImg,
    meals_cooked,
    meals_kit
];
const FavoriteProduct = (props) => {
    const { loaderVisible, banner_Data, products_list, selected_Cat, categories_Data } = useSelector(
        (state) => ({
            loaderVisible: state.globalReducer.loader,
            products_list: state.homeReducer.wishlist_item,
            banner_Data: state.homeReducer.banner_Data,
            categories_Data: state.homeReducer.categories_Data,
            selected_Cat: state.homeReducer.selected_Cat

        }),
        shallowEqual,
    );
    const dispatch = useDispatch();
    const [inc_dec, setIncDec] = useState(0);
    const [productArray, setProductArray] = useState([]);
    const [loader, setloader] = useState(loaderVisible);
    useEffect(() => {
        console.log("loader", loaderVisible)
        setloader(loaderVisible)
    }, [loaderVisible]);
    const callDescription = (item) => {

        dispatch({
            type: types.TOGGLE_LOADING,
            payload: true,
        });
        dispatch({
            type: types.PRODUCT_DETAILS,
            payload: item.listing_id
        });

    }
    const removeWishlistItem = (item) => {

        dispatch({
            type: types.TOGGLE_LOADING,
            payload: true,
        });
        dispatch({
            type: types.REMOVE_WISHLIST_ITEM,
            payload: item.id
        });

    }
    const callNav = (from, item, index) => {

        if (from === 'minus' && inc_dec > 0) {
            // Object.assign(item, { quantity: item.quantity - 1});
            setIncDec(inc_dec - 1)
            productArray[index].quantity = item.quantity - 1;

        } else if (from === 'plus') {
            productArray[index].quantity = item.quantity + 1;

            setIncDec(inc_dec + 1)
        } else {
            setIncDec(0)
            productArray[index].quantity = 0;
        }
        console.log("productArray", productArray)


    };
    useEffect(() => {
        getBannerData();
    }, []);
    useEffect(() => {
        if (products_list?.length > 0) {
            products_list.map((item) => {
                Object.assign(item, { quantity: 0 });

            })
        }
        setProductArray(products_list);
        console.log("productArray", productArray)

    }, [products_list]);

    const getBannerData = (from) => {


        dispatch({
            type: types.TOGGLE_LOADING,
            payload: true,
        });



        dispatch({
            type: types.GET_WISHLIST,
            // payload: props?.route?.params?.cat_id
        });
        // dispatch({
        //     type: types.GET_CATEGORY,
        // });
    };


    const keyExtractor = useCallback((item, index) => index.toString(), []);

    return (

        <SafeAreaView style={{ flex: 1, }}>


            <ProgressLoader
                visible={loader}
                barHeight={100}
                isModal={true} isHUD={false}
                hudColor={"#5DD986"}
                color={"#FFFFFF"} />
            <View style={{ height: '8%', flexDirection: 'row', alignContent: 'center', }}>
                <View style={styles.commonHeaderView}>
                    <TouchableOpacity
                        activeOpacity={1.0}
                        onPress={() => props.navigation.goBack()}>
                        <Image
                            source={backIcon}
                            style={styles.backArrow}
                            resizeMode="contain"
                        />
                    </TouchableOpacity>
                    <Text allowFontScaling={false} style={styles.heading}>
                        Favorites
                    </Text>
                    <Image source={main_logo}
                        style={

                            { width: wpx(150), height: hpx(150), right: wpx(-135), marginBottom: hpx(50), marginTop: hpx(20) }
                        }
                    >

                    </Image>
                </View>

            </View>

            <View style={{ justifyContent: 'flex-start', height: '92%', marginTop: hpx(20), paddingHorizontal: hpx(20) }}>


                {productArray?.length > 0 ? (

                    <FlatList

                        data={productArray}
                        numColumns={3}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item, index }) =>

                        (
                            <TouchableOpacity
                                activeOpacity={1.0}
                            // onPress={() => select(item)}
                            >
                                <View style={styles.designerNameView}>
                                    {
                                        item.image ?
                                            <ImageBackground
                                                style={styles.designerImage}
                                                source={{ uri: item.image }}
                                                resizeMode="cover"
                                            /> : <ImageBackground
                                                style={styles.designerImage}
                                                source={meals_cooked}
                                                resizeMode="cover"
                                            />
                                    }

                                    <TouchableOpacity
                                        activeOpacity={1.0}
                                        style={{ position: 'absolute', top: 1, left: -5 }}
                                        onPress={() => callDescription(item)}
                                    >
                                        <Image source={infoImg}></Image>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        activeOpacity={1.0}
                                        style={{ position: 'absolute', top: 7, right: 5, tintColor: 'white' }}
                                        onPress={() => removeWishlistItem(item)}
                                    >
                                        <Image source={remove_fav} ></Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={{ position: 'absolute', bottom: 25, right: 5, }}
                                        onPress={() => callNav('plus', item, index)}>
                                        <Image source={select_heart} >

                                        </Image>
                                    </TouchableOpacity>



                                    <Text
                                        allowFontScaling={false}
                                        style={styles.designerNameText}>
                                        {item.title}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        )}
                        keyExtractor={keyExtractor}
                    />) : <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                    <Text>No Product found

                    </Text>
                </View>}


            </View>


        </SafeAreaView>

    );
}

const styles = StyleSheet.create({

    searchBox: {
        height: hpx(49),
        //width: wpx(340),
        backgroundColor: '#5DD986',
        borderRadius: hpx(7),
        borderColor: '#E4E6E772',
        marginTop: hpx(5),
        flexDirection: 'row',

        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: wpx(10),
    },
    heading: {
        color: Colors.black,
        fontSize: nf(15),
        fontFamily: Fonts.tofino_pro_personal_book,
        marginLeft: wpx(15),
    },
    backArrow: {
        width: wpx(16),
        height: hpx(16),
    },
    address: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
        alignContent: 'center',
        bottom: hpx(20)
    },
    selectInfo: {

        marginLeft: wpx(10),
        fontSize: nf(14),
        fontFamily: Fonts.tofino_pro_personal_book,
        padding: 2,
    },
    commonHeaderView: {

        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: hpx(53),
        top: 20,
        width: wpx(175),
        paddingHorizontal: wpx(21),
    },
    location_style: {
        flex: 1,
        justifyContent: 'center',
        height: hpx(126),
        margin: 20, bottom: 1
    },

    designerImage: {
        width: wpx(105),
        height: hpx(106),
        //    backgroundColor: 'black'


    },
    designerNameView: {

        justifyContent: 'center',
        width: wpx(105),
        height: hpx(131),
        borderRadius: 5,
        alignItems: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        marginHorizontal: 8,
        borderColor: Colors.grey,
        marginTop: hpx(20),

        elevation: 3,

        shadowOffset: { width: 0, height: 2 },
        shadowColor: '#707070',
        shadowOpacity: 0.26,
        shadowRadius: 10,

        backgroundColor: 'white',
    },
    designerNameText: {

        top: 2,
        fontSize: nf(12),
        fontFamily: Fonts.tofino_pro_personal_book,
        textAlign: 'center',
        textAlignVertical: 'center',
        color: Colors.black,

    },
    cart_button: {
        height: hpx(43),
        width: wpx(115),

        alignContent: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
        //marginTop: hpx(26)
    },
    button: {
        height: hpx(20),
        width: wpx(50),
        paddingHorizontal: hpx(4),

        alignContent: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 6,
        //marginTop: hpx(26)
    },
});
export default FavoriteProduct;
import React, { useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { WebView } from 'react-native-webview';
import {
  Colors,
  hpx,
  wpx,
  nf,
  Fonts,
  serviceUrl
} from '../../constants/constants';

import { types } from '../../store/ActionTypes';

let backIcon = require('../.././assets/images/backArrow.png');
import HTML from 'react-native-render-html';

import { useDispatch, shallowEqual, useSelector } from 'react-redux';

export default function Aboutus({ navigation, route }) {
  const { from } = route.params;

  const { staticPage, loaderVisible } = useSelector(
    (state) => ({
      staticPage: state.profileReducer.staticPage,
      loaderVisible: state.globalReducer.loader,
    }),
    shallowEqual,
  );


  const tagsStyles = {
    body: {
      whiteSpace: 'normal',
      color: 'gray',


    },
    a: {
      color: 'green'
    }
  };


  return (
    <SafeAreaView style={styles.maincontainer}>
      <View style={styles.commonHeaderView}>
        <TouchableOpacity activeOpacity={1.0} onPress={() => navigation.goBack()}>
          <Image
            source={backIcon}
            style={styles.backArrow}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <Text allowFontScaling={false} style={styles.heading}>Account</Text>
      </View>
      <ScrollView>

        <Text allowFontScaling={false} style={styles.contentText1}>{from}</Text>
        <View
          style={{
            ...styles.maincontainer,
            marginLeft: wpx(15),
            marginRight: wpx(15),
            paddingHorizontal: hpx(10),
            backgroundColor: '#D3EEDC'
          }}>
          {
            from !== 'Website' ?
              <HTML
                source={{ html: staticPage?.content }}
                baseFontStyle={{ fontFamily: Fonts.book_font, fontSize: nf(14), }}
                containerStyle={{ marginTop: hpx(10), }}
                contentWidth={500}
                tagsStyles={tagsStyles}
              /> : <WebView startInLoadingState={true} style={{ flex: 1, height: 1000 }} source={{ uri: serviceUrl.webUrl }} />
          }

        </View>
      </ScrollView>
    </SafeAreaView>

  );
}

const styles = StyleSheet.create({
  maincontainer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  commonHeaderView: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: hpx(53),
    width: wpx(375),
    backgroundColor: 'white',
    paddingHorizontal: wpx(21),
  },
  backArrow: {
    width: wpx(16),
    height: hpx(16),
  },
  heading: {
    color: Colors.black,
    fontSize: nf(18),
    fontFamily: Fonts.regular,
    marginLeft: wpx(15),
  },
  contentText1: {
    fontFamily: Fonts.regular,
    fontSize: nf(22),
    marginTop: hpx(10),
    marginHorizontal: wpx(15),
    marginBottom: hpx(30)
  },
});

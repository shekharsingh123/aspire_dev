import React, { Component, useEffect, useCallback, useState } from 'react';
import { Platform, Text, StyleSheet, Image, Dimensions, ScrollView, FlatList, TextInput, TouchableOpacity, View, SafeAreaViewBase, SafeAreaView, ImageBackground, } from 'react-native'
import { hpx, wpx, Colors, nf, Fonts, hp } from '../constants/constants';
let location = require('../assets/images/pin.png');
let logo = require('../assets/images/logo.png');
let drow_down = require('../assets/images/down-arrow.png');
let meals_kit = require('../assets/images/mealkits.jpg');
let meals_cooked = require('../assets/images/cookedmeals.jpg');
let notice_board = require('../assets/images/notice.png');
import ProgressLoader from 'rn-progress-loader';
import { types } from '../store/ActionTypes';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { useDispatch, shallowEqual, useSelector } from 'react-redux';
export default function SelectionScreen(props) {
    const [cat_meals_kit, setmeals_kit] = useState('');

    const { loaderVisible, banner_Data, products_list, categories_Data } = useSelector(
        (state) => ({
            loaderVisible: state.globalReducer.loader,
            products_list: state.homeReducer.products_list,
            banner_Data: state.homeReducer.banner_Data,
            categories_Data: state.homeReducer.categories_Data,

        }),
        shallowEqual,
    );
    const dispatch = useDispatch();
    const [loader, setloader] = useState(loaderVisible);
    useEffect(() => {
        console.log("loader", loaderVisible)
        setloader(loaderVisible)
    }, [loaderVisible]);
    const BannerWidth = Dimensions.get('window').width;
    const BannerHeight = 260;

    useEffect(() => {
        getcatdata();
    }, []);
    const keyExtractor = useCallback((item, index) => index.toString(), []);
    const callNav = () => {
        AsyncStorage.setItem("SELECTED_CATEGORY", JSON.stringify(cat_meals_kit), (err) => {
            if (err) {
                console.log("an error");
                throw err;
            }
            console.log("success");
        }).catch((err) => {
            console.log("error is: " + err);
        });
        dispatch({
            type: types.SELECTED_CATEGORY,
            payload: JSON.stringify(cat_meals_kit),
        });
        dispatch({
            type: types.RENDER_AGAIN_STACK_NAV,

        });
    }
    const selectCategories = (item) => {

        if (item.id == 42) {
            setmeals_kit(item)
        } else {
            AsyncStorage.setItem("SELECTED_CATEGORY", JSON.stringify(item), (err) => {
                if (err) {
                    console.log("an error");
                    throw err;
                }
                console.log("success");
            }).catch((err) => {
                console.log("error is: " + err);
            });
            dispatch({
                type: types.SELECTED_CATEGORY,
                payload: JSON.stringify(item),
            });
            dispatch({
                type: types.RENDER_AGAIN_STACK_NAV,

            });
        }


    }
    const getcatdata = (from) => {


        dispatch({
            type: types.TOGGLE_LOADING,
            payload: true,
        });
        dispatch({
            type: types.GET_CATEGORY,
        });
    };
    const [isVisible, setIsVisible] = useState(true);
    const [password, setPassword] = useState('');
    return (
        <View
            style={styles.MainContainer} >
            <ProgressLoader
                visible={loader}
                barHeight={100}
                isModal={true} isHUD={false}
                hudColor={"#5DD986"}
                color={"#FFFFFF"} />
            <View style={{ flex: 1, height: '30%', marginVertical: Platform.OS == 'ios' ? hpx(10) : 0, flexDirection: 'row', alignContent: 'center', }}>
                <View style={styles.card}>
                    <Image
                        source={logo}
                        style={styles.logoStyle}></Image>


                </View>

                <View style={styles.location_style}>
                    <View style={styles.address}>
                        <Image source={location} style={{
                            width: wpx(13.5), height: hpx(16.5), bottom: hpx(5),
                            marginRight: wpx(5)
                        }}></Image>
                        <Text style={{ fontFamily: Fonts.pangram_regular, color: '#707070', fontSize: nf(18), }}>Deliver to</Text>
                    </View>
                    <View style={styles.address}>

                        <Text style={{ fontFamily: Fonts.pangram_regular, color: '#2DA461', fontSize: nf(24), }}>HOME </Text>
                        <Image source={drow_down} style={{ tintColor: '#2DA461', width: wpx(13.7), height: hpx(7.83), bottom: hpx(10) }}></Image>
                    </View>
                </View>
            </View>
            <View style={styles.celebrateView}>

                {
                    cat_meals_kit ?
                        <View style={styles.main_notice}>
                            <View style={styles.noticeBoard}>
                                <View style={{ width: '100%', height: hpx(60), flexDirection: 'row', justifyContent: 'flex-start', alignContent: 'center', paddingHorizontal: hpx(20), alignItems: 'center', alignSelf: 'center' }}>
                                    <Image source={notice_board} style={{
                                        width: wpx(26.5), height: hpx(34.5),


                                    }}></Image>
                                    <Text style={{ fontFamily: Fonts.tofinoSemi, marginLeft: wpx(15), color: '#010101', fontSize: nf(18), top: 8 }}>Important Notice</Text>
                                </View>
                                <Text style={{ fontFamily: Fonts.tofino_pro_personal_book, marginLeft: wpx(15), color: '#010101', fontSize: nf(17), top: 8 }}>To keep the maximum quality and freshness of the food, Meal Kits are available for Pre-order from Saturdays through Thursdays and the delivery will  be done every Saturday. </Text>
                                <Text style={{ fontFamily: Fonts.tofino_pro_personal_book, marginLeft: wpx(15), color: '#010101', fontSize: nf(17), top: 18 }}>
                                    Cut off for Orders throughout the week at 8:00 PM. </Text>

                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    // backgroundColor: 'black',
                                    alignContent: 'center',

                                    flex: 1,
                                    justifyContent: 'space-between',
                                    alignItems: 'center',




                                }}>
                                <TouchableOpacity
                                    onPress={() => setmeals_kit('')}
                                    style={{
                                        ...styles.button,
                                        backgroundColor: '#D3EEDC',
                                        borderWidth: 1,
                                        borderColor: '#D3EEDC',
                                    }}>
                                    <Text style={{ ...styles.buttonText, color: '#010101', fontSize: nf(16), }}>
                                        Back
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => callNav()}
                                    style={{
                                        ...styles.button,
                                        backgroundColor: '#D3EEDC',
                                        borderWidth: 1,
                                        borderColor: '#D3EEDC',
                                    }}>
                                    <Text style={{ ...styles.buttonText, color: '#010101', fontSize: nf(16), }}>Proceed</Text>
                                </TouchableOpacity>
                            </View>

                        </View> :
                        categories_Data?.length > 0 && (
                            <FlatList

                                data={categories_Data.slice(0, 8)}
                                numColumns={3}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item }) => (
                                    <TouchableOpacity
                                        activeOpacity={1.0}
                                        onPress={() => selectCategories(item)}
                                    >
                                        <View style={styles.mainCat}>
                                            {
                                                item.cover_image ? <Image style={styles.celebrateImage} source={{ uri: item.cover_image }} />
                                                    :
                                                    <Image style={styles.celebrateImage} source={item.cover_image} />

                                            }

                                            <View
                                                style={{

                                                    borderColor: '#707070',
                                                    backgroundColor: '#C0C0C0'

                                                }}>
                                                <Text style={styles.celebrateText}>{item.name}</Text>

                                            </View>

                                        </View>
                                    </TouchableOpacity>
                                )}
                                keyExtractor={keyExtractor}
                            />)
                }




            </View>
            <View >

            </View>


        </View >


    );
}


const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,

        backgroundColor: '#F8FBF9',

    },

    celebrateView: {

        alignContent: 'center',
        flexDirection: 'row',
        marginHorizontal: hpx(10),

        justifyContent: 'center',


        height: '70%',

    },
    mainCat: {

        flex: 1,

        padding: wpx(10),
        flexDirection: 'column',
        justifyContent: 'space-between',


    },
    noticeBoard: {

        height: hpx(297),
        width: wpx(347),
        backgroundColor: '#D3EEDC',
        padding: wpx(10),
        flexDirection: 'column',



    },
    main_notice: {


        height: hpx(400),
        padding: wpx(10),
        flexDirection: 'column',



    },



    celebrateImage: { width: wpx(155), height: hpx(218), },
    celebrateText: {
        padding: hpx(5),
        fontSize: nf(14),
        textAlign: 'center',
        color: '#000000',
        textAlign: 'center',
        textAlignVertical: "center",
        alignSelf: "center",
        fontFamily: Fonts.tofinoSemi,

    },
    address: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
        alignContent: 'center',
        bottom: hpx(25)
    },
    location_style: {
        flex: 1,
        justifyContent: 'center',
        height: hpx(126),
        margin: 20, bottom: 10
    },
    SectionStyle: {
        flex: 1,
        marginStart: wpx(25),
        marginEnd: wpx(30),
        marginTop: wpx(20),
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    inputStyle: {
        borderBottomColor: '#707070', // Add this to specify bottom border color
        width: wpx(320),
        borderBottomWidth: 1,
        fontSize: nf(22),
        fontFamily: Fonts.tofinoSemi,

    },
    logoStyle: {
        width: wpx(200),
        top: hpx(-10),
        height: hpx(350),
        tintColor: '#2DA461'
    },
    card: {

        height: hpx(111),
        justifyContent: 'center',
        borderRadius: wpx(3)
    },
    button: {
        height: hpx(42),
        width: wpx(139),


        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 18,
        //marginTop: hpx(26)
    },
    welcomeStyle: {
        fontSize: nf(32),
        color: '#2DA461',
        fontFamily: Fonts.tofinoSemi,

    },
    startedStyle: {
        fontSize: nf(22),
        color: '#707070',
        fontFamily: Fonts.book_font,

    },
    eye: {
        marginTop: hpx(15),
        position: 'absolute',
        right: wpx(-10),



    },
    eye_icon: {
        width: wpx(37),
        tintColor: '#5DD986',
        height: hpx(28),
    },
    buttonText: {
        fontSize: nf(20),
        fontFamily: Fonts.tofinoSemi,

    },
});

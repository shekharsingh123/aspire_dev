import React, { Component, useEffect, useCallback, useState } from 'react';
import { Platform, Text, StyleSheet, Image, Dimensions, ScrollView, FlatList, TextInput, TouchableOpacity, View, SafeAreaViewBase, SafeAreaView, ImageBackground, } from 'react-native'
import { hpx, wpx, nf, hp, Fonts, Colors, wp } from '../../constants/constants';
let search = require('../.././assets/images/search.png');
let location = require('../.././assets/images/pin.png');
import { useFocusEffect } from '@react-navigation/native';
let logo = require('../.././assets/images/logo.png');
let drow_down = require('../.././assets/images/down-arrow.png');
let meals_cooked = require('../.././assets/images/cookedmeals.jpg');
let demoImg = require('../.././assets/images/download.jpeg');
import Carousel from 'react-native-banner-carousel';
let backImg = require('../.././assets/images/backArrow.png');
let infoImg = require('../.././assets/images/info.png');
let shopping_cart = require('../.././assets/images/shopping_cart.png');
let addImg = require('../.././assets/images/add.png');
import { back } from 'react-native/Libraries/Animated/Easing';
let meals_kit = require('../.././assets/images/mealkits.jpg');
import * as RootNavigation from '../../util/RootNavigation'
let plusImg = require('../.././assets/images/plus.png');
let minusImg = require('../.././assets/images/minus.png');
import Swipeout from 'react-native-swipeout';
let remove_fav = require('../.././assets/images/remove_fav.png');
let cartImg = require('../.././assets/images/cartImg.png');
import ProgressLoader from 'rn-progress-loader';
import { types } from '../../store/ActionTypes';
import AsyncStorage from '@react-native-async-storage/async-storage';
import 'intl';
import 'intl/locale-data/jsonp/en';
const formatter = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 10 });
import { useDispatch, shallowEqual, useSelector } from 'react-redux';

const cartScreen = ({ navigation, }) => {
    const { loaderVisible, selected_Cat, cart_item, banner_Data, products_list, categories_Data } = useSelector(
        (state) => ({
            loaderVisible: state.globalReducer.loader,
            cart_item: state.homeReducer.cart_item

        }),
        shallowEqual,
    );
    const dispatch = useDispatch();
    const [cart_item_array, setcart_item_array] = useState(cart_item);
    const [inc_dec, setIncDec] = useState(1);
    const [activeRow, setactiveRow] = useState();


    const [loader, setloader] = useState(loaderVisible);
    useEffect(() => {
        console.log("loader", loaderVisible)
        setloader(loaderVisible)
    }, [loaderVisible]);

    const getCartdata = () => {
        dispatch({
            type: types.TOGGLE_LOADING,
            payload: true,
        });
        dispatch({
            type: types.CART_ITEM,
            // payload: item.id
        });
    }
    const callNav = (from, item, index) => {


        if (from === 'minus' && cart_item_array[index].items[0].quantity > 0) {
            // Object.assign(item, { quantity: item.quantity - 1});
            //  setIncDec(inc_dec - 1)
            cart_item_array[index].items[0].quantity = parseInt(item.items[0].quantity) - 1;

        } else if (from === 'plus') {
            cart_item_array[index].items[0].quantity = parseInt(item.items[0].quantity) + 1;
            // setIncDec(inc_dec + 1)
        } else {
            //  setIncDec(0)
            cart_item_array[index].items[0].quantity = 0;
        }
        console.log("cart", cart_item_array)
        setcart_item_array([...cart_item_array])
    };


    useFocusEffect(
        React.useCallback(() => {
            getCartdata()
        }
            , []),
    );
    useFocusEffect(
        React.useCallback(() => {
            setcart_item_array(cart_item)
        }
            , [cart_item]),
    );





    const removeCartItem = (id) => {
        var selectedIds = [...cart_item_array];

        selectedIds?.splice(id, 1);
        setcart_item_array(selectedIds)

        let body = {
            cart: cart_item_array[id]?.id,
            item: cart_item_array[id]?.items[0]?.id
        }

        dispatch({
            type: types.REMOVE_CART_ITEM,
            payload: body,
        });

        // getCartdata();

    }

    const swipeoutBtns = [
        {
            component: (
                <TouchableOpacity
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'column',

                    }}
                    onPress={() => removeCartItem(activeRow)}
                    activeOpacity={1.0} >
                    <View

                    >
                        <Image source={remove_fav} style={{ tintColor: 'white' }} />
                    </View>
                </TouchableOpacity>
            ),
            backgroundColor: '#2E6A42',
            underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
            onPress: () => {
                console.log("Delete Item");
            },
        },
    ];

    const renderCartList = ({ item, index }) => {
        console.log("item", item.items[0].quantity)

        return (

            <>
                <Swipeout right={swipeoutBtns}
                    rowID={index}
                    sectionId={1}
                    autoClose={true}
                    onOpen={(secId, rowId, direction) => onSwipeOpen(rowId, direction)}
                >
                    <View style={styles.cardView}>
                        <TouchableOpacity activeOpacity={1.0} style={styles.cardView2}>
                            {item.outfit_thumbnail ? (
                                <Image
                                    source={{ uri: item.items.image }}
                                    style={styles.lenga}
                                    resizeMode="contain"
                                />
                            ) : (
                                <Image
                                    source={meals_cooked}
                                    style={styles.lenga}
                                    resizeMode="contain"
                                />
                            )}
                            <View style={{ paddingHorizontal: wpx(20), }}>
                                <Text
                                    allowFontScaling={false}
                                    style={{ ...styles.lehangaText1, width: wpx(160) }}>
                                    chicken
                                </Text>
                                <Text
                                    allowFontScaling={false}
                                    style={{ ...styles.lehangaText2, width: wpx(160) }}>
                                    Non veg
                                </Text>

                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'flex-start',
                                        alignItems: 'center',
                                    }}>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}>

                                        <Text
                                            allowFontScaling={false}
                                            style={{
                                                ...styles.lehangaText2,
                                                // textDecorationLine: 'line-through',


                                                textAlign: 'center',
                                            }}>
                                            {item?.items[0].total}
                                        </Text>
                                    </View>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            marginLeft: 10,
                                        }}>
                                        <Text
                                            style={{
                                                ...styles.currencyText,
                                                fontSize: nf(13),
                                                right: 0,
                                            }}>
                                            {item.currency_symbol}
                                            {''}
                                        </Text>
                                        {/* <Text
                                        allowFontScaling={false}
                                        style={{
                                            ...styles.lehangaText3,
                                            left: 5,
                                            top: 1.8,
                                            textAlign: 'center',
                                        }}>
                                        {formatter.format(item?.selling_price)}
                                    </Text> */}

                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={{

                            ...styles.button1,
                            backgroundColor: '#F3F3F3',
                            borderWidth: 1,
                            paddingHorizontal: hpx(5),
                            borderColor: '#D3EEDC',
                            shadowColor: '#707070',
                            shadowOffset: { width: 0, height: 1 },
                            shadowOpacity: 0.8,
                            shadowRadius: 1,
                        }}>


                            <TouchableOpacity
                                TouchableOpacity={5}
                                onPress={() => callNav('minus', item, index)}>

                                <Image style={{ width: wpx(9), height: hpx(3), }}
                                    source={minusImg} />
                            </TouchableOpacity>
                            <TouchableOpacity
                            >

                                <Text style={{ ...styles.buttonText, color: '#010101', fontSize: nf(15), }}>{item.items[0].quantity}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                TouchableOpacity={5}
                                onPress={() => callNav('plus', item, index)}>

                                <Image style={{ width: wpx(9), height: hpx(9) }}
                                    source={plusImg} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {/* <View style={styles.seprateCardLine} /> */}
                </Swipeout>
            </>
        );
    };
    const onSwipeOpen = (rowId, direction) => {
        if (typeof direction !== 'undefined') {

            setactiveRow(rowId)
            console.log("Active Row", rowId);
        }
    }
    const BannerWidth = Dimensions.get('window').width;
    const BannerHeight = 260;
    const keyExtractor = useCallback((item, index) => index.toString(), []);

    return (
        <SafeAreaView style={{ width: '100%', height: '100%', backgroundColor: 'white' }}>
            <ProgressLoader
                visible={loader}
                barHeight={100}
                isModal={true} isHUD={false}
                hudColor={"#5DD986"}
                color={"#FFFFFF"} />
            <SafeAreaView style={{ flex: 1, }}>


                <View style={{ height: '10%', flexDirection: 'row', alignContent: 'center', }}>
                    <View style={styles.commonHeaderView}>
                        <TouchableOpacity
                            activeOpacity={1.0}
                            onPress={() => navigation.goBack()}>
                            <Image
                                source={backImg}
                                style={styles.backArrow}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                        <Text allowFontScaling={false} style={styles.heading}>
                            Shop More
                        </Text>
                    </View>

                    <View style={styles.location_style}>
                        <View style={styles.address}>
                            <Image source={location} style={{
                                width: wpx(13.5), height: hpx(16.5), bottom: hpx(5),
                                marginRight: wpx(5)
                            }}>


                            </Image>
                            <Text style={{ fontFamily: Fonts.pangram_regular, color: '#707070', fontSize: nf(18), }}>Deliver to</Text>
                        </View>
                        <View style={styles.address}>

                            <Text style={{ fontFamily: Fonts.pangram_regular, color: '#2DA461', fontSize: nf(24), }}>HOME </Text>
                            <Image source={drow_down} style={{ tintColor: '#2DA461', width: wpx(13.7), height: hpx(7.83), bottom: hpx(10) }}></Image>
                        </View>
                    </View>
                </View>



                {
                    cart_item_array?.length > 0 ?
                        <View
                            style={styles.MainContainer} >
                            <View style={{ marginTop: hpx(15), marginBottom: hpx(15), width: wpx(105), marginHorizontal: wpx(20), height: hpx(37), flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', alignContent: 'center' }}>
                                <Text style={{
                                    fontSize: nf(20),
                                    color: '#2DA461',
                                    fontFamily: Fonts.tofinoSemi,
                                }}>CART</Text>
                                <Image source={shopping_cart} style={{ width: wpx(25), height: hpx(24) }}>

                                </Image>
                            </View>
                            <View style={styles.review}>





                                <FlatList
                                    //  extraData={cart_item_array}
                                    data={cart_item_array}
                                    showsHorizontalScrollIndicator={false}
                                    renderItem={renderCartList}
                                    keyExtractor={keyExtractor}
                                />



                            </View>

                        </View>
                        : <View
                            style={styles.MainContainer} >
                            <View style={{ flex: 1, margin: wpx(18) }}>
                                <Text style={styles.welcomeStyle}>
                                    your cart is empty

                                </Text>

                                {/* <Text style={{ ...styles.startedStyle, marginTop: wpx(30), marginEnd: wpx(60) }}>
                            You can now continue shopping on Springs 15 Marketplace! Have fun!

                        </Text> */}
                            </View>

                            <View >
                                <Image
                                    source={cartImg}
                                    style={styles.logoStyle}></Image>


                            </View>
                            <View style={{ flex: 1, justifyContent: 'flex-end', margin: wpx(18), flexDirection: 'row' }}>

                                <TouchableOpacity
                                    onPress={() => RootNavigation.navigate('HomeStack')}
                                    style={{
                                        ...styles.button,
                                        bottom: 15,
                                        backgroundColor: '#5DD986',

                                        borderColor: '#707070',
                                    }}>
                                    <Text style={{ ...styles.buttonText, color: 'white', }}>Shop Now</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                }


                {/* <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row',marginRight:wpx(30) }}>

                    <TouchableOpacity
                        onPress={() => props.navigation.navigate('Login')}
                       >
                        <Text style={{ fontFamily: Fonts.pangram_regular, color: '#707070', marginTop: hpx(10), fontSize: nf(15),right:10 }}>or Sign Up</Text>
                    </TouchableOpacity>
                </View> */}

            </SafeAreaView>
            <View style={{
                height: hpx(111),
                width: wpx(375),
                bottom: 0,
                borderTopColor: 'black',


                borderColor: '#707070',

                elevation: 1,
                shadowColor: '#707070',
                // shadowOffset: { width: 0, height: 2 },
                // shadowOpacity: 0.9,

                backgroundColor: '#5DD986',
            }}>
                <View style={{ flex: 1, flexDirection: 'row', alignSelf: 'center', alignItems: 'center', justifyContent: 'space-between', }}>
                    <Text allowFontScaling={false} style={{ ...styles.heading, color: '#ffffff', fontSize: nf(20), marginEnd: wpx(20) }}>
                        {cart_item_array?.length} items | AED 00.00
                    </Text>

                    <View style={{

                        ...styles.cart_button,
                        backgroundColor: '#FFFFFF',
                        borderWidth: 1,
                        paddingHorizontal: hpx(5),
                        borderColor: '#D3EEDC',
                        shadowColor: '#707070',
                        shadowOffset: { width: 0, height: 1 },
                        shadowOpacity: 0.8,
                        shadowRadius: 1,
                    }}>



                        <TouchableOpacity
                            onPress={() => navigation.navigate('CheckOut')}
                        >

                            <Text style={{ ...styles.buttonText, color: '#5DD986', fontSize: nf(16), }}>Check-Out</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                        //onPress={() => callNav('plus')}
                        >

                            <Image
                                source={backImg}
                                style={{ ...styles.backArrow, left: wpx(10), tintColor: '#5DD986', transform: [{ rotate: '180deg' }] }}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                    </View>

                </View>
            </View>

        </SafeAreaView >
    );
}

const styles = StyleSheet.create({

    searchBox: {
        height: hpx(49),
        //width: wpx(340),
        backgroundColor: '#5DD986',
        borderRadius: hpx(7),
        borderColor: '#E4E6E772',
        marginTop: hpx(5),
        flexDirection: 'row',

        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: wpx(10),
    },
    heading: {
        fontSize: nf(14),
        marginStart: 10,
        fontFamily: Fonts.begum,
        textAlign: 'center',
    },
    backArrow: {

        width: wpx(16),
        height: hpx(16),
    },
    address: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
        alignContent: 'center',
        bottom: hpx(25)
    },
    selectInfo: {

        marginLeft: wpx(10),
        fontSize: nf(14),
        fontFamily: Fonts.tofino_pro_personal_book,
        padding: 2,
    },
    lehangaText2: {
        fontSize: nf(12),
        fontFamily: Fonts.medium,
        color: '#999999',
        marginVertical: hpx(3),
    },
    commonHeaderView: {

        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: hpx(53),

        width: wpx(175),
        paddingHorizontal: wpx(21),
    },
    location_style: {
        flex: 1,
        justifyContent: 'center',
        height: hpx(126),
        bottom: 1,
        paddingHorizontal: wpx(21),
    },
    lehangaText1: { fontSize: nf(16), fontFamily: Fonts.book_font },
    cardView: {

        height: hpx(109),
        width: wpx(349),
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',

    },
    lenga: {
        width: wpx(80),
        height: hpx(80),
    },
    designerImage: {
        width: wpx(105),
        height: hpx(106),
        //    backgroundColor: 'black'


    },
    designerNameView: {

        justifyContent: 'center',
        width: wpx(105),
        height: hpx(131),
        borderRadius: 5,
        alignItems: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        marginHorizontal: 8,
        borderColor: Colors.grey,
        marginTop: hpx(20),

        elevation: 1,
        shadowColor: '#00000029',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        backgroundColor: 'white',
    },
    designerNameText: {

        top: 2,
        fontSize: nf(12),
        fontFamily: Fonts.tofino_pro_personal_book,
        textAlign: 'center',
        textAlignVertical: 'center',
        color: Colors.black,

    },
    MainContainer: {
        flex: 1,



    },
    SectionStyle: {
        flex: 1,
        marginStart: wpx(25),
        marginEnd: wpx(30),
        marginTop: wpx(20),
        flexDirection: 'row',
        justifyContent: 'flex-start',




    },
    inputStyle: {
        borderBottomColor: '#707070', // Add this to specify bottom border color
        width: wpx(320),
        borderBottomWidth: 1,
        fontSize: nf(22),

        fontFamily: Fonts.tofinoSemi,

    },
    logoStyle: {
        width: wpx(116),
        marginStart: wpx(20),
        height: hpx(89),
        bottom: hpx(20),
        resizeMode: 'contain'

    },
    button1: {
        height: hpx(25),
        width: wpx(68),

        alignContent: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 6,
        //marginTop: hpx(26)
    },
    cart_button: {
        height: hpx(43),
        width: wpx(135),

        alignContent: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
        //marginTop: hpx(26)
    },
    button: {

        flexDirection: "row",
        justifyContent: "flex-end",
        height: hpx(43),
        width: wpx(145),
        marginTop: wpx(40),
        marginRight: wpx(20),
        backgroundColor: '#D80B8C',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3,
        shadowColor: "#000",
        shadowOffset: {
            width: 1,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        //marginTop: hpx(26)
    },
    welcomeStyle: {
        fontSize: nf(32),
        color: '#2DA461',
        fontFamily: Fonts.tofinoSemi,

    },
    startedStyle: {
        fontSize: nf(22),
        color: '#707070',
        fontFamily: Fonts.book_font,

    },
    eye: {
        marginTop: hpx(15),
        position: 'absolute',
        right: wpx(-10),



    },
    eye_icon: {
        width: wpx(37),
        tintColor: '#5DD986',
        height: hpx(28),
    },
    buttonText: {
        fontSize: nf(20),
        fontFamily: Fonts.tofinoSemi,

    },
    review: {
        alignContent: 'center',
        alignItems: 'center',

        justifyContent: 'center',
        alignSelf: 'center',
        width: wpx(349),

        backgroundColor: '#F3F3F3',
        borderColor: '#F3F3F3',
        shadowColor: '#F3F3F3',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        borderRadius: 1,

    },
    cardView2: {

        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    seprateCardLine: {
        width: wp(90),
        height: hpx(0.9),
        backgroundColor: '#00000029',
        marginTop: hpx(20),
        opacity: 0.5,
    },
});
export default cartScreen;
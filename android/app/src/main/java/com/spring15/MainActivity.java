package com.spring15;

import com.facebook.react.ReactActivity;
import android.os.Bundle;
 import com.facebook.react.ReactRootView;
 import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.bridge.JSIModulePackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;
import com.swmansion.reanimated.ReanimatedJSIModulePackage;

public class MainActivity extends ReactActivity {
 @Override
protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(null);
}
  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "Spring15";
  }


  @Override
 protected ReactActivityDelegate createReactActivityDelegate() {
   return new ReactActivityDelegate(this, getMainComponentName()) {
    @Override
      protected ReactRootView createRootView() {
       return new RNGestureHandlerEnabledRootView(MainActivity.this);
     }
   };
 }
 
}
